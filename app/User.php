<?php

/**
 * Fields
name (имя пользователя)
email (электронная почта)
password (пароль)
sex (пол)
avatar (аватар)
 */

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{

    use Notifiable;

    use EntrustUserTrait; // add this trait to your user model
    use \KodiComponents\Support\Upload; //загрузка файлов из админки

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'avatar' => 'image', // or file | upload
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'sex', 'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Получает комментарии пользователя
     * @return [type] [description]
     */
    public function getUserCommentNews()
    {
        return $this->hasMany(NewComment::class, 'user_id');
    }

    /**
     * Получает участников номинации по пользователю
     * @return [type] [description]
     */
    public function competitors()
    {
        return $this->hasMany('App\Competitor');
    }

    /**
     * Получает все комментарии пользователя
     * @return [type] [description]
     */
    public function comments()
    {
        return $this->hasMany('App\Comment', 'user_id');
    }

    /**
     * Получает роль пользователя, пока что используется для получения админа для генерации жалобы
     * @return [type] [description]
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }


    /**
     * Получает количество лайков у пользователя
     * @return [type] [description]
     */
    public function likes()
    {
        return $this->hasOne('App\LikeUser');
    }

}
