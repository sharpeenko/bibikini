<?php

/**
 * @Author: sharpeenko
 * @Date:   2017-01-30 08:37:50
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-02-08 10:29:29
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Nomination extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'nomination';
    }

}
