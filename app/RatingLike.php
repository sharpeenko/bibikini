<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RatingLike extends Model
{
    protected $table = 'rating_likes';

    /**
     * Получение фото по рейтингу
     */
    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }

    /**
     * Получение номинации по рейтингу
     */
    public function nomination()
    {
        return $this->belongsTo('App\Nomination');
    }
}
