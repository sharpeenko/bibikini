<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nomination extends Model
{

    use \KodiComponents\Support\Upload;

    protected $casts = [
        'pic' => 'image', // or file | upload
        'pic_rules' => 'image', // or file | upload
        'poses' => 'image', // Изобразение роз
    ];

    /**
     * Получает всех участников номинации
     */
    public function competitors()
    {

//        return $this->hasManyThrough(
        //                        'App\User', 'App\Competitor', 'user_id', 'id', 'id'
        //        );

        return $this->hasMany('App\Competitor');
    }

    /**
     * Получает все фото номинации
     */
    public function photos()
    {
        return $this->hasMany('App\Photo');
    }


    /**
     * Получает всех судей номинации
     */
    public function referees()
    {
        return $this->hasMany('App\Referee');
    }


    /**
     * Получает призовые фонды  номинации по судейским оценкам
     */
    public function fundsScores()
    {
        return $this->hasOne('App\Fund')->where('prizeable', 'score');
    }

    /**
     * Получает призовые фонды  номинации по лайкам
     */
    public function fundsLikes()
    {
        return $this->hasOne('App\Fund')->where('prizeable', 'like');
    }

    /**
     * Получает рейтинги  номинации по судейским оценкам
     */
    public function ratingScores()
    {
        return $this->hasMany('App\RatingScore');
    }

    /**
     * Получает рейтинги  номинации по лайкам
     */
    public function ratingLikes()
    {
        return $this->hasMany('App\RatingLike');
    }

    /**
     * Получить все комментарии номинации
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }


}
