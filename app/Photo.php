<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'photo',
    ];

    /**
     * Получение участника по фото
     */
    public function getCompetitor()
    {
        return $this->belongsTo('App\Competitor', 'competitor_id');
    }

    /**
     * Получение номинации по фото
     */
    public function getNomination()
    {
        return $this->belongsTo('App\Nomination', 'nomination_id');
    }

    //Получение имени пользователя (не используется)

    public function username($id)
    {
        $username = DB::table('photos')
            ->join('competitors', 'photos.competitor_id', '=', 'competitors.id')
            ->join('users', 'competitors.user_id', '=', 'users.id')
            ->select('users.name')
            ->where('photos.id', '=', $id)
            ->get();

        return $username;

    }

    /**
     * Получить все комментарии фото
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Получить все лайки фото.
     */
    public function likes()
    {
        return $this->morphMany('App\Like', 'likeable');
    }

    /**
     * Получение рейтинга лайков
     */
    public function RatingLike() {
       return $this->hasOne('App\RatingLike');
    }



}
