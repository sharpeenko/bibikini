<?php

use App\Competitor;
use App\Photo;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Competitor::class, function (ModelConfiguration $model) {
    $model->enableAccessCheck(); //проверка прав доступа к модели
    $model->setTitle('Участники');
    $model->disableCreating();
    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()
            ->setFilters(AdminDisplayFilter::field('nomination_id')->setTitle('Nomination ID [:value]'))
            ->with('getUser', 'getNomination')
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::relatedLink('getUser.name')->setLabel('Имя пользователя')->setWidth('200px'),
                AdminColumn::relatedLink('getNomination.name')->setLabel('Номинация')->setWidth('200px'),
            ])->paginate(20);
    });

    // Create And Edit
    $model->onCreateAndEdit(function ($id = null) {

        $form = AdminForm::panel()
            ->setHtmlAttribute('enctype', 'multipart/form-data')
            ->addHeader([
                AdminColumn::relatedLink('getUser.name')->setLabel('Имя пользователя')->setWidth('200px'),
                '<br>',
                AdminColumn::relatedLink('getNomination.name')->setLabel('Номинация')->setWidth('200px'),
                '<br />',
                AdminFormElement::text('hashtag', 'Хэштэг'),
                AdminFormElement::checkbox('is_privelege_addition_photo', 'Дополнительные фото бесплатно'),
            ])
            ->addBody([
                AdminFormElement::textarea('about', 'О себе'),
                AdminFormElement::checkbox('is_champion', 'Чемпион'),
                AdminFormElement::ckeditor('champion_description', 'Чемпионское описание'),
                AdminFormElement::select('prize_place', 'Место')->setOptions([
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                ]),

            ])
            ->addFooter([
                AdminColumn::image('avatar')->setWidth('300px'),
                AdminFormElement::upload('avatar', 'Загрузить'),
                  '<h3>Дополнительный тур</h3>',
                AdminFormElement::checkbox('is_additional_round', 'Прошел в дополнительный тур'),
                AdminFormElement::textarea('additional_round_description', 'Примечание к дополнительному туру'),
            ]);

        $photos = AdminDisplay::table()
            ->setModelClass(Photo::class)
            ->setApply(function ($query) use ($id) {
                $query->where('competitor_id', $id); // Фильтруем список фотографий по ID галереи
            })

            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::image('photo')->setLabel('Все фото участника')->setImageWidth('300px'),

            ]);

        $form->addBody($photos);
        return $form;
    });
})
    ->addMenuPage(Competitor::class, 0)
    ->setIcon('fa fa-child');
