<?php

use App\RatingScore;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(RatingScore::class, function (ModelConfiguration $model) {
    $model->enableAccessCheck(); //проверка прав доступа к модели
    $model->setTitle('Рейтинг оценок');
    $model->disableCreating();
    $model->disableEditing();
    $model->disableDeleting();

    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()
            ->with('competitor')
            ->with('nomination')
        //->with('getNew')
            ->setColumns([
                AdminColumn::relatedLink('competitor.getUser.name')->setLabel('Участник/Пользователь')->setWidth('200px'),
                AdminColumn::relatedLink('nomination.name')->setLabel('Номинация')->setWidth('200px'),
                AdminColumn::text('avscore')->setLabel('Оценка')->setWidth('200px'),
                AdminColumn::text('position')->setLabel('Позиция')->setWidth('200px'),

            ])
            ->paginate(10);
        ;
    });


})
    ->addMenuPage(RatingScore::class, 0)
    ->setIcon('fa fa-bar-chart');
