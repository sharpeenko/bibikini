<?php

use App\News;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(News::class, function (ModelConfiguration $model) {
            $model->enableAccessCheck(); //проверка прав доступа к модели
            $model->setTitle('Новости');
            // Display
            $model->onDisplay(function () {
                return AdminDisplay::table()
                        ->setColumns([
                            AdminColumn::link('header')->setLabel('Заголовок')->setWidth('150px'),
                            AdminColumn::datetime('publish_at')->setLabel('Дата публикации')->setFormat('d.m.Y')->setWidth('200px'),
                            AdminColumn::text('author')->setLabel('Автор')->setWidth('200px'),
                            AdminColumn::text('introtext')->setLabel('Аннотация')->setWidth('200px'),
                            AdminColumn::custom()->setLabel('Важная?')->setCallback(function ($instance) {
                                if ($instance->is_important == 1) {
                                    return 'да';
                                } else {
                                    return 'нет';
                                }
                            })->setWidth('50px'),
                            AdminColumn::custom()->setLabel('Комментарии')->setCallback(function ($instance) {
                                        return '<a href=' . url('/admin/new_comments?new_id=' . $instance->id) . '>Комментарии</a>';
                                    })->setWidth('50px'),
                        ])->paginate(20);
            });


            // Create And Edit
            $model->onCreateAndEdit(function() {
                return AdminForm::panel()
                        ->setHtmlAttribute('enctype', 'multipart/form-data')
                        ->addHeader([
                            AdminFormElement::text('header', 'Заголовок')->required(),
                            AdminColumn::image('pic')->setWidth('300px'),
                            AdminFormElement::date('publish_at', 'Опубликован')->required(),
                            AdminFormElement::text('author', 'Автор')->required(),
                            AdminFormElement::text('introtext', 'Аннотация')->required(),
                        ])
                        ->addBody([
                            AdminFormElement::upload('pic', 'Мини-изображение'),
                            AdminFormElement::ckeditor('content', 'Содержимое'),
                            AdminFormElement::text('source', 'Источник'),
                              '<h3>Важная новость?</h3>',
                            AdminFormElement::checkbox('is_important', 'Является важной'),
                            AdminFormElement::textarea('important_intro', 'Аннотация'),
                            AdminFormElement::text('important_styles', 'Стили jumbotron'),
                            AdminFormElement::text('important_button_style', 'Стиль кнопки'),
                            AdminFormElement::text('important_button_text', 'Надпись на кнопке'),
                            AdminFormElement::text('important_button_icon', 'Иконка на кнопке'),
                        ])
                        ->addFooter([
                            AdminFormElement::checkbox('published', 'Опубликовано'),
                            AdminFormElement::checkbox('deleted', 'Удалено'),
                ]);
            });
        })
        ->addMenuPage(News::class, 0)
        ->setIcon('fa fa-newspaper-o');
