<?php

use App\RatingLike;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(RatingLike::class, function (ModelConfiguration $model) {
    $model->enableAccessCheck(); //проверка прав доступа к модели
    $model->setTitle('Рейтинг фото');
    $model->disableCreating();
    $model->disableEditing();
    $model->disableDeleting();

    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()
            ->with('photo')
            ->with('nomination')
        //->with('getNew')
            ->setColumns([
                AdminColumn::image('photo.photo')->setLabel('Фото')->setWidth('200px'),
                AdminColumn::relatedLink('nomination.name')->setLabel('Номинация')->setWidth('200px'),
                AdminColumn::text('like_count')->setLabel('Лайки')->setWidth('200px'),
                AdminColumn::text('position')->setLabel('Позиция')->setWidth('200px'),

            ])
            ->paginate(10);
        ;
    });


})
    ->addMenuPage(RatingLike::class, 0)
    ->setIcon('fa fa-bar-chart');
