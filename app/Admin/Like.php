<?php

use App\Like;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Like::class, function (ModelConfiguration $model) {
    $model->enableAccessCheck(); //проверка прав доступа к модели
    $model->setTitle('Лайки');
    $model->disableCreating();
    $model->disableEditing();
    $model->disableDeleting();

    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()
            ->setFilters(AdminDisplayFilter::field('likeable_id')->setTitle('Photo ID [:value]'))
            ->with('user')
        //->with('getNew')
            ->setColumns([
                AdminColumn::datetime('created_at')->setLabel('Установлен')->setFormat('Y-m-d H:i:s')->setWidth('200px'),
                AdminColumn::relatedLink('user.name')->setLabel('Пользователь')->setWidth('200px'),
                AdminColumn::custom()->setLabel('Объект')->setCallback(function ($instance) {
                    if ($instance->likeable_type == 'App\Photo') {
                        return  '<td class="row-image">
                                         <a href="'.$instance->likeable->photo.'" data-toggle="lightbox">
		                                           <img class="thumbnail" src="'.$instance->likeable->photo.'" width="80px">
	                                        </a>
                                    </td>' ;
                    }
                })->setWidth('50px'),
            ])->paginate(10);
        ;
    });


})
    ->addMenuPage(Like::class, 0)
    ->setIcon('fa fa-heart');
