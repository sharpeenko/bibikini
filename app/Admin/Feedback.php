<?php

use App\Feedback;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Feedback::class, function (ModelConfiguration $model) {

            // Запрет на создание
            $model->disableCreating();

            // Запрет на редактирование
            $model->disableEditing();

            // Запрет на удаление
            $model->disableDeleting();

            $model->setTitle('Сообщения ФОС');
            // Display
            $model->onDisplay(function () {
                return AdminDisplay::table()
                        ->setColumns([
                            AdminColumn::email('email', 'Email')->setWidth('150px'),
                            AdminColumn::text('name')->setLabel('Отправитель')->setWidth('150px'),
                            AdminColumn::text('message')->setLabel('Сообщение')->setWidth('150px'),
                        ])->paginate(10);
                ;
            });
        })
        ->addMenuPage(Feedback::class, 0)
        ->setIcon('fa fa-envelope-square');

