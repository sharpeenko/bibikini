<?php

use App\Recipient;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Recipient::class, function (ModelConfiguration $model) {
            $model->enableAccessCheck(); //проверка прав доступа к модели
            $model->setTitle('Получатели');
            // Display
            $model->onDisplay(function () {
                return AdminDisplay::table()
                        ->setColumns([
                            AdminColumn::text('email')->setLabel('Email')->setWidth('150px'),
                ]);
            });


            // Create And Edit
            $model->onCreateAndEdit(function() {
                return AdminForm::panel()->addBody([
                            AdminFormElement::text('email', 'Email')->required(),
                ]);
            });
        })
        ->addMenuPage(Recipient::class, 0)
        ->setIcon('fa fa-envelope-o');

