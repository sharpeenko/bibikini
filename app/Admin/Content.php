<?php

use App\Content;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Content::class, function (ModelConfiguration $model) {
            $model->enableAccessCheck(); //проверка прав доступа к модели
            $model->setTitle('Контент');
            // Display
            $model->onDisplay(function () {
                return AdminDisplay::table()
                        ->setHtmlAttribute('class', 'table-primary')
                        ->setColumns([
                            AdminColumn::link('description')->setLabel('Описание')->setWidth('300px'),
                        ])->paginate(20);
            });


            // Create And Edit
            $model->onCreateAndEdit(function() {
                return AdminForm::panel()
                        ->addHeader([
                          AdminFormElement::text('description', 'Описание')->required(),
                          AdminFormElement::text('key', 'Ключ')->required(),
                          AdminFormElement::text('key_page', 'Ключ страницы'),
                        ])
                        ->addBody([
                            AdminFormElement::text('header', 'Заголовок'),
                            AdminFormElement::ckeditor('content', 'Содержимое'),
                        ])
                        ->addFooter([


                ]);
            });
        })
        ->addMenuPage(Content::class, 0)
        ->setIcon('fa fa-book');
