<?php

use App\Permission;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Permission::class, function (ModelConfiguration $model) {
            $model->enableAccessCheck(); //проверка прав доступа к модели
            $model->setTitle('Разрешения ролей');
            // Display
            $model->onDisplay(function () {
                return AdminDisplay::table()
                        ->setHtmlAttribute('class', 'table-primary')
                        ->setColumns([
                            AdminColumn::text('name')->setLabel('Имя разрешения')->setWidth('150px'),
                            AdminColumn::text('display_name')->setLabel('Отображается, как')->setWidth('150px'),
                            AdminColumn::text('description')->setLabel('Описание')->setWidth('300px'),
                ]);
            });


            // Create And Edit
            $model->onCreateAndEdit(function() {
                return AdminForm::panel()
                        ->addBody([
                            AdminFormElement::text('name', 'Имя разрешения')->required(),
                            AdminFormElement::text('display_name', 'Отображается как'),
                            AdminFormElement::text('description', 'Описание'),
                ]);
            });
        })
        ->addMenuPage(Permission::class, 0)
        ->setIcon('fa fa-users');
