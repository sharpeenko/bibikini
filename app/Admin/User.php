<?php

use App\User;
use App\Role;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(User::class, function (ModelConfiguration $model) {
            $model->enableAccessCheck(); //проверка прав доступа к модели
            $model->setTitle('Аккаунты');
            // Display
            $model->onDisplay(function () {
                return AdminDisplay::table()
                        ->with('roles')
                        ->with('likes')
                        ->setHtmlAttribute('class', 'table-primary')
                        ->setColumns([
                            AdminColumn::link('name')->setLabel('Имя пользователя')->setWidth('200px'),
                            AdminColumn::text('email')->setLabel('Email')->setWidth('150px'),
                            AdminColumn::lists('roles.display_name')->setLabel('Роль')->setWidth('200px'),
                            AdminColumn::text('likes.like_count')->setLabel('Лайков')->setWidth('50px'),
                        ])->paginate(20);
            });


            // Create And Edit
            $model->onCreateAndEdit(function() {
                return AdminForm::panel()
                        ->setHtmlAttribute('enctype', 'multipart/form-data')
                        ->addHeader([
                            AdminFormElement::text('name', 'Имя пользователя')->required(),
                            AdminFormElement::text('email', 'Email')->required(),
                            AdminFormElement::text('hashtag', 'Хэштэг'),
                        ])
                        ->addBody([

                          AdminFormElement::checkbox('is_privelege', 'Привелегированный'),
                            AdminFormElement::select('sex', 'Пол')->setOptions([
                                'мужской' => 'мужской',
                                'женский' => 'женский'
                            ]),
                            AdminFormElement::multiselect('roles', 'Роль')->setModelForOptions(new Role())->setDisplay('display_name')->required(),
                        ])
                        ->addFooter([
                            AdminColumn::image('avatar')->setWidth('300px'),
                            AdminFormElement::upload('avatar', 'Загрузить'),

                ]);
            });
        })
        ->addMenuPage(User::class, 0)
        ->setIcon('fa fa-user');
