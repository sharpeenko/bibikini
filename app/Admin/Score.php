<?php

use App\Score;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Score::class, function (ModelConfiguration $model) {
    //проверка прав доступа к модели
    $model->enableAccessCheck();
    $model->setTitle('Оценки');
    $model->disableCreating();
    $model->disableEditing();
    $model->disableDeleting();
    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()

            ->with('competitor', 'referee')
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::relatedLink('referee.user.name')->setLabel('Судья')->setWidth('200px'),
                AdminColumn::relatedLink('competitor.getUser.name')->setLabel('Участник')->setWidth('200px'),
                AdminColumn::image('competitor.avatar', 'Аватар')->setWidth('100px'),
                AdminColumn::text('score')->setLabel('Оценка')->setWidth('20px'),
                AdminColumn::datetime('created_at', 'Поставлена')->setFormat('d.m.Y')->setWidth('150px'),
                AdminColumn::text('is_actual')->setLabel('Актуальность')->setWidth('20px'),
                AdminColumn::relatedLink('competitor.getNomination.name')->setLabel('Номинация')->setWidth('100px')
            ])->paginate(20);
    });

})


    ->addMenuPage(Score::class, 0)
    ->setIcon('fa fa-graduation-cap');
