<?php

use App\Role;
use App\Permission;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Role::class, function (ModelConfiguration $model) {
    $model->enableAccessCheck(); //проверка прав доступа к модели
    $model->setTitle('Роли пользователей');
    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()
                        ->with('perms')
                        ->setHtmlAttribute('class', 'table-primary')
                        ->setColumns([
                            AdminColumn::link('display_name')->setLabel('Роль')->setWidth('150px'),
                            AdminColumn::text('on_display')->setLabel('В списках')->setWidth('150px'),
                            AdminColumn::lists('perms.display_name')->setLabel('Разрешения')->setWidth('200px'),
                ]);
    });


    // Create And Edit
    $model->onCreateAndEdit(function () {
        return AdminForm::panel()
                        ->addBody([
                            AdminFormElement::text('name', 'Роль')->required(),
                            AdminFormElement::text('display_name', 'Отображается как')->required(),
                            AdminFormElement::select('on_display', 'В списках')->setOptions([
                                'да' => 'да',
                                'нет' => 'нет'
                            ]),
                            AdminFormElement::multiselect('perms', 'Разрешения')->setModelForOptions(new Permission())->setDisplay('display_name'),
                ]);
    });
})
        ->addMenuPage(Role::class, 0)
        ->setIcon('fa fa-users');
