<?php

use App\Nomination;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Nomination::class, function (ModelConfiguration $model) {
    $model->enableAccessCheck(); //проверка прав доступа к модели
    $model->setTitle('Номинации');
    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()
            ->with('fundsScores', 'fundsLikes')
            ->setColumns([
                AdminColumn::link('name')->setLabel('Название')->setWidth('150px'),
                //AdminColumn::text('introtext')->setLabel('Краткое описание')->setWidth('400px'),
                AdminColumn::text('type')->setLabel('Тип')->setWidth('100px'),
                AdminColumn::custom()->setLabel('Опубликована')->setCallback(function ($instance) {
                    if ($instance->published == 1) {
                        return 'да';
                    } else {
                        return 'нет';
                    }
                })->setWidth('50px'),
                AdminColumn::custom()->setLabel('Открыта')->setCallback(function ($instance) {
                    if ($instance->is_actual == 1) {
                        return 'да';
                    } else {
                        return 'нет';
                    }
                })->setWidth('50px'),
                AdminColumn::datetime('start')->setLabel('Старт')->setFormat('d.m.Y')->setWidth('150px'),
                AdminColumn::datetime('finish')->setLabel('Финиш')->setFormat('d.m.Y')->setWidth('150px'),
                AdminColumn::custom()->setLabel('Участники')->setCallback(function ($instance) {
                    return '<a href=' . url('/admin/competitors?nomination_id=' . $instance->id) . '>Участники</a>';
                })->setWidth('50px'),
//                            AdminColumn::text('author')->setLabel('Автор')->setWidth('200px'),
                //                            AdminColumn::text('introtext')->setLabel('Аннотация')->setWidth('200px'),
                //                            AdminColumn::custom()->setLabel('Комментарии')->setCallback(function ($instance) {
                //                                        return '<a href=' . url('/admin/new_comments?new_id=' . $instance->id) . '>Комментарии</a>';
                //                                    })->setWidth('50px'),
                AdminColumn::custom()->setLabel('Статус')->setCallback(function ($instance) {
                    switch ($instance->status) {
                      case 1:
                          return 'Открыта';
                          break;
                      case 2:
                          return 'Продлена';
                          break;
                      case 3:
                          return 'Завершена';
                          break;
                    }
                })->setWidth('50px'),
            ])->paginate(20);
    });

    // Create And Edit
    $model->onCreateAndEdit(function () {
        return AdminForm::panel()
            ->setHtmlAttribute('enctype', 'multipart/form-data')
            ->addHeader([
                AdminFormElement::text('name', 'Название')->required(),
                AdminFormElement::select('type', 'Тип')->setOptions([
                    'женщины' => 'женщины',
                    'мужчины' => 'мужчины',
                    'смешанный' => 'смешанный',
                ])->required(),
                AdminFormElement::textarea('introtext', 'Краткое описание'),
                '<h3>Призовой фонд</h3>',
                '<h4>Судейские оценки</h4>',
                AdminFormElement::text('fundsScores.prizeable', 'Код фонда'),
                AdminFormElement::text('fundsScores.sum', 'Общая сумма'),
                AdminFormElement::text('fundsScores.position_1', 'Первое место'),
                AdminFormElement::text('fundsScores.position_2', 'Второе место'),
                AdminFormElement::text('fundsScores.position_3', 'Третье место'),
                '<h4>Лайки</h4>',
                AdminFormElement::text('fundsLikes.prizeable', 'Код фонда'),
                AdminFormElement::text('fundsLikes.sum', 'Общая сумма'),
                AdminFormElement::text('fundsLikes.position_1', 'Первое место'),
                AdminFormElement::text('fundsLikes.position_2', 'Второе место'),
                AdminFormElement::text('fundsLikes.position_3', 'Третье место'),
                AdminColumn::image('pic')->setWidth('300px'),
                AdminFormElement::upload('pic', 'Мини-изображение'),
                AdminColumn::image('pic_rules')->setWidth('300px'),
                AdminFormElement::upload('pic_rules', 'Мини-изображение правил'),
                AdminFormElement::date('start', 'Старт'),
                AdminFormElement::date('finish', 'Финиш'),
//                            AdminFormElement::text('author', 'Автор')->required(),
                AdminFormElement::text('quant_photo', 'Количество фото'),
                AdminFormElement::text('participation_fee', 'Стоимость участия'),
            ])
            ->addBody([
                '<h3>Платная/бесплатная</h3>',
                AdminFormElement::checkbox('is_commercial', 'Платная'),
                AdminFormElement::textarea('payment_form', 'Код формы оплаты'),
                AdminFormElement::textarea('payment_form_additional_photo', 'Код формы оплаты дополнительных фото'),  
                  '<h3>Содержимое</h3>',
                AdminFormElement::ckeditor('description', 'Правила'),
                AdminFormElement::textarea('introtext_list', 'Краткое описание для списка'),
                AdminColumn::image('poses')->setWidth('400px'),
                AdminFormElement::upload('poses', 'Позы'),
                '<h3>Классы CSS и HTML-атрибуты</h3>',
                AdminFormElement::textarea('styles', 'Стили, иконки и др.'),
            ])
            ->addFooter([
                AdminFormElement::checkbox('published', 'Опубликовано'),
                AdminFormElement::checkbox('is_actual', 'Открыта'),
                AdminColumn::custom()->setLabel('Участники')->setCallback(function ($instance) {
                    return '<a href=' . url('/admin/competitors?nomination_id=' . $instance->id) . ' class="btn btn-default">Участники</a>';
                })->setWidth('50px'),
                AdminFormElement::select('status', 'Статус')->setOptions([
                    '1' => 'открыта',
                    '2' => 'продлена',
                    '3' => 'завершена',
                ]),
            ]);
    });
})
    ->addMenuPage(Nomination::class, 0)
    ->setIcon('fa fa-trophy');
