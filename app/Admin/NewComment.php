<?php

use App\NewComment;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(NewComment::class, function (ModelConfiguration $model) {
            $model->enableAccessCheck(); //проверка прав доступа к модели
            $model->setTitle('Комментарии новостей');
            $model->disableCreating();
            // Display
            $model->onDisplay(function () {
                return AdminDisplay::table()
                        ->setFilters(AdminDisplayFilter::field('new_id')->setTitle('New ID [:value]'))
                        ->with('getAuthor')
                        ->with('getNew')
                        ->setColumns([
                            AdminColumn::link('content')->setLabel('Комментарий')->setWidth('500px'),
                            AdminColumn::datetime('publish_at')->setLabel('Дата публикации')->setFormat('Y-m-d H:i:s')->setWidth('200px'),
                            AdminColumn::relatedLink('getAuthor.name')->setLabel('Автор')->setWidth('200px'),
                            AdminColumn::relatedLink('getNew.header')->setLabel('Новость')->setWidth('200px'),
                        ])->paginate(20);
                ;
            });


            // Create And Edit
            $model->onCreateAndEdit(function() {
                return AdminForm::panel()
                        ->addBody([
                            AdminFormElement::text('content', 'Комментарий'),
                ]);
            });
        })
        ->addMenuPage(NewComment::class, 0)
        ->setIcon('fa fa-commenting');

