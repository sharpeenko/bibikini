<?php

use App\Referee;
use App\User;
use App\Nomination;
use App\Events\AddRefereeNomination;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Referee::class, function (ModelConfiguration $model) {
    //проверка прав доступа к модели
    $model->enableAccessCheck();
    $model->setTitle('Судьи');
    //$model->disableCreating();
    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()

            ->with('user', 'nomination')
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::relatedLink('user.name')->setLabel('Имя пользователя')->setWidth('200px'),
                AdminColumn::relatedLink('nomination.name')->setLabel('Номинация')->setWidth('200px'),
            ])->paginate(20);
    });

    // Create And Edit
    $model->onCreateAndEdit(function () {
        return AdminForm::panel()

                ->setHtmlAttribute('enctype', 'multipart/form-data')
                ->addHeader([


                ])
                ->addBody([
                 AdminFormElement::select('user_id', 'Пользователь', User::class)->setLoadOptionsQueryPreparer(function ($element, $query) {
                     return $query
                      ->join('role_user', 'role_user.user_id', '=', 'users.id')
                     ->where('role_user.role_id', 7);
                 })->setDisplay('name'),
                 AdminFormElement::select('nomination_id', 'Номинация', Nomination::class)->setDisplay('name'),


                ])
                ->addFooter([


        ]);
    });
    $model->created(function (ModelConfiguration $model, Referee $referee) {
        Event::fire(new AddRefereeNomination($referee, 'AddRefereeNomination'));
    });
    $model->updated(function (ModelConfiguration $model, Referee $referee) {
        Event::fire(new AddRefereeNomination($referee, 'AddRefereeNomination'));
    });
})
    ->addMenuPage(Referee::class, 0)
    ->setIcon('fa fa-graduation-cap');
