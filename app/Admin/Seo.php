<?php

use App\Seo;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Seo::class, function (ModelConfiguration $model) {
            $model->enableAccessCheck(); //проверка прав доступа к модели
            $model->setTitle('SEO');
            // Display
            $model->onDisplay(function () {
                return AdminDisplay::table()
                        ->setHtmlAttribute('class', 'table-primary')
                        ->setColumns([
                            AdminColumn::link('page')->setLabel('Страница')->setWidth('150px'),
                            AdminColumn::text('key')->setLabel('Ключ')->setWidth('150px'),
                            AdminColumn::text('title')->setLabel('Тэг title')->setWidth('150px'),
                            AdminColumn::text('description')->setLabel('Тэг description')->setWidth('150px'),
                      ])->paginate(20);
            });


            // Create And Edit
            $model->onCreateAndEdit(function() {
                return AdminForm::panel()
                        ->addHeader([
                          AdminFormElement::text('page', 'Страница')->required(),
                          AdminFormElement::text('key', 'Ключ')->required(),

                        ])
                        ->addBody([
                          AdminFormElement::text('pre_title', 'Префикс тэга title'),
                          AdminFormElement::text('title', 'Тэг title'),
                          AdminFormElement::text('post_title', 'Постфикс тэга title'),
                          AdminFormElement::text('description', 'Тэг description'),
                          AdminFormElement::text('keyword', 'Тэг keywords'),
                          AdminFormElement::text('author', 'Тэг author'),
                          AdminFormElement::text('copyright', 'Тэг copyright'),
                        ])
                        ->addFooter([


                ]);
            });
        })
        ->addMenuPage(Seo::class, 0)
        ->setIcon('fa fa-book');
