<?php
/**
 * @Author: sharpeenko
 * @Date:   2017-03-17 07:43:03
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-05-02 08:45:37
 */
use App\Events\ChangePhotoStatus;
use App\Photo;
use Illuminate\Support\Facades\Event;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Photo::class, function (ModelConfiguration $model) {
    $model->enableAccessCheck(); //проверка прав доступа к модели
    $model->setTitle('Фотогалерея');
    $model->disableCreating();
    //$model->disableDeleting();
    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                //AdminColumn::image('photo')->setLabel('Фото')->setWidth('100px'),
                AdminColumn::relatedLink('id')->setLabel('ID')->setWidth('40px'),
                AdminColumn::custom()->setLabel('Фото')->setCallback(function ($instance) {
                    return '<a href=' . url('/admin/photos/'. $instance->id.'/edit') . '><img src='.$instance->photo.' class="thumbnail" width="80px"></a>';

                })->setWidth('100px'),
                AdminColumn::custom()->setLabel('Статус')->setCallback(function ($instance) {
                    switch ($instance->photo_status_id) {
                        case 1:
                            return 'загружена';
                        case 2:
                            return 'одобрена';
                        case 3:
                            return 'отклонена';
                        case 4:
                            return 'на подтверждении';
                    }
                })->setWidth('50px'),
                AdminColumn::custom()->setLabel('Загружена как')->setCallback(function ($instance) {
                    switch ($instance->is_addition) {
                        case 0:
                            return 'основное';
                        case 1:
                            return 'дополнительное';

                    }
                })->setWidth('50px'),
                AdminColumn::datetime('created_at')->setLabel('Дата создания')->setFormat('d.m.Y')->setWidth('100px'),
                AdminColumn::datetime('updated_at')->setLabel('Дата изменения')->setFormat('d.m.Y')->setWidth('100px'),
                AdminColumn::relatedLink('getCompetitor.id')->setLabel('Участник')->setWidth('50px'),
                AdminColumn::relatedLink('getCompetitor.getUser.name')->setLabel('Пользователь')->setWidth('200px'),
                AdminColumn::relatedLink('getNomination.name')->setLabel('Номинация')->setWidth('200px'),
                AdminColumn::custom()->setLabel('Лайки')->setCallback(function ($instance) {
                    return '<a href=' . url('/admin/likes?likeable_id=' . $instance->id) . '>Лайки</a>';
                })->setWidth('50px'),
            ])
            ->setApply(function ($query) {
                $query->orderBy('created_at', 'desc');
            })
            ->paginate(20);
    });

    // Create And Edit
    $model->onCreateAndEdit(function () {
        return AdminForm::panel()
            ->addHeader([
              '<h5>ID</h5>',
              AdminColumn::text('id')->setLabel('ID')->setWidth('50px'),
                '<h5>Идентификатор участника</h5>',
                AdminColumn::relatedLink('getCompetitor.id')->setLabel('Участник')->setWidth('50px'),
                '<h5>Имя пользователя</h5>',
                AdminColumn::relatedLink('getCompetitor.getUser.name')->setLabel('Пользователь')->setWidth('200px'),
                '<h5>Номинация</h5>',
                AdminColumn::relatedLink('getNomination.name')->setLabel('Номинация')->setWidth('200px'),
                '<h5>Загружено, как</h5>',
                AdminColumn::custom()->setLabel('Загружена как')->setCallback(function ($instance) {
                    switch ($instance->is_addition) {
                        case 0:
                            return 'основное';
                        case 1:
                            return 'дополнительное';

                    }
                })->setWidth('50px'),

            ])
            ->addBody([
                AdminColumn::image('photo')->setImageWidth('300px'),
                AdminFormElement::select('photo_status_id', 'Статус')->setOptions([
                    '1' => 'загружена',
                    '2' => 'одобрена',
                    '3' => 'отклонена',
                    '4' => 'одобрено, как дополнительное',

                ]),
            ])
            ->addFooter([

            ]);
    });

    $model->updated(function (ModelConfiguration $model, Photo $photo) {
        Event::fire(new ChangePhotoStatus($photo, 'ChangeStatusPhoto'));
    });
})
    ->addMenuPage(Photo::class, 0)
    ->setIcon('fa fa-picture-o');
