<?php
/**
 * @Author: sharpeenko
 * @Date:   2017-04-07 08:34:45
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-04-08 10:44:25
 */

use App\Comment;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Comment::class, function (ModelConfiguration $model) {
    $model->enableAccessCheck(); //проверка прав доступа к модели
    $model->setTitle('Комментарии');
    $model->disableCreating();

    // Display
    $model->onDisplay(function () {
        return AdminDisplay::table()
        //->setFilters(AdminDisplayFilter::field('new_id')->setTitle('New ID [:value]'))
            ->with('author')
        //->with('getNew')
            ->setColumns([
                AdminColumn::link('content')->setLabel('Комментарий')->setWidth('500px'),
                AdminColumn::datetime('publish_at')->setLabel('Дата публикации')->setFormat('Y-m-d H:i:s')->setWidth('200px'),
                AdminColumn::relatedLink('author.name')->setLabel('Автор')->setWidth('200px'),
                AdminColumn::custom()->setLabel('Объект')->setCallback(function ($instance) {
                    if ($instance->commentable_type == 'App\News') {
                        return '<i class="fa fa-newspaper-o" aria-hidden="true"></i>    ' . $instance->commentable->header;
                    }
                    if ($instance->commentable_type == 'App\Competitor') {
                        return '<i class="fa fa-trophy" aria-hidden="true"></i>   ' . strip_tags($instance->commentable->getNomination->name);
                    }
                    if ($instance->commentable_type == 'App\Photo') {
                        return '<i class="fa fa-camera" aria-hidden="true"></i>   ' . $instance->commentable->getCompetitor->getUser->name;
                    }
                })->setWidth('50px'),
            ])->paginate(10);
        ;
    });

    // Create And Edit
    $model->onCreateAndEdit(function () {
        return AdminForm::panel()
            ->addBody([
                AdminFormElement::text('content', 'Комментарий'),
            ]);
    });
})
    ->addMenuPage(Comment::class, 0)
    ->setIcon('fa fa-commenting');
