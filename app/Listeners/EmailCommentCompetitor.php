<?php

namespace App\Listeners;

use App\Competitor;
use App\Events\AddCommentCompetitor;
use App\Notifications\AddCommentCompetitor as NotificationCommentCompetitor;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailCommentCompetitor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Отправляется оповещение пользователю, который представлен участником, используется механизм оповещений
     *
     * @param  AddCommentCompetitor  $event
     * @return void
     */
    public function handle(AddCommentCompetitor $event)
    {
        $userForNotifi = Competitor::find($event->comment->commentable_id)->getUser->id;
        $user = $event->user;
        $comment = $event->comment;
        $status = $event->status;
        Notification::send(User::find($userForNotifi), new NotificationCommentCompetitor($user, $comment, $status));
    }
}
