<?php

namespace App\Listeners;

use App\Events\AddLikePhoto;
use App\Notifications\AddLikePhoto as NotificationLikePhoto;
use App\Photo;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailLikePhoto implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Отправляется оповещение пользователю, фото которого лайкнуто))), используется механизм оповещений
     *
     * @param  AddLikePhoto  $event
     * @return void
     */
    public function handle(AddLikePhoto $event)
    {
     $userForNotifi = Photo::find($event->like->likeable_id)->getCompetitor->getUser->id;
     $user = $event->user;
     $like= $event->like;
     $status = $event->status;
     Notification::send(User::find($userForNotifi), new NotificationLikePhoto($user, $like, $status));
    }
}
