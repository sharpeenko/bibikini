<?php

namespace App\Listeners;

use App\Events\InfoModerator;
use App\Notifications\InfoModerator as NotificationInfoModerator;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailModerator implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Отправляется оповещение администратору
     *
     * @param  InfoModerator  $event
     * @return void
     */
    public function handle(InfoModerator $event)
    {
        $userForNotifi = User::whereHas('roles', function ($query) {
            $query->where('name', '=', 'admin');
        })->get();
        $user = $event->user;
        $comment = $event->comment;
        $status = $event->status;
        //dd($userInitial);
        Notification::send($userForNotifi, new NotificationInfoModerator($user, $comment, $status));
    }
}
