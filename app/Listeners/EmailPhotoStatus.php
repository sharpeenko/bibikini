<?php

namespace App\Listeners;

use App\Events\ChangePhotoStatus;
use App\Notifications\NewPhotoStatus;
use App\Photo;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailPhotoStatus implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChangePhotoStatus  $event
     * @return void
     */
    public function handle(ChangePhotoStatus $event)
    {

        $userForNotifi = Photo::find($event->photo->id)->getCompetitor->user_id;
        $photo = $event->photo;
        $status = $event->status;
        Notification::send(User::find($userForNotifi), new NewPhotoStatus($photo, $status));

    }
}
