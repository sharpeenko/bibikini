<?php

namespace App\Listeners;

use App\Events\AddCompetitorNomination;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\AddCompetitorNomination as NewCompetitor;
use App\Competitor;
use App\User;
use Notification;

class EmailCompetitorNomination implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddCompetitorNomination  $event
     * @return void
     */
    public function handle(AddCompetitorNomination $event)
    {
      $userForNotifi = Competitor::find($event->competitor->id)->getUser->id;
      $competitor = $event->competitor;
      $status = $event->status;
      Notification::send(User::find($userForNotifi), new NewCompetitor($competitor, $status));
    }
}
