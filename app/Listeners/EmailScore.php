<?php

namespace App\Listeners;

use App\Events\AddScore;
use App\Notifications\AddScore as NotificationScore;
use App\Competitor;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailScore implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddScore  $event
     * @return void
     */
    public function handle(AddScore $event)
    {
     $userForNotifi = Competitor::find($event->score->competitor_id)->getUser->id;
     $user = $event->user;
     $score = $event->score;
     $status = $event->status;
     Notification::send(User::find($userForNotifi), new NotificationScore($user, $score, $status));
    }
}
