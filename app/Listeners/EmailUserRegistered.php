<?php

namespace App\Listeners;

use App\Confirm;
use App\Events\UserRegistered;
use App\Notifications\ConfirmRegistration as NotificationConfirmRegistration;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailUserRegistered implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $confirm = $event->confirm;
        $status = $event->status;
        Notification::send(User::find($confirm->user_id), new NotificationConfirmRegistration($confirm, $status));
    }
}
