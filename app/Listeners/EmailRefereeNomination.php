<?php

namespace App\Listeners;

use App\Events\AddRefereeNomination;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\AddRefereeNomination as NewReferee;
use App\Referee;
use App\User;
use Notification;

class EmailRefereeNomination implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddRefereeNomination  $event
     * @return void
     */
    public function handle(AddRefereeNomination $event)
    {
     $userForNotifi = Referee::find($event->referee->id)->user->id;
     $referee = $event->referee;
     $status = $event->status;
     Notification::send(User::find($userForNotifi), new NewReferee($referee, $status));
    }
}
