<?php

namespace App\Listeners;

use App\Events\AddCommentPhoto;
use App\Notifications\AddCommentPhoto as NotificationCommentPhoto;
use App\Photo;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class EmailCommentPhoto implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Отправляется оповещение пользователю, фото которого прокомментировано, используется механизм оповещений
     *
     * @param  AddCommentPhoto  $event
     * @return void
     */
    public function handle(AddCommentPhoto $event)
    {
        $userForNotifi = Photo::find($event->comment->commentable_id)->getCompetitor->getUser->id;
        $user = $event->user;
        $comment = $event->comment;
        $status = $event->status;
        Notification::send(User::find($userForNotifi), new NotificationCommentPhoto($user, $comment, $status));
    }
}
