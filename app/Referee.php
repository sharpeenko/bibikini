<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * [Referee description]
 */
class Referee extends Model
{
 /**
  * Получение номинации по судье
  */
 public function nomination()
 {
     return $this->belongsTo('App\Nomination');
 }

 /**
  * Получение пользователя по судье
  */
 public function user()
 {
     return $this->belongsTo('App\User');
 }

 /**
  * Получение просмотров участников
  */
 public function review() {
    return $this->hasMany('App\Review');
 }

 /**
  * Получение оценок
  */
 public function score() {
    return $this->hasMany('App\Score');
 }

 /**
  * Получение актуальной оценки
  */
 public function scoreActual() {
    return $this->hasMany('App\Score')->where('is_actual', 1);
 }
}
