<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RatingScore extends Model
{
    protected $table = 'rating_scores';

    /**
     * [ratingScores description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function rating($id)
    {
        DB::statement(DB::raw('set @row=0'));
        $ratingScores = DB::table('rating_scores')
     ->select(DB::raw('@row:=@row+1 as row, avscore, GROUP_CONCAT(competitor_id)'))
     ->where('nomination_id', $id)
     ->groupBy('avscore')
     ->orderBy('avscore', 'desc')
     ->get();
        return  $ratingScores;
    }


    /**
     * Получение участника по рейтингу
     */
    public function competitor()
    {
        return $this->belongsTo('App\Competitor');
    }

    /**
     * Получение номинации по рейтингу
     */
    public function nomination()
    {
        return $this->belongsTo('App\Nomination');
    }



}
