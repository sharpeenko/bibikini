<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Competitor extends Model
{

    use \KodiComponents\Support\Upload;

    protected $casts = [
        'avatar' => 'image', // or file | upload
    ];

    /**
     * Получение пользователя по участнику номинации
     */
    public function getUser()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Получение номинации по участнику
     */
    public function getNomination()
    {
        return $this->belongsTo('App\Nomination', 'nomination_id');
    }

    /**
     * Получение фото по участнику
     */
    public function getPhoto()
    {
        return $this->hasMany('App\Photo');
    }


    /**
     * Получение одобренных фото по участнику
     */
    public function photos()
    {
        return $this->hasMany('App\Photo')->where('photo_status_id','=', 2);
    }

    /**
     * Получить все комментарии участника
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    /**
     * Получение просмотров судей
     */
    public function review() {
       return $this->hasMany('App\Review');
    }


    /**
     * Получение судейских оценок
     */
    public function score() {
       return $this->hasMany('App\Score');
    }



    /**
     * Получение рейтинга оценок
     */
    public function ratingScore() {
       return $this->hasOne('App\RatingScore');
    }




}
