<?php

namespace App\Policies;

use App\User;
use App\Score;
use Illuminate\Auth\Access\HandlesAuthorization;

class ScorePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function before(User $user, $ability, Score $item) {

      if ($user->can(['superadmin_access']) ) {
        return true;
      }
    }
}
