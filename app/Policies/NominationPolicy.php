<?php

namespace App\Policies;

use App\User;
use App\Nomination;
use Illuminate\Auth\Access\HandlesAuthorization;

class NominationPolicy {

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function before(User $user, $ability, Nomination $item) {

      if ($user->can(['superadmin_access', 'nomination_access']) ) {
        return true;
      }
    }

}
