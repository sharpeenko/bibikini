<?php

namespace App\Policies;

use App\Competitor;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompetitorPolicy {

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function before(User $user, $ability, Competitor $item) {

      if ($user->can(['superadmin_access']) ) {
        return true;
      }
    }

}
