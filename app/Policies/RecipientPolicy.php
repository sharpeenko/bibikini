<?php

namespace App\Policies;

use App\User;
use App\Recipient;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecipientPolicy {

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function before(User $user, $ability, Recipient $item) {

      if ($user->can(['superadmin_access']) ) {
        return true;
      }
    }

}
