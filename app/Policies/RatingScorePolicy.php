<?php

namespace App\Policies;

use App\User;
use App\RatingScore;
use Illuminate\Auth\Access\HandlesAuthorization;

class RatingScorePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before(User $user, $ability, RatingScore $item)
    {

      if ($user->can(['superadmin_access']) ) {
        return true;
      }
    }
}
