<?php

namespace App\Policies;

use App\User;
use App\News;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy {

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function before(User $user, $ability, News $item) {

      if ($user->can(['superadmin_access', 'content_access']) ) {
        return true;
      }
    }

}
