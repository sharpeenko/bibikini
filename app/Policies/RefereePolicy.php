<?php

namespace App\Policies;

use App\User;
use App\Referee;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefereePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before(User $user, $ability, Referee $item) {

      if ($user->can(['superadmin_access']) ) {
        return true;
      }
    }
}
