<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy {

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function before(User $user, $ability) {



      if ($user->can(['superadmin_access']) ) {
        return true;
      }
    }

    /**
     * Проверка, не является ли обновляющий данные судьей

     * @param User $user [description]
     */
   public function update(User $user, User $item)
   {

     // ...и проверяется его роль
    $referee_roles = ['testreferee'];

       foreach ($referee_roles as $role) {
           if ($user->hasRole($role)) {
               return false;
           }
       }
       return true;
   }

}
