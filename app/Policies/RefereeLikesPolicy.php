<?php

namespace App\Policies;

use App\User;
use App\LikeUser;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefereeLikesPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Проверка, не является ли покупатель лайков судьей
     * Метод не требует модели, поэтому передается только пользователь
     * @param User $user [description]
     */
   public function add(User $user, LikeUser $like)
   {

     // ...и проверяется его роль
    $referee_roles = ['testreferee'];

       foreach ($referee_roles as $role) {
           if ($user->hasRole($role)) {
               return false;
           }
       }
       return true;
   }
}
