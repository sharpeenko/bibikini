<?php

namespace App\Policies;

use App\User;
use App\RatingLike;
use Illuminate\Auth\Access\HandlesAuthorization;

class RatingLikePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before(User $user, $ability, RatingLike $item)
    {

      if ($user->can(['superadmin_access']) ) {
        return true;
      }
    }
}
