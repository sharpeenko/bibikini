<?php

namespace App\Policies;

use App\User;
use App\NewComment;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewCommentPolicy {

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function before(User $user, $ability, NewComment $item) {

      if ($user->can(['superadmin_access']) ) {
        return true;
      }
    }

}
