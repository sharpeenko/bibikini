<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
 /**
  * Получать все модели с лайками.
  */
 public function likeable()
 {
     return $this->morphTo();
 }


 /**
  * Получает пользователя, поставившего лайк
  * @return [type] [description]
  */
 public function user()
 {
     return $this->belongsTo('App\User');
 }

}
