<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{

 /**
  * Получение участника по оценке
  */
 public function competitor()
 {
     return $this->belongsTo('App\Competitor');
 }


 /**
  * Получение судьи по оценке
  */
 public function referee()
 {
     return $this->belongsTo('App\Referee');
 }

}
