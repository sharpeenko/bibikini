<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * Получать все модели с комментариями.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Получить автора комментария (пользователя)
     * @return [type] [description]
     */
    public function author()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
