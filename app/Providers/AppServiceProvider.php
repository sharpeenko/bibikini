<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $path = $this->app->environment() .'.app';
      $config = $this->app->make('config');
      $aliasLoader = AliasLoader::getInstance();

      if ($config->has($path)) {
          array_map([$this->app, 'register'], $config->get($path .'.providers'));

          foreach ($config->get($path .'.aliases') as $key => $class) {
              $aliasLoader->alias($key, $class);
          }
      }

    }
}
