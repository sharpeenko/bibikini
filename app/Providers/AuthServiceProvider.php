<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        \App\User::class => \App\Policies\AdminPolicy::class,
        \App\Role::class => \App\Policies\RolePolicy::class,
        \App\Recipient::class => \App\Policies\RecipientPolicy::class,
        \App\News::class => \App\Policies\NewsPolicy::class,
        \App\NewComment::class => \App\Policies\NewCommentPolicy::class,
        \App\Nomination::class => \App\Policies\NominationPolicy::class,
        \App\Competitor::class => \App\Policies\CompetitorPolicy::class,
        \App\Photo::class => \App\Policies\ModeratorPolicy::class,
        \App\Comment::class => \App\Policies\CommentPolicy::class,
        \App\Like::class => \App\Policies\LikePolicy::class,
        \App\LikeUser::class => \App\Policies\RefereeLikesPolicy::class,
        \App\Referee::class => \App\Policies\RefereePolicy::class,
        \App\Score::class => \App\Policies\ScorePolicy::class,
        \App\RatingScore::class => \App\Policies\RatingScorePolicy::class,
        \App\RatingLike::class => \App\Policies\RatingLikePolicy::class,
        \App\Permission::class => \App\Policies\PermissionPolicy::class,
        \App\Content::class => \App\Policies\ContentPolicy::class,
        \App\Seo::class => \App\Policies\SeoPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }

}
