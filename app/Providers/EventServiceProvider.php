<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider {
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'App\Events\SomeEvent' => [
			'App\Listeners\EventListener',
		],
		'App\Events\AddCommentCompetitor' => [
			'App\Listeners\EmailCommentCompetitor', // добавление комментария участнику
		],
		'App\Events\ChangePhotoStatus' => [
			'App\Listeners\EmailPhotoStatus', // смена статуса фото в админке
		],
		'App\Events\AddCommentPhoto' => [
			'App\Listeners\EmailCommentPhoto', // добавление комментария фото
		],
		'App\Events\InfoModerator' => [
			'App\Listeners\EmailModerator', // жалоба модератору
		],
		'App\Events\UserRegistered' => [
			'App\Listeners\EmailUserRegistered', // регистрация пользователя
		],
		'App\Events\AddLikePhoto' => [
			'App\Listeners\EmailLikePhoto', // добавление лайка фото
		],
		'App\Events\AddRefereeNomination' => [
			'App\Listeners\EmailRefereeNomination', // добавление судьи в номинацию
		],
		'App\Events\AddScore' => [
			'App\Listeners\EmailScore', // добавление оценки
		],
		'App\Events\AddCompetitorNomination' => [
			'App\Listeners\EmailCompetitorNomination', // добавление участника в номинацию
		],
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot() {
		parent::boot();

		//
	}
}
