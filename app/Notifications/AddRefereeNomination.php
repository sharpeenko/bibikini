<?php

namespace App\Notifications;

use App\Referee;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AddRefereeNomination extends Notification
{
    use Queueable;


    public $referee;
    public $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Referee $referee, $status)
    {
        $this->status = $status;
        $this->referee = $referee;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
     return (new BibikiniMailMessage)
         ->initialReferee($this->referee) // передача информации судье/номинации
         ->notificationStatus($this->status); // передача статуса
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
