<?php

namespace App\Notifications;

use App\Confirm;
use App\Notifications\BibikiniMailMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class ConfirmRegistration extends Notification
{
    use Queueable;

    public $confirm;
    public $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Confirm $confirm, $status)
    {
        $this->status = $status;

        $this->confirm = $confirm;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new BibikiniMailMessage)
            ->initialConfirm($this->confirm) // передача теста проверки

            ->notificationStatus($this->status); // передача статуса
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
