<?php

namespace App\Notifications;

use App\Comment;
use App\Notifications\BibikiniMailMessage;
use App\User;
//use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class InfoModerator extends Notification
{
    use Queueable;

    public $user;
    public $comment;
    public $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Comment $comment, $status)
    {
        $this->user = $user;
        $this->comment = $comment;
        $this->status = $status;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new BibikiniMailMessage)
            ->initialUser($this->user) // передача пользователя, инициатора сообщения
            ->initialComment($this->comment) // передача комментария
            ->notificationStatus($this->status); // передача статуса
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
