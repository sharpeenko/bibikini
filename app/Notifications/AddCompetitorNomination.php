<?php

namespace App\Notifications;

use App\Competitor;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AddCompetitorNomination extends Notification
{
    use Queueable;

    public $competitor;
    public $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Competitor $competitor, $status)
    {
      $this->status = $status;
      $this->competitor = $competitor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      return (new BibikiniMailMessage)
          ->initialCompetitor($this->competitor) // передача информации участнику
          ->notificationStatus($this->status); // передача статуса
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
