<?php

/**
 * @Author: sharpeenko
 * @Date:   2017-04-28 07:47:37
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-05-04 19:18:53
 */

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;

/**
 * Класс расширяет MailMessage для добавления собственных элементов в оповещение
 */
class BibikiniMailMessage extends MailMessage
{

    /**
     * Пользователь, инициатор сообщения
     */
    public $initialUser;

    public function initialUser($initialUser)
    {
        $this->initialUser = $initialUser;
        return $this;
    }

    /**
     * Комментарий, на который жалоба
     */
    public $initialComment;

    public function initialComment($initialComment)
    {
        $this->initialComment = $initialComment;
        return $this;
    }

    /**
     * Статус, в каком случае было отправлено сообщение
     */

    public $notificationStatus;

    public function notificationStatus($notificationStatus)
    {
        $this->notificationStatus = $notificationStatus;
        return $this;
    }

    /**
     * Фото, измененное в админке
     */

    public $initialPhoto;

    public function initialPhoto($initialPhoto)
    {
        $this->initialPhoto = $initialPhoto;
        return $this;
    }

    /**
     * Тест подтверждения регистрации
     */
    public $initialConfirm;

    public function initialConfirm($initialConfirm)
    {
        $this->initialConfirm = $initialConfirm;
        return $this;
    }

    /**
     * Добавленный фото лайк
     */
    public $initialLike;

    public function initialLike($initialLike)
    {
        $this->initialLike = $initialLike;
        return $this;
    }

    /**
     * Добавленный судья в номинацию
     */
    public $initialReferee;

    public function initialReferee($initialReferee)
    {
        $this->initialReferee = $initialReferee;
        return $this;
    }


    /**
     * Добавленный участник в номинацию
     */
    public $initialCompetitor;

    public function initialCompetitor($initialCompetitor)
    {
        $this->initialCompetitor = $initialCompetitor;
        return $this;
    }


    /**
     * Поставленная оценка
     */
    public $initialScore;

    public function initialScore($initialScore)
    {
        $this->initialScore = $initialScore;
        return $this;
    }

    /**
     * Get an array representation of the message.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'level' => $this->level,
            'subject' => $this->subject,
            'greeting' => $this->greeting,
            'introLines' => $this->introLines,
            'outroLines' => $this->outroLines,
            'actionText' => $this->actionText,
            'actionUrl' => $this->actionUrl,
            /**
             * Здесь начинаются собственные элементы
             */
            'initialUser' => $this->initialUser,
            'initialComment' => $this->initialComment,
            'initialPhoto' => $this->initialPhoto,
            'initialConfirm' => $this->initialConfirm,
            'notificationStatus' => $this->notificationStatus,
            'initialLike' => $this->initialLike,
            'initialReferee' => $this->initialReferee,
            'initialCompetitor' => $this->initialCompetitor,
            'initialScore' => $this->initialScore,

        ];
    }

}
