<?php

namespace App\Notifications;

use App\Like;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AddLikePhoto extends Notification
{
    use Queueable;

    public $user;
    public $like;
    public $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Like $like, $status)
    {
        $this->status = $status;
        $this->user = $user;
        $this->like = $like;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
     return (new BibikiniMailMessage)
         ->initialUser($this->user) // передача пользователя, добавившего комментарий
         ->initialLike($this->like) // передача информации о лайке
         ->notificationStatus($this->status); // передача статуса
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
