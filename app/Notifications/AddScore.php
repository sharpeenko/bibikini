<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use App\Notifications\BibikiniMailMessage;
use App\User;
use App\Score;

class AddScore extends Notification
{
    use Queueable;

    public $user;
    public $score;
    public $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Score $score, $status)
    {
     $this->status = $status;
     $this->user = $user;
     $this->score = $score;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new BibikiniMailMessage)
            ->initialUser($this->user) // передача пользователя, добавившего комментарий
            ->initialScore($this->score) // передача оценки
            ->notificationStatus($this->status); // передача статуса
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
