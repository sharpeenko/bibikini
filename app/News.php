<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    use \KodiComponents\Support\Upload;

    protected $table = 'news';
    protected $casts = [
        'pic' => 'image', // or file | upload
    ];

    /**
     * Получает комментарии к новости (старая функция, закомментирована)
     * @return [type] [description]
     */
    /*public function comments()
    {
    return $this->hasMany(NewComment::class, 'new_id');
    // return $this->hasMany(Task::class); вроде, теперь надо так
    }*/

    /**
     * Получить все комментарии новости
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

}
