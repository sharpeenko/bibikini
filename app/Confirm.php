<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confirm extends Model
{
    /**
     * Если регистрация подтверждена, возвращается true
     * @return [type] [description]
     */
    public function status($email)
    {
        if (Confirm::where('email', '=', $email)->where('status', '=', 1)->exists()) {
            return true;
        }
        return false;
    }
}
