<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewComment extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deleted_at'
    ];

    //Получает имя автора комментария (а комментарии добавляют только зарегистрированные пользователи)
    public function getAuthor() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    //Получает заголовок новости, к которой принадлежит комментарий
    public function getNew() {
        return $this->belongsTo(News::class, 'new_id');
    }

}
