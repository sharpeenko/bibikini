<?php

namespace App\Helpers;

class AppHelp {
    /*
     * Может быть использована для генерации случайного имени файла
     */

    public static function getRandomFileName($path, $extension = '') {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';

        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($file));

        return $name;
    }

    /*
     * Получение формата типа "9 декабря"
     * Параметр предварительно должен быть отформатирован YYYY-MM-DD
     * Возможности пока ограничены, планируется развивать в процессе
     */

    public static function getRussianDate($date) {
        if (!$date) {
            return '';
        }

        $date = explode("-", $date);
        switch ($date[1]) {
            case 1: $m = 'января';
                break;
            case 2: $m = 'февраля';
                break;
            case 3: $m = 'марта';
                break;
            case 4: $m = 'апреля';
                break;
            case 5: $m = 'мая';
                break;
            case 6: $m = 'июня';
                break;
            case 7: $m = 'июля';
                break;
            case 8: $m = 'августа';
                break;
            case 9: $m = 'сентября';
                break;
            case 10: $m = 'октября';
                break;
            case 11: $m = 'ноября';
                break;
            case 12: $m = 'декабря';
                break;
        }
        echo $date[2] . '&nbsp;' . $m;
    }

}
