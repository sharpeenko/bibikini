<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

use Request;

class MenuHelp {

    public static function getActiveClass($path) {
        if ((Request::is($path)) OR ( Request::is($path . '/*'))) {
            echo 'class="active"';
        }
    }

    public static function getSelectedOption($val_name, $sel_name) {
        if ($val_name == $sel_name) {
            echo 'selected';
        }
    }

}
