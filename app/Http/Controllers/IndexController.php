<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\News;
use App\Competitor;
use App\Photo;
use App\Nomination;
use Nomination as Nommi;
use App\Comment;
use App\Content;
use App\Seo;
use Auth;

class IndexController extends Controller
{
    public function index()
    {
        $user = null;
        $user_nominations = null;

        /**
         * Если пользователь авторизован, надо передать его в представление
         * @var [type]
         */
        if (Auth::check()) {
            $user = Auth::user();
            $user_nominations = Nommi::getNominations($user->id);
        }


        $users = User::whereHas('roles', function ($query) {
            $query->where('on_display', '!=', 'нет');
        })->limit(3)->orderByRaw("RAND()")->get();

        //Сео
        $seo = Seo::where('key', '=', 'seo_index')->first();

        //Контент
        $contents = Content::where('key_page', '=', 'index')->get();

        //Важные новости на главную
        $important_news = News::where('published', '=', 1)->where('is_important', '=', 1)->orderBy('publish_at', 'desc')->get();

        // Новость на главную
        $news = News::where('published', '=', 1)->limit(4)->orderBy('publish_at', 'desc')->get();

        // Участники для списка

        $competitors = Competitor::leftjoin('rating_scores as rs', 'rs.competitor_id', '=', 'competitors.id')
        ->orderBy('rs.position', 'asc')
        ->select('competitors.*')
        ->with('getUser')
        ->with('photos')
        ->with('ratingScore')
        ->limit(8)
        ->get();
        foreach ($competitors as $competitor) {
            array_add($competitor, 'name', $competitor->getUser->name);
        }

        // Фото для списка

        $photos = Photo::leftJoin('rating_likes as rl', 'rl.photo_id', '=', 'photos.id')
        ->orderBy('rl.position', 'desc')
        ->select('photos.*')
        ->with('ratingLike')
        ->with('getCompetitor')
        ->with('likes')
        ->with('comments')
        ->limit(8)
        ->where('photos.photo_status_id', '=', 2)->get();

        // Номинации для списка

        $nominations = Nomination::where('published', '=', 1)
        ->with('fundsScores')
        ->with('fundsLikes')
        ->limit(4)
        ->get();

        foreach ($nominations as $nomination) {
            if (!empty($nomination->styles)) {
                foreach (json_decode($nomination->styles, true) as $key => $value) {
                    array_add($nomination, $key, $value);
                }
            }
            $before_finish = Nommi::beforeFinish($nomination->finish);
            array_add($nomination, 'before_finish', $before_finish);

            $count_competitors = Nommi::countCompetitors($nomination->id);
            array_add($nomination, 'count_competitors', $count_competitors);

            $count_likes = Nommi::countLikesNomination($nomination->id);
            array_add($nomination, 'count_likes', $count_likes);
        }

        // Комментарии для списка

        $comments = Comment::with('author')->orderBy('publish_at', 'desc')->limit(4)->get();


        return view('index', [
                                        'user'=>$user,
                                        'user_nominations' => $user_nominations,
                                        'users' => $users,
                                        'important_news' => $important_news,
                                        'news' => $news,
                                        'competitors' => $competitors,
                                        'photos' => $photos,
                                        'nominations' => $nominations,
                                        'comments' => $comments,
                                        'contents' => $contents,
                                        'seo' => $seo,
                                      ]);
    }
}
