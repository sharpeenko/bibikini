<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RatingScore;
use App\RatingLike;
use App\Nomination;
use DB;
use App\Seo;
use App\Content;
use App\Competitor;
use Auth;

class RatingController extends Controller
{
    protected $rating;
    protected $user;
    protected $user_competitors;

    /**
     * Получает рейтинг лайков и оценок по всем номинациям, со ссылками на каждый рейтинг
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      /**
       * Если пользователь авторизован, надо передать его в представление
       * @var [type]
       */
        if (Auth::check()) {
            $this->user = Auth::user();
            $this->user_competitors = Competitor::with('ratingScore')->with('getNomination')->with('photos.ratingLike')->where('user_id', $this->user->id)->get();
        }

        /**
         * Список рейтингов лайков и оценок, сгруппированный по номинациям.
         * От них ведут ссылки на конкретные рейтинги
         */

        // Рейтинг оценок
        $nominations_ratings = Nomination::where('published', '=', 1)->with(['ratingScores' => function ($query) {
            $query->limit(3);
        }])
        //.. и лайков...
        ->with(['ratingLikes' => function ($query) {
            $query->limit(3);
        }])
        ->paginate(6);

        //Сео
        $seo = Seo::where('key', '=', 'seo_ratings')->first();

        //Контент
        $contents = Content::where('key_page', '=', 'all_ratings')->get();

        return view('rating.index', ['user'=>$this->user,  'user_competitors' => $this->user_competitors, 'nominations_ratings' => $nominations_ratings, 'seo' => $seo, 'contents' => $contents]);
    }

    /**
     * Получает рейтинг оценок по номинации
     *
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function showRatingScore(Request $request)
    {
        $nomination = Nomination::find($request->nomination);
        $ratings = RatingScore::with('competitor')->where('nomination_id', $nomination->id)->paginate(10);

        //Сео
        $seo = Seo::where('key', '=', 'seo_rating_scores')->first();
        $seo->title = strip_tags($seo->title .' '. $nomination->name);
        $seo->description = strip_tags($seo->description.' '.$nomination->introtext);

        //Контент
        $contents = Content::where('key_page', '=', 'rating_score')->get();

        return view('rating.score', ['nomination' => $nomination, 'ratings' => $ratings, 'seo' => $seo, 'contents' => $contents]);
        /**
         * Пагинация
         */
    }

    /**
     * Получает рейтинг лайков по номинации
     *
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function showRatingLike(Request $request)
    {
        $nomination = Nomination::find($request->nomination);
        $ratings = RatingLike::with('photo')->where('nomination_id', $nomination->id)->paginate(10);

        $seo = Seo::where('key', '=', 'seo_rating_lakes')->first();
        $seo->title = strip_tags($seo->title .' '. $nomination->name);
        $seo->description = strip_tags($seo->description.' '.$nomination->introtext);

        //Контент
        $contents = Content::where('key_page', '=', 'rating_likes')->get();

        return view('rating.like', ['nomination' => $nomination, 'ratings' => $ratings, 'seo' => $seo, 'contents' => $contents]);
        /**
         * Пагинация
         */
    }


    /**
     * [show description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function show($id)
    {
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Построение рейтинга лайков
     * @return [type] [description]
     */
    public function ratingLike()
    {
        //@TODO

        // Полная очистка рейтинга
        DB::table('rating_likes')->truncate();

        //Подготовка данных для выстраивания рейтинга
        $rows = DB::table('photos')
                    ->join('likes', 'photos.id', '=', 'likes.likeable_id')
                    ->select(DB::raw('photos.id as photo_id, photos.nomination_id as nomination_id, count(likes.id) as like_count'))
                    ->where('likes.likeable_type', 'like', '%Photo%')
                     ->groupBy('photos.id')
                     ->groupBy('photos.nomination_id')
                     ->orderBy('like_count', 'desc')
                     ->get();

        //Вставка
        foreach ($rows as $row) {
            DB::table('rating_likes')->insert(['photo_id' => $row->photo_id, 'nomination_id' => $row->nomination_id, 'like_count' => $row->like_count]);
        }

        //Построение рейтинга

        //Получаем открытые номинации (чтобы не тратить ресурсы на закрытые)
        $nominations = Nomination::where('is_actual', 1)->get();

        // По каждой номинации
        foreach ($nominations as $nomination) {
            $ratings = RatingLike::where('nomination_id', $nomination->id)
             ->orderBy('like_count', 'desc')->get();
            $position = 0;
            $prev_like_count = 0;
            foreach ($ratings as $rating) {
                $rating_current = RatingLike::find($rating->id);
                if ($prev_like_count != $rating_current->like_count) {
                    $position = $position + 1;
                }
                $rating_current->position = $position;
                $prev_like_count = $rating_current->like_count;
                $rating_current->save();
            }
        }
    }






    /**
     * Построение рейтинга оценок
     * @return [type] [description]
     */
    public function ratingScore()
    {

        // Полная очистка рейтинга
        DB::table('rating_scores')->truncate();

        //Подготовка данных для выстраивания рейтинга
        $rows = DB::table('scores')
                    ->select(DB::raw('competitor_id, nomination_id, avg(score) as avscore'))
                     ->where('is_actual', '=', 1)
                     ->groupBy('competitor_id')
                     ->groupBy('nomination_id')
                     ->orderBy('avscore', 'desc')
                     ->get();

        //Вставка
        foreach ($rows as $row) {
            DB::table('rating_scores')->insert(['competitor_id' => $row->competitor_id, 'nomination_id' => $row->nomination_id, 'avscore' => $row->avscore]);
        }

        //Построение рейтинга

        //Получаем открытые номинации (чтобы не тратить ресурсы на закрытые)
        $nominations = Nomination::where('is_actual', 1)->get();

        // По каждой номинации
        foreach ($nominations as $nomination) {
            $ratings = RatingScore::where('nomination_id', $nomination->id)
             ->orderBy('avscore', 'desc')->get();
            $position = 0;
            $prev_score = 0;
            foreach ($ratings as $rating) {
                $rating_current = RatingScore::find($rating->id);
                if ($prev_score != $rating_current->avscore) {
                    $position = $position + 1;
                }
                $rating_current->position = $position;
                $prev_score = $rating_current->avscore;
                $rating_current->save();
            }
        }
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
