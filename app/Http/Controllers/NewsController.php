<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsAddCommentRequest;
use App\News;
use Illuminate\Http\Request;
use Redirect;
use App\Seo;

class NewsController extends Controller
{

      /**
     * Создание нового экземпляра контроллера.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('makeup'); // Для того, чтобы никто, кроме администратора не мог открывать новости по прямой ссылке
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
         * Должен отображать все опубликованные новости
         */

        /*
         * Выборка опубликованных
         */

        $news = News::where('published', '=', 1)->orderBy('publish_at', 'DESC')->paginate(3);

        //Сео
        $seo = Seo::where('key', '=', 'seo_news')->first();
        return view('news.index', ['news' => $news, 'seo' => $seo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
         * Отдает в представление текущую новость, а также три случайных новости ("читать еще"), в которые не должна входить текущая
         * Также отдает комментарии к новости
         */

        /* "Ленивая" загрузка комментариев, цикл
        $newone = News::find($id);
        $news = News::where('published', '=', 1)->where('id', '!=', $id)->limit(3)->orderByRaw("RAND()")->get();
        $comments = $newone->comments;
        //dd($comments);
        foreach ($comments as $comment) {
        $author_name = $comment->author->name;
        //dd($author);
        $comment = array_add($comment, 'author_name', $author_name);
        $avatar = $comment->author->avatar;
        $comment = array_add($comment, 'avatar', $avatar);
        //dd($comment);
        }
         */

        /**
         * "Жадная" загрузка
         */
        $newone = News::with('comments.author')->find($id);
        $news = News::where('published', '=', 1)->where('id', '!=', $id)->limit(3)->orderByRaw("RAND()")->get();
        //Сео
        $seo = Seo::where('key', '=', 'seo_one_new')->first();
        $seo->title = strip_tags($seo->title .' '. $newone->header);
        $seo->description = strip_tags($seo->description.' '.$newone->introtext);
        return view('news.show', ['newone' => $newone, 'news' => $news, 'seo' => $seo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Добавление нового комментария к новости
     * Валидация происходит в классе валидации форм, объект которого и передактся параметром, а класс этот расширяет
     * стандартный Request
     *
     * @param  \Illuminate\Http\Request  $request - вот это уже не так
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addComment(NewsAddCommentRequest $request, $id, $user_id)
    {
        /*
         * До того, как валидация была вынесена в класс валидации форм
         */
//        $this->validate($request, [
        //            'comment' => 'required|max:500',
        //        ]);

        try {
            $comment = new \App\NewComment;
            $protect_content = addslashes($request->comment);
            $protect_content = htmlspecialchars($protect_content);
            //$protect_content = preg_replace("/[^a-z0-9]/i", "", $protect_content);
            $comment->content = $protect_content;
            $comment->new_id = $id;
            $comment->publish_at = date("Y-m-d H:i:s");
            $comment->user_id = $user_id;
            $comment->deleted_at = null;
            $comment->save();

            return Redirect::back(301)
                ->with('success', 'Комментарий успешно добавлен');
        } catch (Exception $e) {
            return Redirect::back()
                ->with('error', $e->getMessage());
        }
    }
}
