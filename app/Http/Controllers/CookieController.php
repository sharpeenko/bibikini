<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CookieController extends Controller
{

    /*
    "Больше не показывать", информер на главной
     */
    public function setCookieIndexInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-index', 1));
        return $response;
    }

    /*
    "Больше не показывать", информер списка номинаций
     */
    public function setCookieNominationInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-nomination', 1));
        return $response;
    }


    /*
    "Больше не показывать", информер списка фотогалереи
     */
    public function setCookiePhotosInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-photos', 1));
        return $response;
    }

    /*
    "Больше не показывать", информер фото
     */
    public function setCookiePhotoInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-photo', 1));
        return $response;
    }

    /*
    "Больше не показывать", информер списка участников
     */
    public function setCookieCompetitorsInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-competitors', 1));
        return $response;
    }


    /*
    "Больше не показывать", информер редактирования участника
     */
    public function setCookieCompetitorEditInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-competitor-edit', 1));
        return $response;
    }


    /*
    "Больше не показывать", информер  участника
     */
    public function setCookieCompetitorInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-competitor', 1));
        return $response;
    }


    /*
    "Больше не показывать", информер  призовых фондов
     */
    public function setCookieFundsInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-funds', 1));
        return $response;
    }

    /*
    "Больше не показывать", информер  покупки лайков
     */
    public function setCookieLikebuyInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-likebuy', 1));
        return $response;
    }


    /*
    "Больше не показывать", информер  всех рейтингов
     */
    public function setCookieRatingsInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-ratings', 1));
        return $response;
    }


    /*
    "Больше не показывать", информер  рейтинга лайков
     */
    public function setCookieRatingLikesInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-rating-likes', 1));
        return $response;
    }

    /*
    "Больше не показывать", информер  рейтинга судейских оценок
     */
    public function setCookieRatingScoresInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-rating-scores', 1));
        return $response;
    }

    /*
    "Больше не показывать", информер списка пользователей
     */
    public function setCookieUsersInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-users', 1));
        return $response;
    }

    /*
    "Больше не показывать", информер редактирования пользователя
     */
    public function setCookieUserEditInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-user-edit', 1));
        return $response;
    }


    /*
    "Больше не показывать", информер  пользователя
     */
    public function setCookieUserInformer(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('informer-user', 1));
        return $response;
    }
}
