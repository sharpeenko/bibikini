<?php

namespace App\Http\Controllers;

use Exception;
use App\LikeUser;
use App\Like;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;
use App\Photo;
use App\Events\AddLikePhoto;
use Event;
use App\Seo;
use App\Content;

class LikeController extends Controller
{


   /**
    * Конструктор
    */
    public function __construct()
    {
        /*
     * Проверка пользователя при переходе на страницу покупки лайков
     */

        $this->middleware('accessbuylikes', ['only' => [
        'buy',
    ]]);

        /*
         * Проверка пользователя при попытке поставить лайк
         */

        $this->middleware('accesssetlikes', ['only' => [
        'create',
    ]]);

        /**
         * Проверка "на владельца" при попытке просмотра "кто лайкнул"
         */

        $this->middleware('accessuserlikes', ['only' => [
         'likes',
     ]]);
    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

     // Лайки может ставить только авторизованный пользователь, это проверяет middleware, инъецированный в конструкторе

        $user_id = Auth::id();
        $photo_id = $request->photo;

        /**
         * Проверка наличия лайков у пользователя
         * @var [type]
         */
        if (!$this->checkLikeCount($user_id)) {
            return back()
          ->withErrors('Нет лайков');
        }

        // Проверка, не ставит ли пользователь лайк самому себе
        if (!$this->checkLikePhotoHimself($user_id, $photo_id)) {
            return back()
          ->withErrors('Нельзя ставить лайк самому себе');
        }


        // Проверка, не ставил ли этот пользователь лайки этому фото сегодня?
        if (!$this->checkLikePhotoToday($user_id, $photo_id)) {
            return back()
          ->withErrors('Нельзя ставить лайк одному и тому же фото в один день');
        }

        // Проверка, не является ли номинация завершенной?
        if (!$this->checkNominationClosed($photo_id)) {
            return back()
          ->withErrors('Номинация завершена');
        }


        // Все проверки прошли, лайк установлен успешно

        try {
            // Установка лайка
            $like = new Like;
            $like->user_id = $user_id;
            $like->likeable_id = $photo_id;
            $like->likeable_type = 'App\Photo'; // В будущем, возможно динамическое присвоение, потому что, возможно, лайкать можно будет не только фото
            $like->save();

            // Списывается лайк у пользователя
            $like_user= LikeUser::where('user_id', $user_id)->first();
            $like_user->like_count--;
            $like_user->deleted_at = Carbon::now(); // дата списывания, пока, похоже, нигде не используется
            $like_user->save();

            /**
             * Генерация события добавления лайка для observer
             */
            $user = User::find($user_id);
            Event::fire(new AddLikePhoto($like, $user, 'AddLikePhoto'));

            return back()
                  ->withSuccess('Лайк поставлен!');
        } catch (\Illuminate\Database\QueryException $e) {
            return back()
                  ->withErrors('Ошибка при установке лайков');
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Открытие страницы покупки лайков. Проходит через middleware, в конструкторе
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function buy($id)
    {
        $user = User::find($id);
        //Сео
        $seo = Seo::where('key', '=', 'seo_bye_likes')->first();

        //Контент
        $contents = Content::where('key_page', '=', 'like_buy')->get();

        return view('likes.like_buy', ['user' => $user, 'seo' => $seo, 'contents' => $contents]);
    }


    /**
     * Покупка лайков
     * Добваить проверки
     * @param Request $request [description]
     */
    public function add(Request $request)
    {
        $user_id = Auth::id();


        if (LikeUser::where('user_id', $user_id)->first()) {
            $like= LikeUser::where('user_id', $user_id)->first();
        } else {
            $like = new LikeUser;
        }


        // Проверка, является ли пользователь судьей?
        try {
            $this->authorize('add', $like);
        } catch (Exception $e) {
            return back()->withErrors('Судьи лайков не покупают!');
        }





        if ($this->checkLikeAdd($like)) {
            try {
                $like->user_id = $user_id;
                $like->like_count = $request->like_count;
                $like->got_at = Carbon::now(); // дата покупки
                $like->save();
                /* Передать-то надо пользователя, то бишь, объект User, да и номинацию, наверное, то же.
                 * Сначала find из моделей, по созданной, а если нет, то исключение вывести.
                 */
                $user = User::find($user_id);
                //return view('likes.like_buy', ['user' => $user])->withSuccess('Покупка лайков успешна');
                return back()
             ->withSuccess('Покупка лайков успешна');
            } catch (\Illuminate\Database\QueryException $e) {
                return back()
             ->withErrors('Ошибка при покупке лайков');
            }
        } else {
            return back()
     ->withErrors('Вы не можете покупать лайки');
        }
    }

    /**
     * Проверка при покупке лайка, на тестовый период
     * @param  LikeUser $like [description]
     * @return [type]         [description]
     */
    protected function checkLikeAdd(LikeUser $like)
    {

        // Чтобы нельзя было покупать более одного раза в сутки, при условии, что раньше лайки покупались, а если не покупались, должен разрешить покупку
        if (($like->got_at) && (Carbon::parse($like->got_at)->isToday())) {
            // если дата null, то функция isToday() возвращает true почему-то, поэтому дополнительное условие - наличие значения в поле, но главное - проверяется дата покупки
            return false;
        }

        // Чтобы нельзя было покупать, если имеются неиспользованные лайки
        if ($like->like_count > 0) {
            return false;
        }


        return true;
    }

    /**
     * Проверка наличия лайков у пользователя при попытке поставить лайк
     * @param [type] $id [description]
     */
    protected function checkLikeCount($id)
    {
        // этот пользователь покупал лайки, вопрос, есть они ук него или нет?
        if (LikeUser::where('user_id', $id)->first()) {
            $like= LikeUser::where('user_id', $id)->first();
            // У пользователя нет лайков, все израсходовал
            if ($like->like_count <= 0) {
                return false;
            }
            // Пользователь не покупал лайков
        } else {
            return false;
        }
        return true;
    }

    /**
     * Проверка, не ставит ли пользователь лайк самому себе? Только в аспекте фото
     * @param [type] $user_id  [description]
     * @param [type] $photo_id [description]
     */
    protected function checkLikePhotoHimself($user_id, $photo_id)
    {
        $photo_owner = Photo::find($photo_id)->getCompetitor->getUser->id;
        if ($user_id == $photo_owner) {
            return false;
        }
        return true;
    }


    /**
     * Проверка, не ставит ли пользователь лайк этому фото сегодня? Только в аспекте фото
    * @param  [type] $id [description]
     * @return [type]          [description]
     */
    protected function checkLikePhotoToday($user_id, $photo_id)
    {
        if (Like::where('user_id', $user_id)->where('likeable_type', 'App\Photo')->where('likeable_id', $photo_id)->whereDate('created_at', '=', Carbon::today()->toDateString())->first()) {
            // дата установки сегодняшняя
            return false;
        }
        return true;
    }

    /**
     * Проверка, не ставит ли пользователь лайк фото из завершенной номинации? Только в аспекте фото
    *
     */
    protected function checkNominationClosed($photo_id)
    {
        $nomination_status = Photo::find($photo_id)->getNomination->status;
        if ($nomination_status == 3 || $nomination_status == 2) {
            return false;
        }
        return true;
    }

    /**
     * Страница "Кто лайкнул"
     *
     * @return \Illuminate\Http\Response
     */
    public function likes(Request $request)
    {
        // Открывается только для владельца/ middleware в конструкторе

        $photo = Photo::find($request->photo);
        $likes = $photo->likes()->with('user')->paginate(20);

        //Сео
        $seo = Seo::where('key', '=', 'seo_likes')->first();

        return view('likes.likes', ['photo' => $photo, 'likes' =>$likes, 'seo' => $seo]);
    }
}
