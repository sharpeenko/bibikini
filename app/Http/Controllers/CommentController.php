<?php
/**
 * @Author: sharpeenko
 * @Date:   2017-03-28 09:25:07
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-05-02 07:36:20
 */

namespace App\Http\Controllers;

use App\Comment;
use App\Events\AddCommentCompetitor;
use App\Events\AddCommentPhoto;
use App\Events\InfoModerator;
use App\Http\Requests\InfoModeratorRequest;
use App\Http\Requests\StoreCommentRequest;
use App\User;
use Event;
use Illuminate\Http\Request;
//use Redirect;
use Exception;
use App\Referee;
use App\Competitor;
use App\Photo;

class CommentController extends Controller
{
    /**
     * Все комментарии
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Добавление комментария к новости
     */
    public function storeCommentNews(StoreCommentRequest $request)
    {
        try {
            $comment = new \App\Comment;
            $comment->commentable_id = $request->instance;
            $comment->commentable_type = 'App\News';
            $protect_content = addslashes($request->comment);
            $protect_content = htmlspecialchars($protect_content);
            //$protect_content = preg_replace("/[^a-z0-9]/i", "", $protect_content);
            $comment->content = $protect_content;
            $comment->publish_at = date("Y-m-d H:i:s");
            $comment->user_id = $request->user;
            $comment->deleted_at = null;
            $comment->save();

            return redirect()->route('news.show', $comment->commentable_id)
                ->with('success', 'Комментарий успешно добавлен');
        } catch (Exception $e) {
            redirect()->back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Добавлене комментария участнику
     * @param  StoreCommentRequest $request [description]
     * @return [type]                       [description]
     */
    public function storeCommentCompetitor(StoreCommentRequest $request)
    {

        /**
         * Проверка судьи на "свою" номинацию, должна быть в каждом методе добавления комментария
         * @var [type]
         */

        if ($this->isReferee($request->user)) {
            if (!$this->isRefereeSelfNomination($request->user, $request->instance)) {
                return back()
           ->withErrors('Судья не может комментировать чужую номинацию');
            }
        }




        try {
            $comment = new \App\Comment;
            $comment->commentable_id = $request->instance;
            $comment->commentable_type = 'App\Competitor';
            $protect_content = addslashes($request->comment);
            $protect_content = htmlspecialchars($protect_content);
            //$protect_content = preg_replace("/[^a-z0-9]/i", "", $protect_content);
            $comment->content = $protect_content;
            $comment->publish_at = date("Y-m-d H:i:s");
            $comment->user_id = $request->user;
            $comment->deleted_at = null;
            $comment->save();

            /**
             * Оповещение пользователю (по участнику), которому добавлен комментарий, пробный вариант
             * закомментирован, это была прямая отправка оповещения
             *
             */
            //$userForNotifi = Competitor::find($comment->commentable_id)->getUser->id;
            //Notification::send(User::find($userForNotifi), new AddCommentCompetitor());
            //
            //

            /**
             * Генерация события добавления комментария для observer
             */

            $user = User::find($request->user);
            Event::fire(new AddCommentCompetitor($comment, $user, 'AddCommentCompetitor'));


            /*
Маршрут зависит от того, комментирует ли участник сам себя
             */

            if (!$this->isUserSelfCompetitor($request->user, $request->instance)) {
                $route = 'competitor.show';
            } else {
                $route = 'competitor.edit';
            }



            return redirect()->route($route, $comment->commentable_id)
                ->with('success', 'Комментарий успешно добавлен');
        } catch (Exception $e) {
            redirect()->back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Добавлене комментария фото
     * @param  StoreCommentRequest $request [description]
     * @return [type]                       [description]
     */
    public function storeCommentPhoto(StoreCommentRequest $request)
    {

        /**
         * Проверка судьи на "свою" номинацию, должна быть в каждом методе добавления комментария
         * @var [type]
         */

        if ($this->isReferee($request->user)) {
            //А здесь надо получить участника
            $competitor_id = Photo::find($request->instance)->getCompetitor->id;

            if (!$this->isRefereeSelfNomination($request->user, $competitor_id)) {
                return back()
           ->withErrors('Судья не может комментировать чужую номинацию');
            }
        }

        try {
            $comment = new \App\Comment;
            $comment->commentable_id = $request->instance;
            $comment->commentable_type = 'App\Photo';
            $protect_content = addslashes($request->comment);
            $protect_content = htmlspecialchars($protect_content);
            //$protect_content = preg_replace("/[^a-z0-9]/i", "", $protect_content);
            $comment->content = $protect_content;
            $comment->publish_at = date("Y-m-d H:i:s");
            $comment->user_id = $request->user;
            $comment->deleted_at = null;
            $comment->save();

            /**
             * Генерация события добавления комментария для observer
             */
            $user = User::find($request->user);
            Event::fire(new AddCommentPhoto($comment, $user, 'AddCommentPhoto'));

            return redirect()->route('photo.show', $comment->commentable_id)
                ->with('success', 'Комментарий успешно добавлен');
        } catch (Exception $e) {
            redirect()->back()
                ->with('error', $e->getMessage());
        }
    }


    /**
     * Добавлене комментария номинации
     * @param  StoreCommentRequest $request [description]
     * @return [type]                       [description]
     */
    public function storeCommentNomination(StoreCommentRequest $request)
    {

        /**
         * Проверка судьи на "свою" номинацию, должна быть в каждом методе добавления комментария
         * @var [type]
         */

        if ($this->isReferee($request->user)) {
            if (!$this->isRefereeSelfNominationPure($request->user, $request->instance)) {
                return back()
           ->withErrors('Судья не может комментировать чужую номинацию');
            }
        }

        try {
            $comment = new \App\Comment;
            $comment->commentable_id = $request->instance;
            $comment->commentable_type = 'App\Nomination';
            $protect_content = addslashes($request->comment);
            $protect_content = htmlspecialchars($protect_content);
            //$protect_content = preg_replace("/[^a-z0-9]/i", "", $protect_content);
            $comment->content = $protect_content;
            $comment->publish_at = date("Y-m-d H:i:s");
            $comment->user_id = $request->user;
            $comment->deleted_at = null;
            $comment->save();



            return redirect()->route('nomination.show', $comment->commentable_id)
                ->with('success', 'Комментарий успешно добавлен');
        } catch (Exception $e) {
            redirect()->back()
                ->with('error', $e->getMessage());
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Жалоба модератору на комментарий
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function infoModerator(InfoModeratorRequest $request)
    {
        //Класс валидации

        try {

            /**
             * Генерация события отправки письма модератору
             */
            $comment = Comment::find($request->comment);
            $user = User::find($request->user); // инициатор жалобы
            Event::fire(new InfoModerator($comment, $user, 'InfoModerator'));

            return redirect()->back()
                ->with('success', 'Жалоба отправлена');
        } catch (Exception $e) {
            redirect()->back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Проверяет, не является ли комментирующий судьей?
     *
     */
    protected function isReferee($id)
    {
        $user = User::find($id);
        $referee_roles = ['testreferee'];

        foreach ($referee_roles as $role) {
            if ($user->hasRole($role)) {
                return true;
            }
        }
    }

    /**
     *
     * Проверяет судью на "свою" номинацию (для этого передается участник)
     */
    protected function isRefereeSelfNomination($user_id, $competitor_id)
    {
        $user = User::find($user_id);
        // смотрим, "своя" ли это номинация?
        $referee = Referee::where('user_id', $user->id)->first();
        $competitor = Competitor::find($competitor_id);
        return ($referee->nomination_id == $competitor->nomination_id);
    }

    /**
     *
     * Проверяет судью на "свою" номинацию (вариант для комментария к номинации, передается номинация)
     */

    protected function isRefereeSelfNominationPure($user_id, $nomination_id)
    {
        $user = User::find($user_id);
        // смотрим, "своя" ли это номинация?
        $referee = Referee::where('user_id', $user->id)->first();
        return ($referee->nomination_id == $nomination_id);
    }

    /*
Проверяет, является ли пользователь "владельцем" участника
     */
    protected function isUserSelfCompetitor($user_id, $competitor_id)
    {
        $competitor = Competitor::find($competitor_id);
        return ($user_id == $competitor->user_id);
    }
}
