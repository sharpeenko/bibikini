<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nomination;
use App\Seo;
use App\Content;

class FundController extends Controller
{
    /**
     * Призовые фонды по всем номинациям
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $funds = Nomination::where('published', '=', 1)
        ->with('fundsScores')
        ->with('fundsLikes')
        ->paginate(2);

        //Сео
        $seo = Seo::where('key', '=', 'seo_funds')->first();

        //Контент
        $contents = Content::where('key_page', '=', 'all_funds')->get();

        return view('fund.index', ['funds' => $funds, 'seo' => $seo, 'contents' => $contents]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
