<?php

/**
 * @Author: sharpeenko
 * @Date:   2017-01-30 08:37:50
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-03-21 20:00:45
 */

namespace App\Http\Controllers;

use App\Nomination;
use App\Photo;
use App\Competitor;
use DB;
use Illuminate\Http\Request;
use Nomination as Nommi;
use App\Seo;
use App\Content;
use Auth;
use App\User;

class NominationController extends Controller
{


  protected $user;
  protected $user_competitors;
  protected $user_photos;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('nominationaccess', ['only' => [
            'show',
        ],
        ]);

      }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*
     * Должен отображать все опубликованные Номинации
     *
     */
    public function index()
    {

        /*
         * Выборка опубликованных
         */

        $nominations = Nomination::where('published', '=', 1)
        ->with('fundsScores')
        ->with('fundsLikes')
        ->paginate(6);

        foreach ($nominations as $nomination) {
            if (!empty($nomination->styles)) {
                foreach (json_decode($nomination->styles, true) as $key => $value) {
                    array_add($nomination, $key, $value);
                }
            }
            $before_finish = Nommi::beforeFinish($nomination->finish);
            array_add($nomination, 'before_finish', $before_finish);

            $count_competitors = Nommi::countCompetitors($nomination->id);
            array_add($nomination, 'count_competitors', $count_competitors);

            $count_likes = Nommi::countLikesNomination($nomination->id);
            array_add($nomination, 'count_likes', $count_likes);
        }

        //Сео
        $seo = Seo::where('key', '=', 'seo_nominations')->first();

        //Контент
        $contents = Content::where('key_page', '=', 'all_nominations')->get();


        return view('nomination.all_nominations', ['nominations' => $nominations, 'seo' => $seo, 'contents' => $contents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        /*
         * Отдает в представление текущую номинацию, вычисляет интервал старт-финиш
         * Если заходят гость или пользователь, который не является участником номинации,
         * то отображается кнопка "Участвовать", а если участник, то соответствующий функционал
         * @TODO: Тестировать результат подачи заявки
         * - номинация открыта, тогда меняется кнопка "Участвовать" при повторном открытии номинации
         * - если номинация закрыта, сообщение об ошибке
         */

        $nomination = Nomination::with('competitors')
        ->with('fundsScores')
        ->with('fundsLikes')
        ->with(['ratingScores' => function ($query) {
            $query->orderBy('position','asc')->limit(7);
        }])
        ->with(['ratingLikes' => function ($query) {
            $query->orderBy('position','asc')->limit(7);
        }])
        ->with('referees')
        ->find($id);
        if (!empty($nomination->styles)) {
            foreach (json_decode($nomination->styles, true) as $key => $value) {
                array_add($nomination, $key, $value);
            }
        }
        $before_finish = Nommi::beforeFinish($nomination->finish);
        array_add($nomination, 'before_finish', $before_finish);

        $count_competitors = Nommi::countCompetitors($nomination->id);
        array_add($nomination, 'count_competitors', $count_competitors);
        $count_photos = Nommi::countPhotos($nomination->id);
        array_add($nomination, 'count_photos', $count_photos);

        foreach($nomination->referees as $referee) {
         //По каждому судье получаем среднюю судейскую оценку
             $avgScore = $this->avgScore($referee->id);
             array_add($referee, 'avg_score', $avgScore);
        }

        // Участники для превью

        $competitors = Competitor::leftjoin('rating_scores as rs', 'rs.competitor_id', '=', 'competitors.id')
        ->orderBy('rs.position', 'asc')
        ->select('competitors.*')
        ->with('getUser')
        ->with('photos')
        ->with('ratingScore')
        ->where('competitors.nomination_id', $nomination->id)
        ->limit(8)
        ->get();

        foreach ($competitors as $competitor) {
            array_add($competitor, 'name', $competitor->getUser->name);
        }


        // Фото для превью

        $photos = Photo::leftJoin('rating_likes as rl', 'rl.photo_id', '=', 'photos.id')
        ->orderBy('rl.position', 'desc')
        ->select('photos.*')
        ->with('ratingLike')
        ->with('getCompetitor')
        ->with('likes')
        ->with('comments')
        ->where('photos.nomination_id', $nomination->id)
        ->limit(8)
        ->where('photos.photo_status_id', '=', 2)->get();

        //Контент
        $contents = Content::where('key_page', '=', 'nomination')->get();

        //Сео
        $seo = Seo::where('key', '=', 'seo_one_nomination')->first();
        $seo->title = strip_tags($seo->title .' '. $nomination->name);
        $seo->description = strip_tags($seo->description.' '.$nomination->introtext);
        return view('nomination.show', ['nomination' => $nomination, 'seo' => $seo, 'contents' => $contents, 'competitors' => $competitors, 'photos' => $photos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//
    }



    /*
     * Получение всех участников
     */

    public function getCompetitors(Request $request)
    {

      /**
       * Если пользователь авторизован, надо передать его в представление
       * @var [type]
       */
      if (Auth::check())
        {
          $this->user = Auth::user();
          $this->user_competitors = $this->user->competitors;
       }

        $id = $request->nomination;

        /*
         * Кажется, eager load здесь не нужна, видимо, она актуальна для вывода в popover-ы, в списках номинаций/
         *
         * Нет, все-таки, именно так. Хотя нет, запрос получается какой-то проблемный
         */

        //$nomination = Nomination::with('competitors')->find($id);
        //dd($nomination);

        /*
         * Лучше обычным способом.
         *
         * Похоже, не лучше, потому что надо передавать и объект номинации
         */

        //$competitors = Nomination::find($id)->competitors;

        /*
         * А вот через построитель запросов
         *
         *
         */

//        $nomination = DB::table('nominations')
        //                ->join('competitors', 'nominations.id', '=', 'competitors.nomination_id')
        //                ->join('users', 'competitors.user_id', '=', 'users.id')
        //                ->select('users.*', 'nominations.*', 'competitors.*')
        //                ->where('nominations.id', '=', $id)
        //                ->get();

       /* $nomination = Nomination::find($id);

        $competitors = DB::table('nominations')
            ->join('competitors', 'nominations.id', '=', 'competitors.nomination_id')
            ->join('users', 'competitors.user_id', '=', 'users.id')
            ->select('users.name', 'competitors.*')
            ->where('nominations.id', '=', $id)
            ->paginate(1);*/

        //dd($competitors);
        //

        $nomination = Nomination::find($id);
        //$competitors = Competitor::where('nomination_id', $nomination->id)->with('photos')->paginate(2);
        $competitors = Competitor::leftjoin('rating_scores as rs', 'rs.competitor_id', '=', 'competitors.id')
        ->where('competitors.nomination_id', $nomination->id)
        ->orderBy('rs.position', 'asc')
        ->select('competitors.*')
        ->with('getUser')
        ->with('photos')
        ->with('ratingScore')
        //->
        ->paginate(18);
        foreach ($competitors as $competitor) {
            array_add($competitor, 'name', $competitor->getUser->name);

        }


        //Сео
        $seo = Seo::where('key', '=', 'seo_nomination_competitors')->first();
        $seo->title = strip_tags($seo->title .' '. $nomination->name);
        $seo->description = strip_tags($seo->description.' '.$nomination->introtext);


        //Контент
        $contents = Content::where('key_page', '=', 'all_competitors')->get();

        return view('competitor.nomination_competitors', ['nomination' => $nomination, 'competitors' => $competitors, 'seo' => $seo, 'contents' => $contents, 'user' => $this->user, 'user_competitors' => $this->user_competitors]);
    }

    public function showDescription($id)
    {
        $nomination = Nomination::find($id);
        $seo = Seo::where('key', '=', 'seo_nomination_description')->first();
        $seo->title = strip_tags($seo->title .' '. $nomination->name);
        $seo->description = strip_tags($seo->description.' '.$nomination->introtext);
        return view('nomination.description', ['nomination' => $nomination, 'seo' => $seo]);
    }

    /*
     * Получение всех фото
     */

    public function photos(Request $request)
    {

        $id = $request->nomination;
        $nomination = Nomination::find($id);

        /**
         * Если пользователь авторизован, надо передать его в представление
         * @var [type]
         */
        if (Auth::check())
          {
            $this->user = User::with('competitors.photos')->find(Auth::user()->id);
         }

        //$photos = Nomination::find($id)->photos()->where('photo_status_id', '=', 2)->with('getCompetitor')->with('getUser')->paginate(3);


       /* $photos = DB::table('nominations')
            ->join('photos', 'nominations.id', '=', 'photos.nomination_id')
            ->join('competitors', 'photos.competitor_id', '=', 'competitors.id')
            ->join('users', 'competitors.user_id', '=', 'users.id')
            ->select('users.name', 'photos.*','competitors.id as competitor')
            ->where('photos.photo_status_id', '=', 2)
            ->where('nominations.id', '=', $id)
            ->paginate(3);*/

          $photos = Photo::where('photo_status_id', '=', 2)->where('nomination_id', $id)
           ->with('getCompetitor.getUser')
           ->with('likes')
           ->with('comments')
          ->paginate(18);

          $seo = Seo::where('key', '=', 'seo_nomination_photos')->first();
          $seo->title = strip_tags($seo->title .' '. $nomination->name);
          $seo->description = strip_tags($seo->description.' '.$nomination->introtext);

          //Контент
        $contents = Content::where('key_page', '=', 'all_photos')->get();

        return view('nomination.photos', ['nomination' => $nomination, 'photos' => $photos, 'seo' => $seo, 'contents' => $contents, 'user' => $this->user]);
    }


    /**
     *   Получение средней судейской оценки
      * Передается идентификатор судьи
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
   protected function avgScore($id) {
     $avgScore = DB::table('scores')
     ->select(DB::raw('round(AVG(score), 2) as avgscore'))
     ->where('referee_id', $id)
     ->where('is_actual', 1)
     ->first();
     return  $avgScore;
   }
}
