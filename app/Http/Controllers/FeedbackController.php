<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use Input;
use App\Feedback;
use App\Recipient;
use AppHelper;
use App\Seo;

class FeedbackController extends Controller
{
    /*
     * Выводит представление, ничего больше
     */

    public function index()
    {
        //Сео
        $seo = Seo::where('key', '=', 'seo_feedback')->first();
        return view('feedback.index', ['seo' => $seo]);
    }

    //Отправка письма


    public function send(Request $request)
    {
        $this->validate($request, [
            'name' => 'max:100',
            'email' => 'required|email',
            'messmail' => 'required|max:1000',
            'attachment' => 'mimes:jpeg,jpg,png,gif|max:1000' // max 1000kb
        ]);

        $messmail = strip_tags($request->messmail); // удаление всех html-тегов
        $messmail = htmlentities($messmail); //преобразует все html-сущности

        $sent = Mail::send('mail.feedback_send', array('email' => $request->email, 'name' => $request->name, 'messmail' => $messmail), function ($m) use ($request) {
            //$m->from('admin@bibikini.com.dev', 'Администрация Bibikini');

            $recipients_all = Recipient::all();
            $recipients = array();
            foreach ($recipients_all as $value) {
                array_push($recipients, $value->email);
            }

            $m->to($recipients, 'Администрация Bibikini')->subject('Сообщение формы обратной связи');
            $m->replyTo($request->email);



            if (Input::hasFile('attachment')) {
                $file = Input::file('attachment');
                $extension = $file->getClientOriginalExtension();
                $filepath = public_path() . '/assets/attachments/';
                $filename = AppHelper::getRandomFileName($filepath, $extension);
                $filename = $filename . '.' . $extension;
                $file->move($filepath, $filename);
                $attach_path = asset('assets/attachments/' . $filename);
                $m->attach($attach_path);
            }

            $this->store($request);
        });

        if ($sent === 0) {
            return redirect()->back()
                            ->withErrors('Ошибка отправки сообщения.');
        }
        return redirect()->back()
                        ->withSuccess('Сообщение успешно отправлено');
    }

    /*
     * Сохранение сообщения в БД
     */

    public function store(Request $request)
    {
        $feedback = new Feedback;
        $feedback->email = $request->email;
        $feedback->name = $request->name;
        $messmail = strip_tags($request->messmail); // удаление всех html-тегов
        $messmail = htmlentities($messmail); //преобразует все html-сущности
        $feedback->message = $messmail;
        $feedback->save();
    }
}
