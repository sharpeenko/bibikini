<?php

namespace App\Http\Controllers;

use App\Content;
use App\Seo;

use Illuminate\Http\Request;

class ContentController extends Controller
{
    /*
     * Согласие на хранение и обработку персональных данных
     */

    public function showPersonalData()
    {

    //Контент
        $content = Content::where('key_page', '=', 'personal_data')->first();
        //Сео
        $seo = Seo::where('key', '=', 'seo_personal_data_agree')->first();
        return view('personal_data.index', ['content' => $content, 'seo' => $seo]);
    }


    /*
     * Справка
     */

    public function showHelp()
    {

    //Контент
        $content = Content::where('key_page', '=', 'help')->first();
        //Сео
        $seo = Seo::where('key', '=', 'seo_help')->first();
        return view('info.help', ['content' => $content, 'seo' => $seo]);
    }

    /*
     * Подробное описание ресурса
     */

    public function showSiteDescription()
    {

    //Контент
        $content = Content::where('key_page', '=', 'bibikini_info')->first();
        //Сео
        $seo = Seo::where('key', '=', 'seo_bibikini_description')->first();
        return view('info.site_description', ['content' => $content, 'seo' => $seo]);
    }


    /*
     * Пользовательское соглашение
     */

    public function showUserAgreement()
    {

    //Контент
        $content = Content::where('key_page', '=', 'user_agreement')->first();
        //Сео
        $seo = Seo::where('key', '=', 'seo_user_agreement')->first();
        return view('info.user_agreement', ['content' => $content, 'seo' => $seo]);
    }
}
