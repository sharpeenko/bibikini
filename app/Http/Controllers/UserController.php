<?php

/**
 * @Author: sharpeenko
 * @Date:   2017-01-31 19:31:52
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-04-06 18:14:00
 */

namespace App\Http\Controllers;

use App\User;
use DB;
use Illuminate\Http\Request;
use Input;
use Nomination;
use Redirect;
use Exception;
use App\Seo;
use App\Content;

class UserController extends Controller
{
    protected $user;

    /**
     * Конструктор
     * @param User $user [Инъекция модели, как зависимости]
     */
    public function __construct(User $user) //Объект класса модели передается в конструктор
    {
        $this->user = $user; // инъекция

        /*
         * Проверка пользователя при посещениии страницы профиля
         * TODO: посмотреть, можно ли тестировать middleware
         */
        $this->middleware('userprofileaccess', ['only' => [
            'show',
        ]]);

        /*
         * Проверка пользователя при переходе по прямой ссылке
         */

        $this->middleware('accessrouteuseredit', ['only' => [
            'edit',
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * TODO: Этот метод протестировать
     */
    public function index()
    {
        /*
         * Должен отображать всех пользователей, за исключением забаненных
         */

        /*
         * Решение выборки по роли с помощью построителя запросов (оставлю для истории)
         */
//        $users = DB::table('users')
        //                ->join('role_user', 'users.id', '=', 'role_user.user_id')
        //                ->where('role_user.role_id', '!=', 4)
        //                ->get();
        /*
         * Решение выборки по роли с помощью отношений, проверка существования
         */

        /**
         * В целях успешного написания тестов на саму модель Eloquent лучше не ссылаться,
         */
        //$users = User::whereHas('roles', function ($query) {
        //$query->where('on_display', '!=', 'нет');
        //})->paginate(3);

        /**
         * Новый вариант: Модель в конструкторе встроена, как зависимость, теперь ее не надо вызывать напрямую
         * @var [type]
         */
        $users = $this->user->whereHas('roles', function ($query) {
            $query->where('on_display', '!=', 'нет');
        })
        ->with('competitors.photos')
        ->with('comments')
        ->paginate(18);

        //Сео
        $seo = Seo::where('key', '=', 'seo_users')->first();

        //Контент
        $contents = Content::where('key_page', '=', 'all_users')->get();

        return view('users.index', ['users' => $users, 'seo' => $seo, 'contents' => $contents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Отображает страницу пользователя для просмотра
     * @param  [type] $id [id пользователя]
     * @return [type]     [description]
     * TODO: Этот метод протестировать, предварительно заменив прямые вызовы модели
     */
    public function show($id)
    {
        //Вызов модели напрямую больше не применяется
        //$user = User::find($id);
        //Вместо этого
        $user = $this->user->with(['comments' => function ($query) {
            $query->limit(5);
        }])
          ->with(['competitors.getPhoto' => function ($query) {
              $query->whereIn('photo_status_id', [2]); //для владельца выбираются все фото, в том числе и отклоненные
          }])->find($id);
        //TODO: здесь прямое обращение к модели, чего следует избегать
        $nominations = Nomination::getNominations($id);

        //Сео
        $seo = Seo::where('key', '=', 'seo_one_user')->first();
        $seo->title = strip_tags($seo->title .' '. $user->name);
        $seo->description = strip_tags($seo->description.' '.$user->name);

        //Контент
        $contents = Content::where('key_page', '=', 'user_profile')->get();

        return view('users.profile', ['user' => $user, 'nominations' => $nominations, 'seo' => $seo, 'contents' => $contents]);
    }

    /**
     * Отображает страницу пользователя для редактирования
     * @param  [type] $id [id пользователя]
     * @return [type]     [description]
     * TODO: Этот метод протестировать, предварительно заменив прямые вызовы модели
     */
    public function edit($id)
    {
        $user = $this->user->with(['comments' => function ($query) {
            $query->limit(5);
        }])
          ->with(['competitors.getPhoto' => function ($query) {
              $query->whereIn('photo_status_id', [1, 2, 3]); //для владельца выбираются все фото, в том числе и отклоненные
          }])->with('likes')->find($id);
        $nominations = Nomination::getNominations($id);
        //Сео
        $seo = Seo::where('key', '=', 'seo_one_user')->first();
        $seo->title = strip_tags($seo->title .' '. $user->name);
        $seo->description = strip_tags($seo->description.' '.$user->name);

        //Контент
        $contents = Content::where('key_page', '=', 'user_edit')->get();

        return view('users.edit', ['user' => $user, 'nominations' => $nominations, 'seo' => $seo, 'contents' => $contents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     * Редактирование владельцем собственного профиля
     * Все прозрачно, выбор обновления в зависимости от загрузки аватара - есть или нет?
     *
     * TODO: Этот метод протестировать, предварительно заменив прямые вызовы модели
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'sex' => 'required|in:мужской,женский',
            'avatar' => 'mimes:jpeg,jpg,png,gif|max:1000',
        ]);

        try {
            $avatar_path = '';

            $user = User::find($id);

            // Проверка, является ли пользователь судьей?
            try {
                $this->authorize('update', $user);
            } catch (Exception $e) {
                return back()->withErrors('Судьи свои данные не редактируют!');
            }

            if (Input::hasFile('avatar')) {
                $file = Input::file('avatar');
                $extension = $file->getClientOriginalExtension();
                $filepath = public_path() . '/assets/img/users/' . $id . '/';
                $filename = 'ava' . $id . '.' . $extension;
                $avatar_path = asset('assets/img/users/' . $id . '/' . $filename);
                $file->move($filepath, $filename);
            }

            if ($avatar_path) {
                $user->name = $request->name;
                $user->sex = $request->sex;
                $user->avatar = $avatar_path;
                $user->save();
            } else {
                $user->name = $request->name;
                $user->sex = $request->sex;
                $user->save();
            }

            return Redirect::back(301)
                ->with('success', 'Данные успешно обновлены');
        } catch (Exception $e) {
            return Redirect::back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
     * Поиск пользователей
     * Не должен отображать забаненных[
     *
     * TODO: Этот метод протестировать
     */
    public function search(Request $request)
    {
        $query = $request->input('search');
        $users = DB::table('users')
            ->select('users.name', 'users.id', 'users.avatar')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where('users.name', 'LIKE', '%' . $query . '%')
            ->where('roles.on_display', '!=', 'нет')
            ->paginate(9);
        return view('users.index', ['users' => $users]);
    }

    /**
     * Смена пароля
     * @param  Request $request [description]
     * @param  [type]  $id      [id пользователя]
     * @return [type]           [description]
     *
     * TODO: Этот метод протестировать, предварительно заменив прямые вызовы модели
     */
    public function changePassword(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        try {
            $user = User::find($id);
            $user->password = bcrypt($request['password']);
            $user->save();
            return Redirect::back(301)
                ->with('success', 'Пароль успешно изменен');
        } catch (Exception $ex) {
            return Redirect::back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Показывает представление со всеми комментариями новостей пользователя.
     *
     * @param  int  $id
     *
     * TODO: Больше не действует, после того, как идеология комментариев будет переработана, удалить
     *
     */
    public function newsComments($id)
    {
        $user = User::find($id);
        $user_comment_news = User::find($id)->getUserCommentNews()->orderBy('publish_at', 'desc')->paginate(5);
        foreach ($user_comment_news as $comment) {
            $header = $comment->getNew->header;
            $comment = array_add($comment, 'header', $header);
            $new_id = $comment->getNew->id;
            $comment = array_add($comment, 'new_id', $new_id);
        }
        return view('users.newscomments', ['user' => $user, 'user_comment_news' => $user_comment_news]);
    }

    public function comments($id)
    {
        $user = User::find($id);
        $comments = $user->comments()->paginate(20);
        //Сео
        $seo = Seo::where('key', '=', 'seo_comments_user')->first();
        $seo->title = strip_tags($seo->title .' '.$user->name);
        $seo->description = strip_tags($seo->description.' '.$user->name);
        return view('users.comments', ['user' => $user, 'comments' => $comments, 'seo' => $seo]);
    }

    /**
     * Вывод списка пользователей в popover
     * Должен отображать всех пользователей, за исключением забаненных
     *
     * @return [type] [description]
     *
     * TODO: Этот метод протестировать, предварительно заменив прямые вызовы модели
     */
    public function testAjax()
    {
        $users = User::all();

        return view('users.testajax', ['users' => $users]);
    }

    /**
     * Отображение всех номинаций по пользователю
     * @param  [type] $id [id пользователя]
     * @return [type]     [description]
     *
     * TODO: Этот метод протестировать, предварительно заменив прямые вызовы модели
     */
    public function getUserNominations($id)
    {
        $user = User::find($id);
        $nominations = Nomination::getNominations($id);


        //Сео
        $seo = Seo::where('key', '=', 'seo_nominations_user')->first();
        $seo->title = strip_tags($seo->title .' '. $user->name);
        $seo->description = strip_tags($seo->description.' '.$user->name);
        return view('nomination.user_nominations', ['user' => $user, 'nominations' => $nominations, 'seo' => $seo]);
    }
}
