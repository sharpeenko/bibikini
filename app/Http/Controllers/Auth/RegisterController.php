<?php

namespace App\Http\Controllers\Auth;

use App\Confirm;
use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use App\User;
use Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Validator;
use App\Seo;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Метод трейта переопределен.
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        /**
         * Прикрепление созданному пользователю роли по умолчанию
         */
        if ($user) {
            $user->attachRole(2); // Пока что тестовый айди
        }

        /**
         * Создание записи проверки регисирации в Confirm
         * Генерация события для оповещения - отправки письма для подтверждения регистрации
         */
        if ($user) {
            $confirm = new Confirm();
            $confirm->email = $user->email;
            $confirm->user_id = $user->id;
            $confirm->activation_code = md5($user->email);
            $confirm->save();

            Event::fire(new UserRegistered($confirm, 'UserRegistered'));
        }



        /**
         * Это, наверное, надо будет закомментировать
         */
        //$this->guard()->login($user); Теперь логина при регистрации нет
        //

        return redirect($this->redirectPath())->withSuccess('На указанный адрес отправлено письмо для подтверждения регистрации.');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'avatar' => 'default', //аватар по умолчанию
        ]);
    }

    /**
     * Подтверждение регистрации
     * @return [type] [description]
     */
    public function confirmRegistration(Request $request)
    {
        $confirm = Confirm::findOrFail($request->id);

        if ($confirm) {
            if ($confirm->activation_code == $request->code && $confirm->status == 0) {
                $confirm->status = 1;
                $confirm->save();

                $user = User::findOrFail($confirm->user_id);
                $this->guard()->login($user);
                return redirect($this->redirectPath())->withSuccess('Учетная запись успешно подтверждена.');
            } else {
                return redirect($this->redirectPath())->withErrors('Ошибка подтверждения учетной записи.');
            }
        }
    }

    /**
     * Переопределяется метод для добавления SEO
     * @return [type] [description]
     */
    public function showRegistrationForm()
    {
        //Сео
        $seo = Seo::where('key', '=', 'seo_register')->first();
        return view('auth.register', ['seo' => $seo]);
    }
}
