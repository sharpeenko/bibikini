<?php

namespace App\Http\Controllers\Auth;

use App\Confirm;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Lang;
use App\Seo;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Переопределил метод, добавил вариант логина через ajax, чтобы выводить ошибки в форму
     * модального окна
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        if ($request->ajax()) {
            //Сделан ли запрос через ajax
            return response()->json([
                'error' => Lang::get('auth.failed'),
            ], 401);
        }

        /*
         * А если не черз ajax, обычный редирект на страницу входа с отображением
         * ошибок
         */

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }

    /**
     * Переопределяю метод для добавления seo
     * @return [type] [description]
     */
    public function showLoginForm()
    {
        //Сео
        $seo = Seo::where('key', '=', 'seo_login')->first();
        return view('auth.login', ['seo' => $seo]);
    }

    /**
     * Переопределенный метод логина, так как планируется добавить в него условие проверки статуса, но пока что - тестово
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        /**
         * Возвращение ошибки, если пользователь не активирован
         */
        if (!$this->statusLogin($request)) {
            return $this->sendFailedLoginResponse($request);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Проверка статуса, активирован ли пользователь?
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    protected function statusLogin(Request $request)
    {
        $email = $request->email;
        $confirm = new Confirm();
        return $confirm->status($email);
    }
}
