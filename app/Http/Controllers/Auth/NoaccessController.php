<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Seo;

class NoaccessController extends Controller
{
    public function noaccess()
    {
        //Сео
        $seo = Seo::where('key', '=', 'seo_noaccess')->first();
        return view('auth.noaccess', ['seo' => $seo]);
    }
}
