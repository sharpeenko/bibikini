<?php

/**
 * @Author: sharpeenko
 * @Date:   2017-03-09 08:31:57
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-04-05 10:40:12
 */

namespace App\Http\Controllers;

use App\Http\Requests\StorePhotoRequest;
use App\Nomination;
use App\Photo;
use App\RatingLike;
use Illuminate\Http\Request;
use Nomination as Nommi;
use Exception;
use App\Seo;
use App\Content;
use Auth;
use App\User;

class PhotoController extends Controller
{
    protected $user;
    protected $user_photos;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::check()) {
            $this->user = User::with('competitors.photos')->find(Auth::user()->id);
        }


        /**
         * Если по get-запросу
         */
        if (!($request->has('nomination'))) {
            //$photos = Photo::where('photos.photo_status_id', '=', //2)->with('getCompetitor')->with('likes')->with('comments')->paginate(9);


            $photos = Photo::leftJoin('rating_likes as rl', 'rl.photo_id', '=', 'photos.id')
            ->orderBy('rl.position', 'desc')
            ->select('photos.*')
            ->with('ratingLike')
            ->with('getCompetitor')
            ->with('getNomination')
            ->with('likes')
            ->with('comments')
            ->where('photos.photo_status_id', '=', 2)->paginate(18);



            $nominations = Nomination::where('published', '=', 1)->get();

            $seo = Seo::where('key', '=', 'seo_photos')->first();


            //Контент
            $contents = Content::where('key_page', '=', 'all_photos')->get();

            return view('photo.index', ['photos' => $photos, 'nominations' => $nominations, 'seo' => $seo, 'contents' => $contents, 'user' => $this->user]);
        }

        /**
         * Если post-запрос из поиска по номинации
         *
         */
        $query = $request->input('nomination');
        /*$photos = Photo::where('photos.photo_status_id', '=', 2)
            ->where('nomination_id', '=', $query)
            ->with('getCompetitor')
            ->with('comments')
            ->with('likes')
            ->paginate(9);*/

        $photos = Photo::leftJoin('rating_likes as rl', 'rl.photo_id', '=', 'photos.id')
            ->orderBy('rl.position', 'desc')
            ->select('photos.*')
            ->with('ratingLike')
            ->with('getCompetitor')
            ->with('getNomination')
            ->with('likes')
            ->with('comments')
            ->where('photos.photo_status_id', '=', 2)
            ->where('photos.nomination_id', '=', $query)
            ->paginate(18);

        $nominations = Nomination::where('published', '=', 1)->get();

        $nomination_current = Nomination::find($query);

        //Сео
        $seo = Seo::where('key', '=', 'seo_photos')->first();
        $seo->title = strip_tags($seo->title .' '. $nomination_current->name);
        $seo->description = strip_tags($seo->description.' '.$nomination_current->introtext);

        //Контент
        $contents = Content::where('key_page', '=', 'all_photos')->get();

        return view('photo.index', ['photos' => $photos, 'nominations' => $nominations, 'nomination_current' => $nomination_current, 'seo' => $seo, 'contents' => $contents, 'user' => $this->user, 'user_photos' => $this->user_photos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Загрузка фото, возвращает ответ в ajax-запрос
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePhotoRequest $request)
    {

        /**
         * Проверка баланса перед загрузкой, и если достигнуто предельное количество, прервать загрузку,
         * с соответствующим сообщением о возможности загрузки дополнительных фото
         */
        $photo_count = Nommi::photoCount($request->nomination);
        $photo_correct_count = Nommi::photoCorrectCount($request->competitor);
        $photo_balance = Nommi::photoBalance($photo_count, $photo_correct_count);

        if ($photo_balance == 0) {
            $photo_message = '<p>Достигнуто предельное количество загруженных фото. Желаете загрузить фото дополнительно?</p>
            <p class="bg-danger text-danger">Не рекомендуем использовать дополнительные фото, если еще остались фото на проверке!</p>
            <p class="text-center"><button class="btn btn-primary" onclick="ChangeUploadPhotoAddition();">Да, желаю</button></p>';

            return response()->json(['photo_count' => $photo_count, 'photo_balance' => $photo_balance,
                'photo_message' => $photo_message]);
        }

        try {
            $filename = time() . '.' . $request->photo->getClientOriginalExtension();
            $filepath = public_path() . '/assets/img/competitors/' . $request->competitor . '/';
            $request->photo->move($filepath, $filename);
            $photo_path = '/assets/img/competitors/' . $request->competitor . '/' . $filename;
            $photo = new Photo;
            $photo->photo = $photo_path;
            $photo->competitor_id = $request->competitor;
            $photo->nomination_id = $request->nomination;
            $photo->save();

            /**
             * Проверка баланса фото после загрузки
             *
             */
            $photo_count = Nommi::photoCount($photo->nomination_id);
            $photo_correct_count = Nommi::photoCorrectCount($photo->competitor_id);
            $photo_balance = Nommi::photoBalance($photo_count, $photo_correct_count);

            $photo_message = 'Фото успешно загружено';

            return response()->json(['photo' => $photo_path, 'photo_count' => $photo_count, 'photo_balance' => $photo_balance,
                'photo_message' => $photo_message]);
            /**
             * Видимо, надо оставить выброс исключения, хотя класс валидации выбрасывает собственное.
             */
        } catch (Exception $e) {

            //return response()->json(['error' => $validator->errors()->all()]); //чтобы не парсить все ошибки
            /**
             * Вот такое сообщение об ошибках - от безвыходности, надо определять их где-то в классе или в словаре
             */
            return response()->json(['error' => 'Ошибка при загрузке фото']);
        }
    }



    /**
     * Загрузка дополнительного фото, возвращает ответ в ajax-запрос
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAddition(StorePhotoRequest $request)
    {

        /**
         * Баланс фото не проверяется, фото загружается сразу
         */


        try {
            $filename = time() . '.' . $request->photo->getClientOriginalExtension();
            $filepath = public_path() . '/assets/img/competitors/' . $request->competitor . '/';
            $request->photo->move($filepath, $filename);
            $photo_path = '/assets/img/competitors/' . $request->competitor . '/' . $filename;
            $photo = new Photo;
            $photo->photo = $photo_path;
            $photo->competitor_id = $request->competitor;
            $photo->nomination_id = $request->nomination;
            $photo->is_addition = 1;
            $photo->save();

            /**
             * Проверка баланса фото после загрузки
             * НАДО, ЧТОБЫ СЧИТАЛ С УЧЕТОМ is_addition
             */
            $photo_count = Nommi::photoCount($photo->nomination_id);
            $photo_correct_count = Nommi::photoCorrectCount($photo->competitor_id);
            $photo_balance = Nommi::photoBalance($photo_count, $photo_correct_count);

            $photo_message = 'Фото успешно загружено';

            return response()->json(['photo' => $photo_path, 'photo_count' => $photo_count, 'photo_balance' => $photo_balance,
                'photo_message' => $photo_message]);
            /**
             * Видимо, надо оставить выброс исключения, хотя класс валидации выбрасывает собственное.
             */
        } catch (Exception $e) {

            //return response()->json(['error' => $validator->errors()->all()]); //чтобы не парсить все ошибки
            /**
             * Вот такое сообщение об ошибках - от безвыходности, надо определять их где-то в классе или в словаре
             */
            return response()->json(['error' => 'Ошибка при загрузке фото']);
        }
    }

    /*
    * Подтверждение загрузки дополнительного фото по ссылке в письме
     */
    public function confirmAddition(Request $request)
    {
        // Проверка, есть ли такое дополнительное фото
        $addition_photo = Photo::where('competitor_id', '=', $request->competitor)
       ->where('id', '=', $request->photo)
       ->where('is_addition', '=', 1)
      ->first();
      
        if ($addition_photo) {
            // Заменить статус на "Одобрена"
            $addition_photo->photo_status_id = 2;
            $addition_photo->save();

            return redirect()->route('competitor.show', $request->competitor)->withSuccess('Дополнительное фото опубликовано');
        } else {
            return redirect()->route('competitor.show', $request->competitor)->withErrors('Невозможно опубликовать фото');
        }
    }



    /**
     * Показ отдельного фото участника
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $photo = Photo::with('comments.author')
                                ->with('getNomination', 'getCompetitor', 'likes', 'ratingLike')
                                ->find($id);
        $competitor_id = $photo->getCompetitor->id;
        //dd($competitor_id);
        $photos = Photo::where('competitor_id', '=', $competitor_id)
            ->where('photo_status_id', '=', 2)
            ->with('comments')
            ->with('likes')
            ->with('ratingLike')
            ->get();
        //dd($photos);

        //Сео
        $seo = Seo::where('key', '=', 'seo_one_photo')->first();
        $seo->title = strip_tags($seo->title .' '. $photo->getCompetitor->getUser->name);
        $seo->description = strip_tags($seo->description.' '.$photo->getCompetitor->getUser->name);

        //Контент
        $contents = Content::where('key_page', '=', 'photo')->get();

        return view('photo.show', ['photo' => $photo, 'photos' => $photos, 'seo' => $seo, 'contents' => $contents]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
