<?php

/**
 * @Author: sharpeenko
 * @Date:   2017-02-08 08:15:39
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-04-05 08:11:46
 */

namespace App\Http\Controllers;

use App\Competitor;
use App\Content;
use App\Events\AddCompetitorNomination;
use App\Http\Requests\UpdateCompetitorRequest;
use App\Nomination;
use App\RatingScore;
use App\Referee;
use App\Seo;
use Auth;
use Event;
use Exception;
use Illuminate\Http\Request;
use Input;
use Nomination as Nommi;
use Redirect;

class CompetitorController extends Controller
{
    protected $rating;
    protected $user;
    protected $user_competitors;

    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*
         * Проверка, что участником становится авторизованный пользователь
         */
        $this->middleware('accesscompetitor', ['only' => [
            'create',
        ],
        ]);

        /*
         * Редирект на страницу редактирования, если владелец открывает собственный профиль
         */
        $this->middleware('accesscompetitoredit', ['only' => [
            'show',
        ],
        ]);

        /*
         * Проверка пользователя при переходе напрямую на страницу редактирования
         */
        $this->middleware('accessroutecompetitoredit', ['only' => [
            'edit',
        ],
        ]);
    }

    /**
     * Отображает список участников
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        /**
         * Если пользователь авторизован, надо передать его в представление
         * @var [type]
         */
        if (Auth::check()) {
            $this->user = Auth::user();
            $this->user_competitors = $this->user->competitors;
        }

        /**
         * Если по get-запросу
         */
        if (!($request->has('nomination'))) {
            $competitors = Competitor::leftjoin('rating_scores as rs', 'rs.competitor_id', '=', 'competitors.id')
                ->orderBy('rs.position', 'asc')
                ->select('competitors.*')
                ->with('getUser')
                ->with('getNomination')
                ->with('photos')
                ->with('ratingScore')
                ->paginate(18);
            foreach ($competitors as $competitor) {
                array_add($competitor, 'name', $competitor->getUser->name);
            }

            $nominations = Nomination::where('published', '=', 1)->get();

            //Сео
            $seo = Seo::where('key', '=', 'seo_competitors')->first();

            //Контент
            $contents = Content::where('key_page', '=', 'all_competitors')->get();

            return view('competitor.all_competitors', ['competitors' => $competitors, 'nominations' => $nominations, 'seo' => $seo, 'contents' => $contents, 'user' => $this->user, 'user_competitors' => $this->user_competitors]);
        }

        /**
         * Если post-запрос из поиска по номинации
         *
         */
        $query = $request->input('nomination');
        $competitors = Competitor::leftjoin('rating_scores as rs', 'rs.competitor_id', '=', 'competitors.id')
            ->orderBy('rs.position', 'asc')
            ->select('competitors.*')
            ->with('getUser')
            ->with('getNomination')
            ->with('photos')
            ->with('ratingScore')
            ->paginate(18);
        foreach ($competitors as $competitor) {
            array_add($competitor, 'name', $competitor->getUser->name);
        }

        $nominations = Nomination::where('published', '=', 1)->get();

        $nomination_current = Nomination::find($query);

        //Сео
        $seo = Seo::where('key', '=', 'seo_competitors')->first();
        $seo->title = strip_tags($seo->title . ' ' . $competitor->getNomination->name);
        $seo->description = strip_tags($seo->description . ' ' . $competitor->getNomination->name);

        //Контент
        $contents = Content::where('key_page', '=', 'all_competitors')->get();

        return view('competitor.all_competitors', ['competitors' => $competitors, 'nomination_current' => $nomination_current, 'nominations' => $nominations, 'seo' => $seo, 'contents' => $contents, 'user' => $this->user, 'user_competitors' => $this->user_competitors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!Nommi::checkNominationStatus($request->nomination)) {
            return back()
                ->withErrors('Такой номинации нет, либо закрыта');
        }

        /*
         * Проверка наличия номинации
         * Проверка, что номинация открыта
         */

        $user_id = Auth::id();
        $nomination_id = $request->nomination;

        try {
            $competitor = new Competitor;
            $competitor->user_id = $user_id;
            $competitor->nomination_id = $request->nomination;
            $competitor->avatar = 'default'; //аватар по умолчанию
            $competitor->save();
            /* Передать-то надо пользователя, то бишь, объект User, да и номинацию, наверное, то же.
             * Сначала find из моделей, по созданной, а если нет, то исключение вывести.
             */
            //$competitor = Competitor::with('getUser')->find($competitor->id);
            //return view('competitor.edit', ['competitor' => $competitor]);
            //
            //

            Event::fire(new AddCompetitorNomination($competitor, 'AddCompetitorNomination'));

            return Redirect::back(301)
                ->with('success', 'Вы стали участником выбранной номинации');
        } catch (Exception $e) {
            return Redirect::back()
                ->with('error', $e->getMessage());
        }

        /*
        Ниже закомментированная часть, которая вызывала ошибку переадресации
        при замене аватара сразу после регистрации. Это происходило оттого, что метод обновления выполняется на create и редиректит на ту же страницу
        А надо уж тогда, чтобы редирект шел на edit. Пока проблема решена тем, что вновь созданный участник не переходит на страницу
        редактирования, ему просто выдается сообщение
         */

        /*$competitor = Competitor::with('getUser')->with('getNomination')->with(['getPhoto' => function ($query) {
    $query->whereIn('photo_status_id', [1, 2, 3]); //для владельца выбираются все фото, в том числе и отклоненные
    }])
    ->with('comments.author')
    ->find($competitor->id);
    $photo_count = Nommi::photoCount($competitor->getNomination->id);
    $photo_correct_count = Nommi::photoCorrectCount($competitor->id);
    $photo_balance = Nommi::photoBalance($photo_count, $photo_correct_count);
    array_add($competitor, 'photo_count', $photo_count);
    array_add($competitor, 'photo_balance', $photo_balance);

    // получаем список судей по данной номинации
    $referees = Referee::with('user')
    ->with(['review' => function ($query) use ($competitor) {
    $query->whereIn('competitor_id', [$competitor->id]);
    }])
    ->with(['score' => function ($query) use ($competitor) {
    $query->whereIn('competitor_id', [$competitor->id]);
    }])
    ->with(['scoreActual' => function ($query) use ($competitor) {
    $query->whereIn('competitor_id', [$competitor->id]);
    }])
    ->where('nomination_id', $competitor->nomination_id)->get();

    //Получаем среднюю судейскую оценку
    $avgScore = Nommi::avgScore($competitor->id);

    //Получаем крайнее одобренное фото (дату):
    $lastPhoto = Nommi::lastPhoto($competitor->id);

    //Сео
    $seo = Seo::where('key', '=', 'seo_one_competitor')->first();
    $seo->title = strip_tags($seo->title.' '.$competitor->getNomination->name.' '.$competitor->getUser->name);
    $seo->description = strip_tags($seo->description.' '.$competitor->getNomination->name.' '.$competitor->getUser->name);

    //Контент
    $contents = Content::where('key_page', '=', 'competitor_profile')->get();

    return view('competitor.edit', ['competitor' => $competitor, 'referees' => $referees, 'avgScore' => $avgScore, 'lastPhoto' => $lastPhoto, 'seo' => $seo, 'contents' => $contents]);
    } catch (\Illuminate\Database\QueryException $e) {
    return back()
    ->withErrors('Ошибка добавления участника');
    } */
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $competitor = Competitor::with('getUser')->with(['getPhoto' => function ($query) {
            $query->whereIn('photo_status_id', [2]);
        }])->with('comments.author')->find($id);
        // получаем список судей по данной номинации
        $referees = Referee::with('user')
            ->with(['review' => function ($query) use ($competitor) {
                $query->whereIn('competitor_id', [$competitor->id]);
            }])
            ->with(['score' => function ($query) use ($competitor) {
                $query->whereIn('competitor_id', [$competitor->id]);
            }])
            ->with(['scoreActual' => function ($query) use ($competitor) {
                $query->whereIn('competitor_id', [$competitor->id]);
            }])
            ->where('nomination_id', $competitor->nomination_id)->get();

        //Получаем среднюю судейскую оценку
        $avgScore = Nommi::avgScore($competitor->id);

        //Получаем крайнее одобренное фото (дату):
        $lastPhoto = Nommi::lastPhoto($competitor->id);

        //Получаем рейтинг
        if (!empty($competitor->ratingScore->position)) {
            $this->rating = $competitor->ratingScore->position;
        }

        //Сео
        $seo = Seo::where('key', '=', 'seo_one_competitor')->first();
        $seo->title = strip_tags($seo->title . ' ' . $competitor->getNomination->name . ' ' . $competitor->getUser->name);
        $seo->description = strip_tags($seo->description . ' ' . $competitor->getNomination->name . ' ' . $competitor->getUser->name);

        //Контент
        $contents = Content::where('key_page', '=', 'competitor_profile')->get();

        return view('competitor.show', ['competitor' => $competitor, 'referees' => $referees, 'avgScore' => $avgScore, 'lastPhoto' => $lastPhoto, 'rating' => $this->rating, 'contents' => $contents]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competitor = Competitor::with('getUser')->with('getNomination')->with(['getPhoto' => function ($query) {
            $query->whereIn('photo_status_id', [1, 2, 3, 4]); //для владельца выбираются все фото, в том числе и отклоненные
        }])
            ->with('comments.author')
            ->find($id);
        $photo_count = Nommi::photoCount($competitor->getNomination->id);
        $photo_correct_count = Nommi::photoCorrectCount($id);
        $photo_balance = Nommi::photoBalance($photo_count, $photo_correct_count);
        array_add($competitor, 'photo_count', $photo_count);
        array_add($competitor, 'photo_balance', $photo_balance);

        // получаем список судей по данной номинации
        $referees = Referee::with('user')
            ->with(['review' => function ($query) use ($competitor) {
                $query->whereIn('competitor_id', [$competitor->id]);
            }])
            ->with(['score' => function ($query) use ($competitor) {
                $query->whereIn('competitor_id', [$competitor->id]);
            }])
            ->with(['scoreActual' => function ($query) use ($competitor) {
                $query->whereIn('competitor_id', [$competitor->id]);
            }])
            ->where('nomination_id', $competitor->nomination_id)->get();

        //Получаем среднюю судейскую оценку
        $avgScore = Nommi::avgScore($competitor->id);

        //Получаем крайнее одобренное фото (дату):
        $lastPhoto = Nommi::lastPhoto($competitor->id);

        //Получаем рейтинг
        if (!empty($competitor->ratingScore->position)) {
            $this->rating = $competitor->ratingScore->position;
        }

        //Сео
        $seo = Seo::where('key', '=', 'seo_one_competitor')->first();
        $seo->title = strip_tags($seo->title . ' ' . $competitor->getNomination->name . ' ' . $competitor->getUser->name);
        $seo->description = strip_tags($seo->description . ' ' . $competitor->getNomination->name . ' ' . $competitor->getUser->name);

        //Контент
        $contents = Content::where('key_page', '=', 'competitor_edit')->get();

        return view('competitor.edit', ['competitor' => $competitor, 'referees' => $referees, 'avgScore' => $avgScore, 'lastPhoto' => $lastPhoto, 'rating' => $this->rating, 'seo' => $seo, 'contents' => $contents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompetitorRequest $request, $id)
    {
        try {
            $avatar_path = '';

            if (Input::hasFile('avatar')) {
                $file = Input::file('avatar');
                $extension = $file->getClientOriginalExtension();
                $filepath = public_path() . '/assets/img/competitors/' . $id . '/';
                $filename = 'ava' . $id . '.' . $extension;
                $avatar_path = asset('assets/img/competitors/' . $id . '/' . $filename);
                $file->move($filepath, $filename);
            }

            if ($avatar_path) {
                $competitor = Competitor::find($id);
                $competitor->avatar = $avatar_path;
                $competitor->about = $request->about;
                $competitor->save();
            } else {
                $competitor = Competitor::find($id);
                $competitor->about = $request->about;
                $competitor->save();
            }

            return Redirect::back(301)
                ->with('success', 'Данные успешно обновлены');
        } catch (Exception $e) {
            return Redirect::back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Страница отображает всех чемпионов во всех номинациях
     * @return [type] [description]
     */
    public function champions()
    {

      /**
       * Получаем завершенные номинации с первыми тремя наивысшими судейскими оценками, это и есть чемпионы
       */
        $nominations = Nomination::where('published', '=', 1)->where('status', '=', 3)
      ->with('competitors')
      ->with(['ratingScores' => function ($query) {
          $query->orderBy('position', 'asc')->limit(3);
      }])->get();


        //Сео
        $seo = Seo::where('key', '=', 'seo_champions')->first();
        return view('competitor.champions', ['nominations' => $nominations, 'seo' => $seo]);
    }

    /**
     * Поиск участников по критериям
     */
//    public function search(Request $request) {
    //
    //        if (!($request->has('nomination'))) {
    //            $competitors = Competitor::with('getUser')->paginate(3);
    //            foreach ($competitors as $competitor) {
    //                array_add($competitor, 'name', $competitor->getUser->name);
    //            }
    //
    //            $nominations = Nomination::where('published', '=', 1)->get();
    //
    //            return view('competitor.all_competitors', ['competitors' => $competitors, 'nominations' => $nominations]);
    //        }
    //
    //        $query = $request->input('nomination');
    //        $competitors = Competitor::with('getUser')
    //                ->where('nomination_id', '=', $query)
    //                ->paginate(3);
    //        foreach ($competitors as $competitor) {
    //            array_add($competitor, 'name', $competitor->getUser->name);
    //        }
    //
    //        $nominations = Nomination::where('published', '=', 1)->get();
    //
    //        $nomination_current = Nomination::find($query);
    //
    //        return view('competitor.all_competitors', ['competitors' => $competitors, 'nomination_current' => $nomination_current, 'nominations' => $nominations]);
    //    }
}
