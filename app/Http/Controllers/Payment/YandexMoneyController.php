<?php

namespace App\Http\Controllers\Payment;

use App\Payment\YandexMoney;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Nomination;
use App\Seo;
use App\Content;

class YandexMoneyController extends Controller
{

   /**
    * TODO Здесь надо свойство класса, в зависимости от того, что приходит из роута,  присваивать, либо номинацию, либо дополнительное фото, и т д..
    */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment\YandexMoney  $yandexMoney
     * @return \Illuminate\Http\Response
     */
    public function show(YandexMoney $yandexMoney)
    {
        return view('payment.yandexmoney.payment');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment\YandexMoney  $yandexMoney
     * @return \Illuminate\Http\Response
     */
    public function edit(YandexMoney $yandexMoney)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment\YandexMoney  $yandexMoney
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, YandexMoney $yandexMoney)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment\YandexMoney  $yandexMoney
     * @return \Illuminate\Http\Response
     */
    public function destroy(YandexMoney $yandexMoney)
    {
        //
    }


    /**
     * Открытие формы оплаты за участие
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function showFormNomination(Request $request)
    {
          $nomination = Nomination::find($request->nomination);

          //Сео
          $seo = Seo::where('key', '=', 'seo_nomination_payment')->first();
          $seo->title = strip_tags($seo->title .' '. $nomination->name);
          $seo->description = strip_tags($seo->description.' '.$nomination->introtext);


          //Контент
          $contents = Content::where('key_page', '=', 'nomination')->get();


          return view('payment.yandexmoney.payment_nomination', ['nomination' => $nomination, 'seo' => $seo, 'contents' => $contents]);
    }


    /**
     * Открытие формы оплаты за дополнительное фото
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function showFormAdditionalPhoto(Request $request)
    {
          $nomination = Nomination::find($request->nomination);

          //Сео
          $seo = Seo::where('key', '=', 'seo_nomination_payment')->first();
          $seo->title = strip_tags($seo->title .' '. $nomination->name);
          $seo->description = strip_tags($seo->description.' '.$nomination->introtext);


          //Контент
          $contents = Content::where('key_page', '=', 'nomination')->get();


          return view('payment.yandexmoney.payment_additional_photo', ['nomination' => $nomination, 'seo' => $seo, 'contents' => $contents]);
    }


    /**
     * Страница возврата после произведенной оплаты
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function recivePaymentNomination(Request $request)
    {
        return redirect()->route('competitor.create',['nomination' => $request->nomination]);

    }
}
