<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\ShowRefereeRequest;
use App\Http\Requests\StoreScoreRequest;
use App\Http\Requests\StoreCommentRequest;
use App\Referee;
use App\Competitor;
//use App\Nomination;
use App\Review;
use App\Score;
use Exception;
use Carbon\Carbon;
use Nomination as Nommi;
use App\User;
use Event;
use App\Events\AddScore;
use App\Seo;

class RefereeController extends Controller
{
    public function __construct()
    {
        $this->middleware('referee');
    }

    /**
     * Всегда редирект на судейский профайл
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('referee.show', Auth::user()->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Открытие судейского профайла, проверка в request-классе, доступно только владельцу
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRefereeRequest $request)
    {
        // Судья
        $referee = Referee::with('user')->where('user_id', $request->referee)->first();
        // Участники номинации
        $nomination_id = $referee->nomination_id;
        $competitors = Competitor::where('nomination_id', $nomination_id)
        ->with(['review' => function ($query) use ($referee) {
            $query->whereIn('referee_id', [$referee->id]);
        }])
        ->with(['score' => function ($query) use ($referee) {
            $query->whereIn('referee_id', [$referee->id])->whereIn('is_actual', [1]);
        }])
        ->paginate(20);
        //Добавляем к участникам даты крайних одобренных фото
        foreach ($competitors as $competitor) {
            array_add($competitor, 'last_photo', Nommi::lastPhoto($competitor->id));
        }
        //dd($competitors);

        //Сео
        $seo = Seo::where('key', '=', 'seo_referee')->first();
        $seo->title = strip_tags($seo->title .' '. $referee->user->name.' '.$referee->nomination->name);
        $seo->description = strip_tags($seo->description.' '. $referee->user->name.' '.$referee->nomination->name);

        // При желании можно еще присоединить данные по номинации, и загружать те же правила, например, в модальное окно
        return view('referee.show', ['referee'=>$referee, 'competitors'=>$competitors, 'seo' => $seo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Загрузка данных участника в модальное окно выставления оценки
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function score($id)
    {
        /**
      * По-идее, сюда функцию типа setView() для фиксации просмотра. Но она должна получать id referee.
      * @var [type]
      */
        //Получаем судью по пользователю, чтобы избежать "подставы"
        $referee = Referee::where('user_id', Auth::user()->id)->first();



        $competitor = Competitor::with(['getPhoto' => function ($query) {
            $query->whereIn('photo_status_id', [2]);
        }])
        ->with(['score' => function ($query) use ($referee) {
            $query->whereIn('referee_id', [$referee->id]);
        }])
        ->find($id);
        // добавляем просмотр
        $this->setReview($referee, $competitor);
        return view('referee.score', ['competitor'=>$competitor]);
    }

    /**
     * Добавление просмотра участника судьей
     * @param Referee    $referee    [description]
     * @param Competitor $competitor [description]
     */
    protected function setReview(Referee $referee, Competitor $competitor)
    {
        // Получаем номинацию по участнику
        $nomination = $competitor->getNomination->first();
        // добавляем просмотр
        try {
            $review = new Review;
            $review->competitor_id = $competitor->id;
            $review->referee_id = $referee->id;
            $review->nomination_id = $nomination->id;
            $review->save();
        } catch (\Illuminate\Database\QueryException $e) {
            return back()
             ->withErrors('Ошибка добавления просмотра');
        }
    }

    /**
     * Установка оценки судьей участнику
     * @param  StoreScoreRequest $request [description]
     * @return [type]                     [description]
     */
    public function storeScore(StoreScoreRequest $request)
    {

     //Получаем судью
        $referee = Referee::where('user_id', Auth::user()->id)->first();
        //Получаем участника
        $competitor = Competitor::find($request->competitor);
        //Получаем номинацию
        $nomination = $competitor->getNomination;
        //Генерируем оценку
        $score_join = (real)($request->beforepoint.'.'.$request->afterpoint);


        try {

         // Найти оценки этого судьи по данному участнику, и сбросить актуальность


            $score_current = Score::where('competitor_id', $competitor->id)
         ->where('referee_id', $referee->id)
         ->get();
            foreach ($score_current as $sc) {
                $sc->is_actual = 0;
                $sc->save();
            }





            // Установить новую актуальную оценку
            $score = new Score;
            $score->referee_id = $referee->id;
            $score->competitor_id = $competitor->id;
            $score->nomination_id = $nomination->id;
            $score->score = $score_join;
            $score->is_actual = 1;
            $score->save();


            /**
             * Генерация события оценка поставлена для observer
             */
            $user = User::find($referee->user_id);
            Event::fire(new AddScore($score, $user, 'AddScore'));



            $success_message = 'Оценка поставлена';
            $comment_message = '';

            // Если устанофлен флажок добавления комментария
            if ($request->addcomment == 'on') {
                if ($this->refereeComment($request->comment, $competitor->id, $referee->user_id)) {
                    $comment_message = ', а комментарий добавлен';
                } else {
                    $comment_message = ', но комментарий не добавлен';
                }
            }

            return response()->json([
             'success_message' => $success_message.$comment_message, 'score_value'=> $score->score, 'score_date'=>date('d.m.Y', strtotime(Carbon::now()))]);
        } catch (Exception $e) {

         //return response()->json(['error' => $validator->errors()->all()]); //чтобы не парсить все ошибки
            /**
             * Вот такое сообщение об ошибках - от безвыходности, надо определять их где-то в классе или в словаре
             */

            $error_message = 'Ошибка при выставлении оценки';
            return response()->json(['error_message' => $error_message]);
        }
    }


    /**
     * Автоматическое добавление комментария участнику при установке оценки, если установлен соответствующий флажок
     * @param  [type] $comment_content [description]
     * @param  [type] $competitor_id   [description]
     * @param  [type] $user_id         [description]
     * @return [type]                  [description]
     */
    protected function refereeComment($comment_content, $competitor_id, $user_id)
    {
        try {
            $comment = new \App\Comment;
            $comment->commentable_id = $competitor_id;
            $comment->commentable_type = 'App\Competitor';
            $protect_content = addslashes($comment_content);
            $protect_content = htmlspecialchars($protect_content);
            //$protect_content = preg_replace("/[^a-z0-9]/i", "", $protect_content);
            $comment->content = $protect_content;
            $comment->publish_at = date("Y-m-d H:i:s");
            $comment->user_id = $user_id;
            $comment->deleted_at = null;
            $comment->save();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
