<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AccessSetLikes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
     /*
      * Если гость, нет доступа
      */
     if (!Auth::check()) {
         return redirect('noaccess');
     }



     /*
      * Если авторизованный, то начинает работать установка лайка
      */
     return $next($request);
    }
}
