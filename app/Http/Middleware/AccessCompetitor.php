<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AccessCompetitor {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        /*
         * Если гость, нет доступа
         */
        if (!Auth::check()) {
            return redirect('login')->withErrors('Войдите на сайт');
        }

        /*
         * Если авторизованный
         */
        return $next($request);
    }

}
