<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $auth = Auth::guard($guard);
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return redirect()->route('noaccess');
            } else {
                return redirect()->guest('login');
            }
        }

        if ($auth->user()->can('adminpanel_access')) {
            return $next($request);
        }

        /*$admin_roles = ['testadmin', 'testmanager', 'testpanel', 'testmoderator'];

        foreach ($admin_roles as $role) {
            if ($auth->user()->hasRole($role)) {
                return $next($request);
            }
        }*/

        return redirect()->route('noaccess');
    }
}
