<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Competitor;

class AccessRouteCompetitorEdit {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        /*
         * Если гость, нет доступа
         */
        if (!Auth::check()) {
            return redirect('noaccess');
        }

        $competitor = Competitor::find($request->route('competitor'));
        $nomination_status = $competitor ->getNomination->status;

        if($nomination_status ==3) {
          return redirect('noaccess');
        }

        /**
         * Если номинация продлена, но в дополнительный тур не допущен
         */

        if($nomination_status ==2 && $competitor->is_additional_round == 0) {
          return redirect('noaccess');
        }

        if (!(Auth::user()->id == $competitor->getUser->id)) {
            return redirect('noaccess');
        }

        return $next($request);
    }

}
