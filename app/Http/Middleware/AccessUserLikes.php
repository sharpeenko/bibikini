<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Photo;


class AccessUserLikes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
     /*
      * Если для "своего" пользователя, открытие страницы "кто лайкнул"
      */
     $user_id = Photo::find($request->route('photo'))->getCompetitor->getUser->id;
     if (Auth::user()->id == $user_id) {
         return $next($request);
     }

     /*
      * Если авторизованный, то открывается страница фото
      */
      return back()
          ->withErrors('Смотреть, кто лайкнул может только владелец');

    }
}
