<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Competitor;

class CompetitorEditAccess {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        /*
         * Если залогинен, проверка
         */
        if (Auth::check()) {
            $competitor = Competitor::find($request->route('competitor'));
            $nomination_status = $competitor ->getNomination->status;



            /*
             * Если владелец, открытие страницы редактирования
             */
            if ($nomination_status ==1 && Auth::user()->id == $competitor->getUser->id) {
                return redirect()->route('competitor.edit', [$competitor->id]);
            }

            /**
             * Если номинация продлена, но в дополнительном туре
             */

             if ($nomination_status ==2 && Auth::user()->id == $competitor->getUser->id) {
                if($competitor->is_additional_round == 1) {
                  return redirect()->route('competitor.edit', [$competitor->id]);
                }

             }
        }


        /*
         * Если открывает чужой профиль, то просмотр
         */
        return $next($request);
    }

}
