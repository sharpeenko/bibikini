<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AccessBuyLikes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
     /*
      * Если гость, нет доступа
      */
     if (!Auth::check()) {
         return redirect('noaccess');
     }

     /*
      * Если для "своего" пользователя, открытие страницы покупки лайков
      */
     if (Auth::user()->id == $request->route('user')) {
         return $next($request);
     }

     /*
      * Если авторизованный, то открывается страница покупки лайков
      */
     return redirect('noaccess');

    }
}
