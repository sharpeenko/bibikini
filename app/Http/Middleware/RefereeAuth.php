<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class RefereeAuth
{
    /**
     * [handle description]
     * @param  [type]  $request [description]
     * @param  Closure $next    [description]
     * @param  [type]  $guard   [description]
     * @return [type]           [description]
     */
    public function handle($request, Closure $next, $guard = null)
    {
     $auth = Auth::guard($guard);
     if (Auth::guard($guard)->guest()) {
         if ($request->ajax() || $request->wantsJson()) {
             return redirect()->route('noaccess');
         } else {
             return redirect()->guest('login');
         }
     }

     /**
      * Если "судейская" роль
      * @var array
      */
     $admin_roles = ['testreferee'];

     foreach ($admin_roles as $role) {
         if ($auth->user()->hasRole($role)) {
             return $next($request);

         }
     }

     return redirect()->route('noaccess');
    }
}
