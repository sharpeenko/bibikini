<?php

namespace App\Http\Middleware;

use Closure;
use App\Nomination;

class NominationAccess {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        $nomination = Nomination::find($request->route('nomination'));
        /*
         * Если номинация не опубликована
         */

        if ($nomination->published != 1) {
            abort(404);
        }

        return $next($request);
    }

}
