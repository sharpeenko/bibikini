<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreScoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         'beforepoint' => 'required|between:0,9',
         'afterpoint' => 'required|between:0,9',
         'comment' => 'required_with:addcomment|max:1000',

        ];
    }
}
