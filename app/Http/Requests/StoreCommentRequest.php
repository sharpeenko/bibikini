<?php
/**
 * @Author: sharpeenko
 * @Date:   2017-03-30 07:43:25
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-04-04 09:46:38
 */

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user_id = $this->route('user');
       return (Auth::user()->id == $user_id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'required|max:1000',
        ];
    }
}
