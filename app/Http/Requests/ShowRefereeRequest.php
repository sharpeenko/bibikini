<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class ShowRefereeRequest extends FormRequest
{
    /**
     * Доступно будет только для владельца
     *
     * @return bool
     */
    public function authorize()
    {
     $user_id = $this->route('referee');
     return (Auth::user()->id == $user_id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
