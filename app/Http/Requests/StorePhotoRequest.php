<?php

namespace App\Http\Requests;

use App\Competitor;
use Auth;
use Illuminate\Foundation\Http\FormRequest;

class StorePhotoRequest extends FormRequest
{
    /**
     * Проверка соответствия "участник - зарегистрированный пользователь"
     *
     * @return bool
     */
    public function authorize()
    {

        $competitor = Competitor::find($this->route('competitor'));
        return (Auth::user()->id == $competitor->getUser->id);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ];
    }

}
