<?php

/**
 * @Author: sharpeenko
 * @Date:   2017-02-04 10:41:34
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-03-23 17:54:05
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Nominations;

use App;
use App\Competitor as Competitor;
use App\Nomination as Nommi;
use App\Fund;
use App\Photo as Photo;
use App\Like as Like;
use Auth;
use DateTime;
use DB;
use App\User as User;

class Nomination
{
    public function nominationTest()
    {
        return 'foo';
    }

    /*
     * Проверяет, существует номинация, и открыта ли?
     *
     * @param int $id
     *
     * @return bool
     */

    public function checkNominationStatus($id)
    {
        $nomination = Nommi::where('id', '=', $id)
            ->where('is_actual', '=', 1)
            ->first();
        if ($nomination === null) {
            return false;
        }

        return true;
    }

    /*
     * Проверяет, является ли пользователь участником номинации?

     *
     * @param int $nomination_id
     *
     * @return bool
     */

    public function checkNominationCompetitor($nomination_id)
    {
        if (!Auth::check()) {
            return false;
        }

        $is_competitor = Competitor::where('user_id', '=', Auth::id())
            ->where('nomination_id', '=', $nomination_id)
            ->first();
        if ($is_competitor === null) {
            return false;
        }

        return $is_competitor;
    }

    //Получение сведений об участнике номинации

    public function getCompetitorInfo($nomination_id)
    {
        $competitor = Competitor::where('user_id', '=', Auth::id())
            ->where('nomination_id', '=', $nomination_id)
            ->with('photos')
            ->first();
        //dd($competitor);
        return $competitor;
    }


    //Получение участника номинации по пользователю и номинации, для профиля пользователя, в список номинаций

    public function competitorForUserProfile($nomination_id, $user_id)
    {
        $competitor = Competitor::where('user_id', '=', $user_id)
            ->where('nomination_id', '=', $nomination_id)
            ->with('photos')
            ->first();
        //dd($competitor);
        return $competitor;
    }


    // Сколько дней до финиша

    public function beforeFinish($finish)
    {
        $today = new DateTime(date('Y-m-d'));
        $finish_date = new DateTime($finish);
        $before_finish = $today->diff($finish_date);
        return $before_finish->format('%a');
    }

    // Количество участников

    public function countCompetitors($id)
    {
        return Competitor::where('nomination_id', $id)->count();
    }

    /**
     * Получение суммы призового фонда по номинации
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function fundSumScores($id)
    {
        return Fund::where('nomination_id', $id)->where('prizeable', 'score')->first()->sum;
    }

    /**
     * Получение суммы призового фонда по лайкам
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function fundSumLikes($id)
    {
        return Fund::where('nomination_id', $id)->where('prizeable', 'like')->first()->sum;
    }

    // Количество фото (только одобренных)

    public function countPhotos($id)
    {
        return Photo::where('nomination_id', $id)->where('photo_status_id', '=', 2)->count();
    }

    // Количество лайков фото по номинации

    public function countLikesNomination($id)
    {
        return DB::table('likes')
         ->join('photos', 'likes.likeable_id', '=', 'photos.id')
        ->where('likes.likeable_type', 'like', '%Photo%')
         ->where('photos.nomination_id', '=', $id)
         ->count();
    }

    // Количество лайков фото по участнику, пока только фото

    public function countLikesCompetitor($id)
    {
        return DB::table('likes')
         ->join('photos', 'likes.likeable_id', '=', 'photos.id')
        ->where('likes.likeable_type', 'like', '%Photo%')
         ->where('photos.competitor_id', '=', $id)
         ->count();
    }


    // Количество  фото пользователя

    public function countPhotoUser($id)
    {
        return DB::table('photos')
         ->join('competitors', 'photos.competitor_id', '=', 'competitors.id')
         ->join('users', 'competitors.user_id', '=', 'users.id')
        ->where('photos.photo_status_id', '=', 2)
         ->where('users.id', '=', $id)
         ->count();
    }

    /*
     * Получает все номинации пользователя
     */

    public function getNominations($id)
    {

//        $nominations = DB::table('users')
        //                ->join('competitors', 'users.id', '=', 'competitors.user_id')
        //                ->join('nominations', 'competitors.nomination_id', '=', 'nomination.id')
        //                ->select('nominations.*')
        //                ->where('users.id', '=', $id);

        $nominations = DB::table('users')
            ->join('competitors', 'users.id', '=', 'competitors.user_id')
            ->join('nominations', 'competitors.nomination_id', '=', 'nominations.id')
            ->select('nominations.*', 'competitors.avatar', 'competitors.id as competitor_id', 'competitors.is_champion', 'competitors.prize_place' )
            ->where('users.id', '=', $id)
            ->paginate(20);

        foreach ($nominations as $nomination) {
            if (!empty($nomination->styles)) {
                foreach (json_decode($nomination->styles, true) as $key => $value) {
                    $nomination->$key = $value;
                }
            }
            $before_finish = self::beforeFinish($nomination->finish);
            $nomination->before_finish = $before_finish;
            $nomination->count_competitors = self::countCompetitors($nomination->id);
            $nomination->count_likes = self::countLikesNomination($nomination->id);
            $nomination->fund_scores = self::fundSumScores($nomination->id);
            $nomination->fund_likes = self::fundSumLikes($nomination->id);
        }
        return $nominations;
    }

    /**
     * Получение количества фото, определенное для номинации
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function photoCount($id)
    {
        $nomination = Nommi::find($id);
        return $nomination->quant_photo;
    }

    /**
     * Получение фото, имеющих статус "Загружена" и "Одобрена"
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function photoCorrectCount($id)
    {
        $photo_correct_count = Photo::whereIn('photo_status_id', [1, 2])->where('competitor_id', '=', $id)->count();
        return $photo_correct_count;
    }

    /**
     * Получение количества фото для загрузки, либо 0
     * @param  [type] $photo_quant   [description]
     * @param  [type] $photo_correct [description]
     * @return [type]                [description]
     * Меньше 0 он в том случае, если загружены дополнительные фото, логической ошибки нет.
     */
    public function photoBalance($photo_quant, $photo_correct)
    {
        $photo_balance = $photo_quant - $photo_correct;
        if ($photo_balance > 0) {
            return $photo_quant - $photo_correct;
        } else {
            return 0;
        }
    }



    /**
     * Получение крайнего одобренного фото (дату)
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
   public function lastPhoto($id)
   {
       $lastPhoto = DB::table('photos')
     ->where('competitor_id', $id)
     //->whereIn('photo_status_id', ['2', '4'])
     ->where('photo_status_id', '=', 2)
     ->max('updated_at');
     //dd($lastPhoto);
     return  $lastPhoto;
   }

    /**
     * Является ли роль этого пользователя "судейской"? Для применения стилей комментария в представлении.
     * @param  [type]  $id [description]
     * @return boolean     [description]
     */
    public function isReferee($id)
    {
        $user = User::find($id);
        $referee_roles = ['testreferee'];

        foreach ($referee_roles as $role) {
            if ($user->hasRole($role)) {
                return true;
            }
        }
    }


/**
 *   Получение средней судейской оценки
  * Передается идентификатор участника
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
public function avgScore($id)
{
    $avgScore = DB::table('scores')
 ->select(DB::raw('round(AVG(score), 2) as avgscore'))
 ->where('competitor_id', $id)
 ->where('is_actual', 1)
 ->get();
    return  $avgScore;
}

    /**
     * Вычисляет высоту ленты для виджета рейтинга в номинации
     * @param  [type] $score   оценка
     * @param  [type] $coeff  коэффициент, зависит от верстки, сколько пикселов на часть оценки
     * @param  [type] $measure Единица измерения высоты
     * @return [type]          [description]
     */
    public function ratingLentaHeight($score, $coeff, $measure)
    {
        return $score*10*$coeff."$measure";
    }
}
