<?php

namespace App\Events;

use App\Referee;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddRefereeNomination
{
    use InteractsWithSockets, SerializesModels;

    public $referee; // добавление пользователь/номинация в админке
    public $status; // статус, указание, что это именно добавление пользователя в номинацию (судья), для использования в шаблоне

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Referee $referee, $status)
    {
        $this->referee = $referee;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
