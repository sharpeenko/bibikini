<?php

namespace App\Events;

use App\Like;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddLikePhoto
{
    use InteractsWithSockets, SerializesModels;

    public $like; // добавленный лайк фото
    public $user; // пользователь, который добавил лайк
    public $status; // статус, указание, что это именно добавление лайка к фото, для использования в шаблоне

    /**
     * Create a new event instance. Передается экземпляр модели лайка из контроллера
     *
     * @return void
     */
    public function __construct(Like $like, User $user, $status)
    {
        $this->like = $like;
        $this->user = $user;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
