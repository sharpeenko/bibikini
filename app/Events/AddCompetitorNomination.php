<?php

namespace App\Events;

use App\Competitor;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddCompetitorNomination
{
    use InteractsWithSockets, SerializesModels;

    public $competitor;
    public $status; // статус, указание, что это именно добавление участника в номинацию , для использования в шаблоне

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Competitor $competitor, $status)
    {
      $this->competitor = $competitor;
      $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
