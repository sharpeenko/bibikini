<?php

namespace App\Events;

use App\Photo;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class ChangePhotoStatus
{
    use InteractsWithSockets, SerializesModels;

    public $photo; // фото, статус которого изменен в админке
    public $status; // статус, указание, что это именно изменение статуса фото, для использования в шаблоне

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Photo $photo, $status)
    {
        $this->photo = $photo;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
