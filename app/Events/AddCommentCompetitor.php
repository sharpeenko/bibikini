<?php

namespace App\Events;

use App\Comment;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class AddCommentCompetitor
{
    use InteractsWithSockets, SerializesModels;

    public $comment; // добавленный комментарий участнику
    public $user; // пользователь, который добавил комментарий
    public $status; // статус, указание, что это именно добавление комментария участнику, для использования в шаблоне

    /**
     * Передается экземпляр модели комментария из контроллера
     *
     * @return void
     */
    public function __construct(Comment $comment, User $user, $status)
    {
        $this->comment = $comment;
        $this->user = $user;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
