<?php

namespace App\Events;

use App\Comment;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class InfoModerator
{
    use InteractsWithSockets, SerializesModels;

    public $comment; // комментарий, на который жалоба
    public $user; // инициатор сообщения админу
    public $status; // статус, указание, что это именно жалоба модератору, для использования в шаблоне

    /**
     * Передается экземпляр модели комментария из контроллера
     *
     * @return void
     */
    public function __construct(Comment $comment, User $user, $status)
    {
        $this->comment = $comment;
        $this->user = $user;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
