<?php

namespace App\Events;

use App\Confirm;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class UserRegistered
{
    use InteractsWithSockets, SerializesModels;

    public $confirm; // данные, необходимые для генерации письма с подтверждением
    public $status; // статус, указание, что это именно подтверждение регистрации

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Confirm $confirm, $status)
    {

        $this->confirm = $confirm;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
