<?php

namespace App\Events;

use App\Score;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;


class AddScore
{
    use InteractsWithSockets, SerializesModels;

    public $score; // оценка
    public $user; // пользователь, который поставил оценку
    public $status; // статус, указание, что это именно поставлена оценка, для использования в шаблоне

    /**
     * Передается экземпляр модели оценки из контроллера(?)
     *
     * @return void
     */
    public function __construct(Score $score, User $user, $status)
    {
        $this->score = $score;
        $this->user = $user;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
