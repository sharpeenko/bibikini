/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {

//Для ajax-запросов

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

 // Лайтбокс
 $(document).on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
 });
 //Поля загрузки файлов
 $('input[type=file]').bootstrapFileInput();
 // Кнопка "Наверх"
 // Only enable if the document has a long scroll bar
 // Note the window height + offset
 if (($(window).height() + 100) < $(document).height()) {
  $('#top-link-block').removeClass('hidden').affix({
   // how far to scroll down before link "slides" into view
   offset: {
    top: 100
   }
  });
 }
 // Вход на сайт с использованием ajax
 var loginForm = $("#frm-login");
 loginForm.submit(function(e) {
  e.preventDefault();

  var formData = loginForm.serialize();
  $('#alert-block').html("");
  $.ajax({
   url: '/login',
   type: 'POST',
   data: formData,
   success: function(data) {
    $('#modalLogin').modal('hide');
    location.reload(true);
   },
   error: function(data) {
    console.log(data.responseText);
    var obj = jQuery.parseJSON(data.responseText);
    if (obj.error) {
     $('#alert-block').append('<div class="alert alert-danger alert-block">' + '<button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>' + obj.error + '</div>');
    }
   }
  });
 });
 //Загрузка содержимого ajax в popover
 //Комментируется, в связи с тем, что popover пока решено не применять
 /*$('[data-toggle="popover"]').click(function(event) {
     event.preventDefault();
     var e = $(this);
     e.off('hover');
     $.get(e.data('poload'), function(d) {
         e.popover({
             html: 'true',
             content: d,
             placement: 'top',
             trigger: 'focus'
         }).popover('show');
     });
 });*/


 //    $('[data-toggle="popover"]').hover(function () {
 //        var e = $(this);
 //        e.off('hover');
 //        $.get(e.data('poload'), function (d) {
 //            e.popover({html: 'true', content: d, placement: 'top', trigger: 'hover focus'}).popover('show');
 //        });
 //    });
 //Загрузка содержимого ajax в модальное окно
 //Комментируется, в связи с тем, что modalInformer пока решено не применять
 /*$('#modalInformer').on('show.bs.modal', function() {
     $.ajax({
         url: '/makeup/testajax',
         type: 'GET',
         success: function(data) {
             $('#tA').html("");
             $('#tA').append(data);
         },
         error: function(data) {
             console.log(data.responseText);
         }
     });
 }); */

 //Загрузка фото через ajax
 var uploadPhotoForm = $("#frm-upload-photo");
 uploadPhotoForm.submit(function(e) {
  e.preventDefault();
  var $that = $(this);
  formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
  $('#modalUploadPhoto').find('#alert-block').html("");
  $('#modalUploadPhoto').find('#photo-count').html("");
  $.ajax({
   url: $that.attr('action'),
   type: $that.attr('method'),
   contentType: false, // важно - убираем форматирование данных по умолчанию
   processData: false, // важно - убираем преобразование строк по умолчанию
   data: formData,
   dataType: 'json',
   success: function(data) {
    if (data.photo) {
     $("#last-photo").before('<div class=\"col-md-3 col-sm-3 col-xs-3\">' +
      '<div class=\"thumbnail photo\">' +
      '<div class=\"pic mini download\">' +
      '<img class=\"img-responsive img-rounded\" src=\"' + data.photo + '\"' + ' alt=\"...\">' +
      '<div class=\"pic-bar\">' +
      '<p class=\"text-center\">' +
      'Проверка' +
      '</p>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>');
     $("#photo").val("");
     $('.file-input-name').html('');
     $('#modalUploadPhoto').find('#alert-block').append('<div class=\"alert alert-success alert-block\">' +
      '<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"fa fa-minus-square\"></i></button>' +
      data.photo_message + '</div>');
     $('#modalUploadPhoto').find('#photo-count').append('<button type=\"button\" class=\"close\" data-dismiss=\"alert\">' +
      '<i class=\"fa fa-minus-square\"></i></button>' +
      'По правилам номинации можно загрузить не более ' + data.photo_count + ' фото. ' +
      'Осталось ' + data.photo_balance + ' фото.');

    } else {
     $("#photo").val("");
     $('.file-input-name').html('');
     $('#modalUploadPhoto').find('#alert-block').append('<div class=\"alert alert-success alert-block\">' +
      '<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"fa fa-minus-square\"></i></button>' +
      data.photo_message + '</div>');
     $('#modalUploadPhoto').find('#photo-count').append('<button type=\"button\" class=\"close\" data-dismiss=\"alert\">' +
      '<i class=\"fa fa-minus-square\"></i></button>' +
      'По правилам номинации можно загрузить не более ' + data.photo_count + ' фото. ' +
      'Осталось ' + data.photo_balance + ' фото.');
    }
   },
   error: function(response) {
    $("#photo").val("");
    $('.file-input-name').html('');
    /**
     * Так как текст возврата не удается установить на сервере, определяем в js, в зависимости от
     * ошибки из json-строки
     */
    var jsonErrors = JSON.parse(response.responseText);
    if (jsonErrors.photo == 'validation.uploaded') {
     $('#modalUploadPhoto').find('#alert-block').append('<div class=\"alert alert-danger alert-block\">' +
      '<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"fa fa-minus-square\"></i></button>' +
      'Не соответствует формату, либо слишком большого размера' + '</div>');
    }

   }
  });
 });

 //Закрытие формы загрузки фото
 $('#frm-close').click(function() {
  $('#modalUploadPhoto').modal('hide');
  location.reload(true);
 });

 //Закрытие формы загрузки фото "крестик"
 $('#modalUploadPhoto').find('button.close').click(function() {
  $('#modalUploadPhoto').modal('hide');
  location.reload(true);
 });




 // Переход к добавлению комментария при нажатии на кнопку "Ответить"
 if ($('.link-addcomment').length > 0) {
  $('.link-addcomment').click(function() {
   var questionName = $(this).data('user');
   $('#text-addcomment').val(questionName + ',');
  });
 }




 // Загрузка данных в подальное окно просмотра-оценки для судей
 $('#modalScore').on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget);
  var competitor = button.data('competitor');
  $.ajax({
   url: '/referee/score/' + competitor,
   type: 'GET',
   success: function(data) {
    $('#score').html("");
    $('#score').append(data);
   },
   error: function(data) {
    console.log(data.responseText);
   }
  });
 });


 //Закрытие модального окна просмотра/оценок
 $('#modalScore').on('hide.bs.modal', function() {
  location.reload(true);
 });

 // Добавление агрегирующего столбца в таблицу судейских оценок/просмотров на страницах участников
 if ($('#table-referees').length > 0) {
  var rowCount = $('#table-referees tr').length;
  var avgScore = $('#table-referees').data('avg');
  var rating = $('#table-referees').data('rating');
  $('#table-referees  tr:first').append('<td class=\"score middle\" rowspan=\"'+rowCount+'\">' +
   '<span class=\"badge badge-info\"><i class=\"fa fa-graduation-cap\" aria-hidden=\"true\"></i>&nbsp;'+avgScore+'</span>' +
   '<br>' +
   '<span class=\"badge badge-warning\"><i class=\"fa fa-bar-chart\" aria-hidden=\"true\"></i>&nbsp;'+rating+'</span>' +
  '</td>' );

 }


// Развернуть/свернуть "Быстрый старт"

 if ($('button.quick-start').length > 0) {

   var myFlag = true;

   $('button.quick-start').click(function () {

            var caption = $(this).data('caption');

            if (myFlag) {
                $(this).html(caption +  ' <i class="fa fa-chevron-up" aria-hidden="true"></i>');
                myFlag = false;

            }
            else {
                $(this).html(caption + ' <i class="fa fa-chevron-down" aria-hidden="true"></i>');
                myFlag = true;
            }
        });

}

// Переход к якорям

$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      headerHeight = $('.header').height() + 110;
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - headerHeight
        }, 500, function() {
          target.focus();
        });
        return false;
      }
     }
   });


 // Для перехода к якорю на другой странице

   if(window.location.hash) {
     var target = $(window.location.hash);
     target = target.length ? target : $('[name=' + window.location.hash.slice(1) +']');
     headerHeight = $('.header').height() + 110;
     if (target.length) {
       $('html,body').animate({
         scrollTop: target.offset().top - headerHeight
       }, 500, function() {
         target.focus();
       });
    }

  }

  // Закрытие информера "по крестику"

 if ($('.informer').length > 0) {
   $('.informer button').click(function () {
     if($("#notshow").prop("checked")) { // с созданием cookie
       var url = $("#notshow").data('url');
       $.ajax({
         url: url,
         type: 'POST',
         success: function(data) {
           console.log(data);

         },
         error: function(data) {
            console.log(data);
          }
        });
      }
      $(this).parents('.informer').addClass('closed');
     });
}

//Открыть модальное окно согласия на участие

if (getUrlVars()["agree"]) {
  if ($('#modalAgreement').length > 0) {
      $('#modalAgreement').modal('show');
  }

  }



});


function ChangeUploadPhotoAddition() {
  var uploadPhotoFormAction = $("#frm-upload-photo").attr("action");
 uploadPhotoFormAction = uploadPhotoFormAction+'/addition';
 $("#frm-upload-photo").attr("action",  uploadPhotoFormAction);
 $("a.file-input-wrapper span").html("Загрузить дополнительно");
 $("input#photo").attr("title",  "Загрузить дополнительно");
 $('.alert').alert('close');
}


function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}
