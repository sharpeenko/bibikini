/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function ( ) {

    if ($('.autohight').length > 0) {
        var PageHeight = $(window).height();
        var HeaderHeight = $('#header').height();
        var FooterHeight = $('#footer').height() - $('.footer-area').height();
        var SiteHeight = PageHeight - HeaderHeight - FooterHeight;
        $('.autohight').css('min-height', SiteHeight);
    }

    $('input[type=file]').bootstrapFileInput();

    if ($('.link-addcomment').length > 0) {
        $('.link-addcomment').click(function () {
            var questionName = $(this).data('user');
            $('#text-addcomment').val(questionName + ',');
        });
    }

});