@extends('layouts.bibikini')
@section('breadcrumbs')
@yield('breadcrumbs')
@endsection
@section('header')
        <h1>{!! $content->header !!}</h1>
@endsection
@section('content')



    <div class="row">
        <div class="col-lg-12">


                    {!! $content->content !!}



        </div>
    </div>
</section>
@endsection
