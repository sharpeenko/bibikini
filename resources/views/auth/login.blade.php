@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('login') !!}
@endsection
@section('header')
      <h1>Вход на сайт <small></small></h1>
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-8">

            <form class="form-horizontal" role="form" method="POST" id="frm-login">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Эл. почта <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Введите email" value="{{old('email')}}" required autofocus>
                        <p class="help-block text-danger">{!! $errors->first('email') !!}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Пароль <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" placeholder="Введите пароль">
                        <p class="help-block text-danger">{!! $errors->first('password') !!}</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Запомнить меня
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Войти</button>
                        &nbsp;&nbsp;&nbsp;<a href="{{url('/password/reset')}}">Забыли пароль?</a>&nbsp;&nbsp;&nbsp;<a href="{{url('/register')}}">Регистрация</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
