@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('noaccess') !!}
@endsection
@section('header')
  <h1>Извините, Вы не можете просматривать этот раздел</h1>
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-12">

            <p>
                <a href="{{ url('login') }}">
                    Вход
                </a>
            </p>
            <p>
                <a href="{{ url('register') }}">
                    Регистрация
                </a>
            </p>
            <p>
                <a href="{{ url('/') }}">
                    На главную
                </a>
            </p>
        </div>
    </div>

@endsection
