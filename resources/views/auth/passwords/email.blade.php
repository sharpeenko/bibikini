@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('password.reset') !!}
@endsection
@section('header')
      <h1>Восстановление пароля <small>Создайте новый пароль</small></h1>
@endsection
@section('content')


    <div class="row">
        <div class="col-lg-8">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email" class="col-sm-2">Эл. почта <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Введите email" value="{{old('email')}}">
                        <p class="help-block text-danger">{!! $errors->first('email') !!}</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Сброс пароля</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
