@extends('layouts.bibikini')
@section('breadcrumbs')
  <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Главная</a></li>
      <li class="active">Новый пароль</li>
  </ol>
@endsection
@section('header')
      <h1>Новый пароль <small>Создайте новый пароль</small></h1>
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-8">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <label for="email" class="col-sm-2">Эл. почта <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Введите e-mail" value="{{ $email or old('email') }}">
                        <p class="help-block text-danger">{!! $errors->first('email') !!}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2">Пароль <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" placeholder="Введите пароль">
                        <p class="help-block text-danger">{!! $errors->first('password') !!}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password_confirm" class="col-sm-2">Повторите пароль<span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Повторите пароль">
                        <p class="help-block text-danger">{!! $errors->first('password_confirmation') !!}</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Подтвердить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
