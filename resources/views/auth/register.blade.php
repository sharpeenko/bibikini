@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('register') !!}
@endsection
@section('header')
  <h1>Регистрация</h1>
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-8">

            <form action="{{ url('/register') }}" class="form-horizontal" method="POST" role="form">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-sm-2" for="name">
                        Имя пользователя
                        <span class="text-danger">
                            *
                        </span>
                    </label>
                    <div class="col-sm-10">
                        <input autofocus="" class="form-control" name="name" placeholder="Введите имя пользователя" required="" type="text" value="{{ old('name') }}">
                            <p class="help-block text-danger">
                                {!! $errors->first('name') !!}
                            </p>
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2" for="email">
                        Эл. почта
                        <span class="text-danger">
                            *
                        </span>
                    </label>
                    <div class="col-sm-10">
                        <input class="form-control" name="email" placeholder="Введите email" type="email" value="{{old('email')}}">
                            <p class="help-block text-danger">
                                {!! $errors->first('email') !!}
                            </p>
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2" for="password">
                        Пароль
                        <span class="text-danger">
                            *
                        </span>
                    </label>
                    <div class="col-sm-10">
                        <input class="form-control" name="password" placeholder="Введите пароль" type="password">
                            <p class="help-block text-danger">
                                {!! $errors->first('password') !!}
                            </p>
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2" for="password_confirmation">
                        Повторите пароль
                        <span class="text-danger">
                            *
                        </span>
                    </label>
                    <div class="col-sm-10">
                        <input class="form-control" name="password_confirmation" placeholder="Повторите пароль" type="password">
                            <p class="help-block text-danger">
                                {!! $errors->first('password_confirmation') !!}
                            </p>
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <p>
                          Нажимая на кнопку "Зарегистрироваться" Вы подтверждаете, что принимаете
                          <a href="{{route('user_agreement')}}">Пользовательское соглашение</a> а также предоставляете свое
                          <a href="{{route('personal_data.index')}}">согласие на обработку Ваших персональных данных</a>.
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-success" type="submit">
                            Зарегистрироваться
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
