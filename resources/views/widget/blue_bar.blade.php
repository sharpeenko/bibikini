<div class="container-fluid">


<div class="row visible-xs visible-sm visible-md">
    <div class="link-mobile">
      <div class="btn-group">
        @if (Auth::user())
          @if(Auth::user()->avatar=='default')
             @php
             Auth::user()->avatar= asset('assets/img/default/avatar.png');
             @endphp
          @endif
    <button type="button"  data-toggle="dropdown">
      <img style="max-height: 46px; display: inline-block; margin-bottom: 0;" src="{{asset(Auth::user()->avatar)}}" alt="{{Auth::user()->name}}" class="thumbnail img-responsive">
       <i class="fa fa-chevron-down" aria-hidden="true"></i>
     </button>
    <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('users.edit', [Auth::user()->id])}}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;{{Auth::user()->name}}</a></li>
      <li><a href="{{route('users.edit', [Auth::user()->id])}}#mynominations"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;Мои номинации</a></li>
      <li><a href="{{route('users.edit', [Auth::user()->id])}}#myphotos"><i class="fa fa-camera" aria-hidden="true"></i>&nbsp;Мои фото</a></li>
      <li><a href="{{route('users.edit', [Auth::user()->id])}}#myratings"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;Мои рейтинги</a></li>
      <li><a href="{{route('users.comments', [Auth::user()->id])}}"><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;Мои комментарии</a></li>
      <li><a href="{{route('like.buy', [Auth::user()->id])}}"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;Купить</a></li>
      @permission('adminpanel_access')
      <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;Панель управления</a></li>
      @endpermission @role('testreferee')
      <li><a href="{{route('referee.index')}}"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;Судейство</a></li>
      @endrole
      <li class="divider"></li>
      <li>
        <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Выход</a>
      </li>
      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>
    </ul>

  @else
    <button type="button"  data-toggle="modal" data-target="#modalLogin">

      <strong>Вход</strong>
      <i class="fa fa-chevron-down" aria-hidden="true"></i>
    </button>
  @endif
  </div>
    </div>
  </div>

  </div>
