<div class="table-responsive">
    <table id="table-referees" class="table scores" data-avg="
    @if(!empty($avgScore))
    {{$avgScore->first()->avgscore}}
   @endif
    "
    data-rating="
    @if(!empty($rating))
    {{$rating}}
   @endif
    "
    >
        @foreach ($referees as $referee)
         <tr>
             <td class="photo">
              <div class="thumbnail">
                @if($referee->user->avatar=='default')
                   @php
                   $referee->user->avatar= asset('assets/img/default/avatar.png');
                   @endphp
                @endif
                  <a href="{{route('users.show', [$referee->user->id])}}"><img src="{{asset($referee->user->avatar)}}" /></a>
              </div>
             </td>
             @if ($referee->review->count() > 0)
              <td class="revize">
              <small><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;{{$referee->review->count()}}</small>
             @else
              <td class="revize no">
              <small><i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;0</small>
             @endif
            </td>
             </td>
             @if (!empty($referee->scoreActual->first()->score))
               <td class="score">
                 <small class="score-actual"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;{{$referee->scoreActual->first()->score}}</small>
               </td>
               <td class="score">
                 <small>{{ date('d.m.Y', strtotime($referee->scoreActual->first()->created_at)) }}</small>
               </td>
              @else
               <td class="score no">
                 <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;0</small>
                </td>
                <td class="score">
                  <small></small>
                </td>
              @endif
              @if(!empty($lastPhoto) && !empty($referee->scoreActual->first()->score))
                @if($lastPhoto > $referee->scoreActual->first()->created_at)
                 <td class="score no">
                   <small><i class="fa fa-camera" aria-hidden="true"></i></small>
                 </td>
                @else
                 <td class="score">
                   <small></small>
                 </td>
                @endif
              @else
               <td class="score">
                 <small></small>
               </td>
              @endif


             <td class="score">
              <button type="button" class="btn btn-xs btn-info" data-toggle="collapse" data-target="#editdata_{{$referee->id}}">
                  История
              </button>
             </td>


         </tr>
         <tr id="editdata_{{$referee->id}}" class="collapse">

             <td class="td-collapse"></td>
             <td class="td-collapse"></td>
             <td class="score td-collapse">
              @foreach($referee->score as $score)
               @if($score->is_actual == 0)
                <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;{{$score->score}}</small>&nbsp;&nbsp;&nbsp;&nbsp;<small>{{ date('d.m.Y', strtotime($score->created_at)) }}</small>
               <br>
               @else
                <small class="score-actual"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;{{$score->score}}</small>&nbsp;&nbsp;&nbsp;&nbsp;<small>{{ date('d.m.Y', strtotime($score->created_at)) }}</small>
                <br>
               @endif
              @endforeach

             </td>
         </tr>
        @endforeach


    </table>
</div>
