<div class="row">
    <div class="col-lg-12">
        <div class="informer">
                  @foreach ($contents as $content)
                        @if ($content->key == $content_key)
                            {!! $content->content !!}
                        @endif
                  @endforeach
                <p class="pull-left">
                    <button type="button" class="btn btn-danger btn-lg">Закрыть&nbsp; <i class="fa fa-times" aria-hidden="true"></i></button>
                </p>
                <p class="pull-right">
                    <input id="notshow" data-url="{{$cookie_url}}" type="checkbox"> Больше не показывать это сообщение
                </p>
                <div class="clearfix"></div>
        </div>
    </div>

</div>
