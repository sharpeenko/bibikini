@if (!Auth::user())
<div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
    <a class="btn btn-primary" href="/register">Регистрация</a>
    <a class="btn btn-danger" href="/help#quick-start">Старт</a>
    <a class="btn btn-success" href="/help">Справка</a>
</div>
@endif
