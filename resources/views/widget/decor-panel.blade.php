<br>
<div class="row">
  <div class="col-lg-12">

</div>
  @if (Auth::user())
    <a href="{{route('like.buy', [Auth::user()->id])}}" class="decor col-md-2 col-xs-4 text-center">
      <p class="lead color-red"><i class="fa fa-heart" aria-hidden="true"></i></p>
      <p  class="text-muted">Лайки</p>
    </a>

  @else
    <div class="decor col-md-2 col-xs-4 text-center">
      <p class="lead text-muted"><i class="fa fa-heart" aria-hidden="true"></i></p>
      <p  class="text-muted">Лайки</p>
    </div>
  @endif

 <a href="{{url('/nomination')}}" class="decor col-md-2 col-xs-4 text-center">
    <p class="lead color-gold"><i class="fa fa-trophy" aria-hidden="true"></i></p>
    <p  class="text-warning">Номинации</p>
  </a>
  <a href="{{url('/rating')}}" class="decor col-md-2 col-xs-4 text-center">
    <p class="lead color-black"><i class="fa fa-bar-chart" aria-hidden="true"></i></p>
    <p  class="text-danger">Рейтинги</p>
  </a>
  <a href="{{url('/fund')}}" class="decor col-md-2 col-xs-4 text-center">
    <p class="lead color-green-bibikini"><i class="fa fa-money" aria-hidden="true"></i></p>
    <p  class="text-muted">Призы</p>
  </a>
  <a href="{{url('/photo')}}" class="decor col-md-2 col-xs-4 text-center">
    <p class="lead color-blue"><i class="fa fa-camera-retro" aria-hidden="true"></i></p>
    <p  class="text-muted">Фото</p>
  </a>
  <a href="{{url('/competitor')}}" class="decor col-md-2 col-xs-4 text-center">
    <p class="lead"><span class="color-blue"><i class="fa fa-male" aria-hidden="true"></i></span>
      <span class="color-green-bibikini"><i class="fa fa-female" aria-hidden="true"></i></span></p>
    <p  class="text-muted">Участники</p>
  </a>

</div>
