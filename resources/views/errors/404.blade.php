@extends('layouts.meta')
@section('body')
<div class="container">
    <div class="row">

        <div class="error-header">
            <h1>404</h1>
            <h2>Страница не найдена</h2>
            <div class="error-details">
                Извините, такой страницы не существует.<br>
            </div>
        </div>

        <div class="error-action">
            <a href="{{url('/')}}" class="btn btn-primary">
                <i class="fa fa-home" aria-hidden="true"></i>&nbsp; На главную </a>
            <a href="{{url('/feedback')}}" class="btn btn-default">
                <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;Написать </a>
        </div>

    </div>
</div>
@endsection

