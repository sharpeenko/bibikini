
<!DOCTYPE html>
<html>

    <head>

        <title>СЕО-заголовок страницы</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="СЕО-описание страницы">
        <meta name="author" content="">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Freelancer - Start Bootstrap Theme</title>

        <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
        <link href= "{{ asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

        <!--Lightbox CSS-->
        <link href= "{{ asset('assets/lightbox/css/lightbox.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href= "{{ asset('assets/css/styles.css')}}" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href= "{{ asset('assets/bootstrap/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Scripts -->
        <script>
            window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
        </script>

    </head>

    <body>
        <header id="header">
            <div class="menu-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10">
                            <ul>
                                <li>
                                    <a href="">О сайте</a>
                                </li>
                                <li>
                                    <a href="">Соревнования</a>
                                </li>
                                <li>
                                    <a {{MenuHelper::getActiveClass('users')}} href="{{url('/users')}}">Пользователи</a>
                                </li>
                                <li>
                                    <a  {{MenuHelper::getActiveClass('news')}} href="{{url('/news')}}">Новости</a>
                                </li>
                                <li>
                                    <a  {{MenuHelper::getActiveClass('feedback')}} href="{{url('/feedback')}}">Написать</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-2">
                            <div class="link-auth">
                                @if (Auth::user())
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                                    <i class="fa fa-unlock-alt" aria-hidden="true"></i>&nbsp;Выход
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                @else
                                <a href="{{url('/login')}}"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;Вход</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-area">
                <div class="panel-us collapse" id="panel1">
                    <button class="panel-close" data-toggle="collapse" data-target="#panel1">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <div class="clearfix"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="link-auth">
                                    @if (Auth::user())
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                        <i class="fa fa-unlock-alt" aria-hidden="true"></i>&nbsp;Выход
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    @else
                                    <a href="{{url('/login')}}"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;Вход</a>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="menu-mobile">
                                    <ul>
                                        <li>
                                            @if (Auth::check())
                                            <a {{MenuHelper::getActiveClass('users/'.Auth::user()->id.'/edit')}} href="{{route('users.edit', [Auth::user()->id])}}">Профиль</a>
                                            @else
                                            <a href="{{url('/noaccess')}}">Профиль</a>
                                            @endif
                                        </li>
                                        <li>
                                            <a {{MenuHelper::getActiveClass('users')}} href="{{url('/users')}}">Пользователи</a>
                                        </li>
                                        <li>
                                            <a href="">Фото</a>
                                        </li>
                                        <li>
                                            <a href="">Результаты</a>
                                        </li>
                                        <li>
                                            <a  {{MenuHelper::getActiveClass('news')}} href="{{url('/news')}}">Новости</a>
                                        </li>
                                        <li>
                                            <a  {{MenuHelper::getActiveClass('feedback')}} href="{{url('/feedback')}}">Написать</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="container">

                    <div class="row">
                        <div class="col-sm-4">
                            <a class="logo" href="/"></a>
                            <button class="show-bar" data-toggle="collapse" data-target="#panel1">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-sm-8">
                            <ul>
                                <li>
                                    <a href="{{url('/nomination')}}">
                                        <div class="ll">
                                            <i class="fa fa-trophy" aria-hidden="true"></i>
                                        </div>
                                        <div class="lr">
                                            Все номинации
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>

                                </li>
                                <li>
                                    <a href="">
                                        <div class="ll">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </div>
                                        <div class="lr">
                                            Купить лайки
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <div class="ll">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                        </div>
                                        <div class="lr">
                                            Рейтинг сегодня
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <div class="ll">
                                            <i class="fa fa-coffee" aria-hidden="true"></i>
                                        </div>
                                        <div class="lr">
                                            Мне повезет!
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-bottom" >
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>
                                    @if (Auth::check())
                                    <a {{MenuHelper::getActiveClass('users/'.Auth::user()->id.'/edit')}} href="{{route('users.edit', [Auth::user()->id])}}">Профиль</a>
                                    @else
                                    <a href="{{url('/noaccess')}}">Профиль</a>
                                    @endif
                                </li>
                                <li>
                                    <a href="">Фото</a>
                                </li>
                                <li>
                                    <a href="">Результаты</a>
                                </li>
                                <li>
                                    <a  {{MenuHelper::getActiveClass('news')}} href="{{url('/news')}}">Новости</a>
                                </li>
                                <li>
                                    <a  {{MenuHelper::getActiveClass('feedback')}} href="{{url('/feedback')}}">Написать</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </header>

        @include('errors.errmsg')
        @yield('content')

        <footer id="footer">
            <div class="menu-top">

            </div>
            <div class="footer-area">

            </div>
            <!-- jQuery -->
            <script src="//code.jquery.com/jquery.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src= "{{ asset('assets/bootstrap/js/bootstrap.min.js')}}" ></script>

            <!--Lightbox JS-->
            <script src= "{{ asset('assets/lightbox/js/lightbox.js')}}" http://bibikini.com.dev/></script>

            <!--File input-->
            <script src= "{{ asset('assets/js/file-input.js')}}"></script>

            <!-- Plugin JavaScript -->
            <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
            <script src= "{{ asset('assets/bootstrap/js/classie.js')}}"></script>
            <script src= "{{ asset('assets/bootstrap/js/cbpAnimatedHeader.min.js')}}"></script>

            <!-- Local -->
            <script src= "{{ asset('assets/js/local.js')}}"></script>

        </footer>

    </body>

</html>







