<!DOCTYPE html>
<html>
    <head>
      @if (isset($seo))
      <title>{{$seo->title}}</title>
      @else
      <title>Интернет-чемпионат по бодибилдингу и фитнес-бикини facefront.ru</title>
      @endif

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @if (isset($seo))
        <meta name="description" content="{{$seo->description}}">
        @else
        <meta name="description" content="Интернет чемпионат по бодибилдингу и фитнес-бикини facefront.ru">
        @endif

        <meta name="author" content="">

        <!--Иконки-->
        <link rel="icon" href="/favicon.png" type="image/png">
        <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!--<link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">-->
        <link href="{{ asset('assets/jasny/css/jasny-bootstrap.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/css/bibikini.css') }}" rel="stylesheet" media="screen">
        <!--Слайдер-->
        <link href="http://atmpl.ru/design/adaptive-slider/2015/bootstrap-atmpl/slider-bootstrap-slidePinwheel.css" rel="stylesheet" />
        <!--Слайдер-->
        <link href= "{{ asset('assets/bootstrap/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <!--Лайтбокс-->
        <link href= "{{ asset('assets/ekko-lightbox/ekko-lightbox.min.css') }}" rel="stylesheet" type="text/css">
        <!--Лайтбокс-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- Scripts -->
        <script>
            window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
        </script>
        @include('widget.metrika')
    </head>
    <body>
        @yield('body')
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://code.jquery.com/jquery.js"></script>
        <!--Cookie-->
        <script src="{{ asset('assets/js/cookie.js') }}"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/jasny/js/jasny-bootstrap.min.js') }}"></script>
        <!--Local-->
        <script src="{{ asset('assets/js/bibikini.js') }}"></script>
        <!--Лайтбокс-->
        <script src="{{ asset('assets/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
        <!--Лайтбокс-->
        <!--File input-->
        <script src= "{{ asset('assets/js/file-input.js')}}"></script>
    </body>
</html>
