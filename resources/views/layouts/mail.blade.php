<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            body table {
                width: 100%;
            }
            body table tr td {
                padding: 30px;
            }
            body table tr td p a {
                color: #0083C9;
                text-decoration: none;
            }
            body table tr td.mail-head {
                height: 30px;
                background-color: #0083C9;
            }
        </style>
    </head>
    <body>
        <table>

            <tr>
                <td class="mail-head">
                    <a href="{{url('/')}}"><img src="{{asset('assets/img/logo/logo-white.png')}}"></a>
                </td>
            </tr>

            @yield('content')

        </table>
    </body>
</html>
