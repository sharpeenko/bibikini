@extends('layouts.meta')
@section('body')
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      <a class="navbar-brand" href="/">
                <img src="/assets/img/logo/logo.png"/>
            </a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown visible-lg visible-md">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-trophy color-gold" aria-hidden="true"></i>&nbsp;Номинации <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{url('/nomination')}}"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;Все номинации</a></li>
            <li><a href="{{url('/fund')}}"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Призовые фонды</a></li>
            <li><a href="{{url('/champion')}}"><i class="fa fa-child"></i>&nbsp;Чемпионы</a></li>
        </ul>
        </li>
        <li class="hidden-md hidden-lg"><a href="{{url('/nomination')}}"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;Номинации</a></li>
        <li class="hidden-md hidden-lg"><a href="{{url('/fund')}}"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;Призовые фонды</a></li>
        <li class="hidden-md hidden-lg"><a href="{{url('/champion')}}"><i class="fa fa-child"></i>&nbsp;Чемпионы</a></li>
        <li {{MenuHelper::getActiveClass( 'photo')}}><a href="{{url('/photo')}}"><i class="fa fa-camera" aria-hidden="true"></i>&nbsp;Фото</a></li>
        <li {{MenuHelper::getActiveClass( 'rating')}}><a href="{{url('/rating')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;Рейтинги</a></li>
        <li class="hidden-md hidden-lg"><a href="{{url('/news')}}"><i class="fa fa-newspaper-o"></i>&nbsp;Новости</a></li>
        <li {{MenuHelper::getActiveClass( 'competitor')}}><a href="{{url('/competitor')}}"><i class="fa fa-male" aria-hidden="true"></i><i class="fa fa-female" aria-hidden="true"></i>&nbsp;Участники</a></li>
        <li {{MenuHelper::getActiveClass( 'feedback')}}><a href="{{url('/feedback')}}"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;Написать</a></li>


        <li class="dropdown visible-lg visible-md">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;О сайте <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{url('/users')}}"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;Пользователи</a></li>
            <li><a href="{{url('/news')}}"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Новости</a></li>
            <li class="divider"></li>
            <li><a href="{{url('/site-description')}}"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;O Facefront</a></li>
            <li><a href="{{url('/user-agreement')}}"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;Пользовательское соглашение</a></li>
            <li><a href="{{url('/help')}}"><i class="fa fa-question" aria-hidden="true"></i>&nbsp;Справка</a></li>
          </ul>
        </li>
        @if (Auth::user())
          <li class=""><a href="{{route('like.buy', [Auth::user()->id])}}"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;Купить</a></li>
        @else
          <li class="disabled"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;Купить</a></li>
        @endif
        <li class="hidden-md hidden-lg"><a href="{{url('/users')}}"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;Пользователи</a></li>
        <li class="hidden-md hidden-lg"><a href="{{url('/site-description')}}"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;O Facefront</a></li>
        <li class="hidden-md hidden-lg"><a href="{{url('/help')}}"><i class="fa fa-question" aria-hidden="true"></i>&nbsp;Справка</a></li>
        <li class="hidden-md hidden-lg"><a href="{{url('/user-agreement')}}"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;Пользовательское соглашение</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::user())
        <li class="dropdown visible-lg visible-md">
          @if(Auth::user()->avatar=='default')
             @php
             Auth::user()->avatar= asset('assets/img/default/avatar.png');
             @endphp
          @endif
          <a href="#" class="dropdown-toggle visible-lg visible-md avatar" data-toggle="dropdown"><img style="max-height: 46px; display: inline-block; margin-bottom: 0;" src="{{asset(Auth::user()->avatar)}}" alt="{{Auth::user()->name}}" class="thumbnail img-responsive"><b class="caret"></b></a>
          <a href="#" class="dropdown-toggle visible-sm visible-xs" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;{{Auth::user()->name}}<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{route('users.edit', [Auth::user()->id])}}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;{{Auth::user()->name}}</a></li>
            <li><a href="{{route('users.edit', [Auth::user()->id])}}#mynominations"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;Мои номинации</a></li>
            <li><a href="{{route('users.edit', [Auth::user()->id])}}#myphotos"><i class="fa fa-camera" aria-hidden="true"></i>&nbsp;Мои фото</a></li>
            <li><a href="{{route('users.edit', [Auth::user()->id])}}#myratings"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;Мои рейтинги</a></li>
            <li><a href="{{route('users.comments', [Auth::user()->id])}}"><i class="fa fa-comment" aria-hidden="true"></i>&nbsp;Мои комментарии</a></li>
            <li><a href="{{route('like.buy', [Auth::user()->id])}}"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;Купить</a></li>
            @permission('adminpanel_access')
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;Панель управления</a></li>
            @endpermission
            @role('testreferee')
            <li><a href="{{route('referee.index')}}"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;Судейство</a></li>
            @endrole
            <li class="divider"></li>
            <li>
              <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Выход</a>
            </li>
          </ul>
        </li>
        <li class="hidden-md hidden-lg"><a href="{{route('users.edit', [Auth::user()->id])}}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;{{Auth::user()->name}}</a></li>
        <li class="hidden-md hidden-lg"><a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Выход</a></li>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
        @else
        <li><a href="" data-toggle="modal" data-target="#modalLogin"><i class="fa fa-key" aria-hidden="true"></i> Вход</a></li>
        @endif
      </ul>
    </div> {{--/.nav-collapse--}}
  </div>
</div>

@include('modals.login') {{--@include('modals.informer')--}}
@include('errors.errmsg')
@include('widget.registration_link')
@include('widget.blue_bar')
<div class="container">
    <div class="row hidden-xs">
      <div class="col-lg-12">
          @yield('breadcrumbs')
      </div>
  </div>

  <div class="row">
    <div class="col-md-12">
        <div class="page-header">
            @yield('header')
        </div>
      </div>
    </div>

    {{--@yield('help-button')--}}
    {{--@yield('informer')--}}

    @yield('content')
    @include('widget.groups')
</div>
<div id="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-right">
        <ul class="footer-nav">
          <li>
            <a href=""><i class="fa fa-copyright" aria-hidden="true"></i>&nbsp;facefront 2018</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<span id="top-link-block" class="hidden text-center">
    <a href="#top" onclick="$('html,body').animate({scrollTop: 0}, 'slow');return false;">
        <i class="glyphicon glyphicon-chevron-up"></i>
    </a>
</span>
@endsection
