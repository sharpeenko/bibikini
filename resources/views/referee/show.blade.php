@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('referee.show', $referee->user->id) !!}
@endsection
@section('header')
        <h1>{{$referee->user->name}} <small>Участники Вашей номинации</small></h1>
@endsection
@section('content')
@include('modals.score')


    <div class="row">
     <div class="col-lg-12">
      <div class="table-responsive">
       <table class="table scores table-hover">
        @foreach($competitors as $competitor)
         <tr>
             <td class="photo">
              <div class="thumbnail">
                @if($competitor->avatar=='default')
                   @php
                   $competitor->avatar= asset('assets/img/default/avatar.png');
                   @endphp
                @endif
                  <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}"><img src="{{asset($competitor->avatar)}}" /></a>
              </div>
             </td>

              @if ($competitor->review->count() > 0)
               <td class="revize">
               <small><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;{{$competitor->review->count()}}</small>
              @else
               <td class="revize no">
               <small><i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;0</small>
              @endif
             </td>
             @if (!empty($competitor->score->first()->score))
              <td class="score">
                <small class="score-actual"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;{{$competitor->score->first()->score}}</small>
               </td>
               <td class="score">
                <small>{{ date('d.m.Y', strtotime($competitor->score->first()->created_at)) }}</small>
               </td>
             @else
              <td class="score no">
                <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;0</small>
              </td>
              <td class="score">
               <small></small>
              </td>
             @endif
             @if(!empty($competitor->last_photo) && !empty($competitor->score->first()->score))
               @if($competitor->last_photo > $competitor->score->first()->created_at)
                <td class="score no">
                  <small><i class="fa fa-camera" aria-hidden="true"></i></small>
                </td>
               @else
                <td class="score">
                  <small></small>
                </td>
               @endif
             @else
              <td class="score">
                <small></small>
              </td>
             @endif
             @if ($competitor->is_additional_round == 1)
              <td class="revize">
              <small><i class="fa fa-circle color-green-bibikini " aria-hidden="true"></i></small>
             @else
              <td class="revize">
              <small></small>
             @endif
            </td>

             <td class="score">
              <button class="btn btn-xs btn-primary view-score" data-toggle="modal" data-target="#modalScore" data-competitor="{{$competitor->id}}" >Просмотр</button>
             </td>
         </tr>
        @endforeach
    </table>
      </div>
     </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
             {{ $competitors->links() }}
        </div>
    </div>





@endsection
