
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Просмотр и оценка участника</h4>
        </div>
        <div class="modal-body">

            <div class="container-fluid">

             <div class="row">
              <div class="col-sm-12">
              <h3>Фотогалерея <small>Занесут скоро</small></h3>
              @foreach ($competitor->getPhoto as $photo)
               <div class="col-sm-2">
                   <a href="{{$photo->photo}}" data-toggle="lightbox" data-title="" data-footer="" data-gallery="competitor_{{$competitor->id}}">
                    <img src="{{$photo->photo}}" class="img-responsive img-thumbnail pull-left img-content img-left" alt="">
                   </a>
               </div>
              @endforeach
             </div>
             </div>
              <div class="row">


               <div class="col-sm-12">
                <h3>История оценок <small>Занесут скоро</small></h3>
                <div class="table-responsive">
                    <table id="table-history" class="table scores">
                     @foreach ($competitor->score as $score)
                      <tr>
                          <td ></td>
                          <td class="score"><small>{{ date('d.m.Y', strtotime($score->created_at)) }}</small></td>
                          <td class="score">
                           @if($score->is_actual == 0)
                           <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;{{$score->score}}</small>
                           @else
                            <small class="score-actual"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;{{$score->score}}</small>
                           @endif
                          </td>
                         </tr>
                     @endforeach
                        </table>
                  </div>
                </div>
              </div>
              <div class="row">

               <div class="col-sm-12">
                <h3>Поставить оценку <small>И можно написать комментарий</small></h3>
                <div id="alert-block">
                </div>
                @if (!empty($competitor->additional_round_description))
                 <p class="text-warning bg-warning">
                   Оценивать в диапазоне {{$competitor->additional_round_description}}
                 </p>
                @endif
                <form class="form-horizontal" role="form" method="POST" id="frm-set-score"  action="{{route('referee.score.store', ['competitor' => $competitor->id])}}" >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="beforepoint" class="col-sm-2 control-label">Целых <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                         <select name="beforepoint" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="afterpoint" class="col-sm-2 control-label">Десятых <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                         <select name="afterpoint" class="form-control">
                               <option value="1">1</option>
                               <option value="2">2</option>
                               <option value="3">3</option>
                               <option value="4">4</option>
                               <option value="5">5</option>
                               <option value="6">6</option>
                               <option value="7">7</option>
                               <option value="8">8</option>
                               <option value="9">9</option>
                               <option value="0">0</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                     <label for="comment" class="col-sm-2 control-label">Комментарий </label>
                        <div class="col-sm-10">
                            <textarea id="text-addcomment" class="form-control" rows="5" name="comment" placeholder="Максимальная длина 1000 знаков"></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="addcomment"> Добавить комментарий к оценке. Нет комментария - снимите флажок!
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Оценка</button>

                        </div>
                    </div>
                </form>
               </div>
              </div>
             </div>
            </div>
            <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
 </div>
    </div>
</div>
<script>
//Установка оценки через ajax
var setScoreForm = $("#frm-set-score");
setScoreForm.submit(function(e) {
 e.preventDefault();
 var $that = $(this);
 formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
 $('#modalScore').find('#alert-block').html("");
 $.ajax({
  url: $that.attr('action'),
  type: $that.attr('method'),
  contentType: false, // важно - убираем форматирование данных по умолчанию
  processData: false, // важно - убираем преобразование строк по умолчанию
  data: formData,
  dataType: 'json',
  success: function(data) {
   if (data.success_message) {
     $('#modalScore').find('#alert-block').append('<div class=\"alert alert-success alert-block\">' +
     '<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"fa fa-minus-square\"></i></button>' +
     data.success_message + '</div>');
     $('#table-history').find('small').removeClass('score-actual');
     $('#table-history > tbody:last').append('<tr>'+
       '<td ></td>' +
       '<td class=\"score\"><small>'+data.score_date+'</small></td>'+
       '<td class=\"score\"><small class=\"score-actual\"><i class=\"fa fa-graduation-cap\" aria-hidden=\"true\"></i>&nbsp;'+data.score_value+'</small></td>'+
      '</tr>' );


   } else {

    $('#modalScore').find('#alert-block').append('<div class=\"alert alert-danger alert-block\">' +
     '<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"fa fa-minus-square\"></i></button>' +
     data.error_message + '</div>');

   }

  },
  error: function(response) {

   /**
    * Так как текст возврата не удается установить на сервере, определяем в js, в зависимости от
    * ошибки из json-строки
    */
   var jsonErrors = JSON.parse(response.responseText);
   //if (jsonErrors.photo == 'validation.uploaded') {
    $('#modalScore').find('#alert-block').append('<div class=\"alert alert-danger alert-block\">' +
     '<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"fa fa-minus-square\"></i></button>' +
     'Оценка недействительна' + '</div>');
   //}

  }
 });
});
</script>
