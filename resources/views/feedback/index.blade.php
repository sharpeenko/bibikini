@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('feedback.index') !!}
@endsection
@section('header')
      <h1>Обратная связь <small>Написать нам</small></h1>
@endsection
@section('content')


    <div class="row">
        <div class="col-lg-8">
            <form class="form-horizontal" role="form" method="POST" enctype = "multipart/form-data" action="{{ url('/feedback/send') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email" class="col-sm-2">Эл. почта <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Введите email" value="{{old('email')}}">
                        <p class="help-block text-danger">{!! $errors->first('email') !!}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2">Как обращаться </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" placeholder="Введите имя" value="{{old('name')}}">
                        <p class="help-block text-danger">{!! $errors->first('name') !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label for="messmail" class="col-sm-2">Сообщение <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" name="messmail" placeholder="Введите сообщение">{{old('messmail')}}</textarea>
                        <p class="help-block text-danger">{!! $errors->first('messmail') !!}</p>
                    </div>
                </div>
                {{--<div class="form-group">
                    <label for="attachment" class="col-sm-2"></label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control btn-primary" name="attachment" title="Вложение">
                        <p class="help-block text-danger">{!! $errors->first('attachment') !!}</p>
                    </div>
                </div>--}}
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <p>
                          Нажимая на кнопку "Отправить" Вы подтверждаете, что предоставляете свое
                          <a href="{{route('personal_data.index')}}">согласие на обработку Ваших персональных данных</a>.
                        </p>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
