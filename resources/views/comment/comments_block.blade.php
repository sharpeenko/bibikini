

            <div class="page-header">
                <h3>Комментарии <small></small></h3>
            </div>
            @if (Auth::user())
            <p class="lead">
                Вы можете прокомментировать
            </p>
            @else
            <p class="lead">
                Комментировать могут только зарегистрированные пользователи.
                Выполните <a href="{{url('/login')}}">вход на сайт</a>
            </p>
            @endif
            @foreach ($comments as $comment)
             @if (Nomination::isReferee($comment->author->id))
              <div class="media comment-content referee">
                  <a name="comment_{{$comment->id}}"></a>
                  <h4 class="media-heading"><a href="{{url('/users/'.$comment->user_id)}}"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;{{$comment->author->name}}</a> <small>{{ date('Y-m-d H:i:s', strtotime($comment->publish_at)) }}</small></h4>
                  <a class="pull-left" href="{{url('/users/'.$comment->user_id)}}">
                      <img class="media-object img-thumbnail" src="{{asset($comment->author->avatar)}}" alt="{{$comment->author->name}}">
                  </a>
                   <span class="text-warning">{{$comment->content}}</span>
                  <div class="clearfix"></div>
                  @if (Auth::user())
                  <p class="text-right">
                      <a href="#addcomment" data-user="{{$comment->author->name}}" class="link-addcomment btn btn-md btn-primary">Ответить</a>
                      <a href="{{route('comment.info.moderator', [$comment->id, Auth::user()->id])}}" class="btn btn-md btn-danger">Пожаловаться</a>
                  </p>
                  @else
                  <p class="text-right">
                      <a href="" class="btn btn-md btn-primary disabled">Ответить</a>
                      <a href="" class="btn btn-md btn-danger disabled">Пожаловаться</a>
                  </p>
                  @endif

              </div>
             @else
              <div class="media comment-content">
                  <a name="comment_{{$comment->id}}"></a>
                  <h4 class="media-heading"><a href="{{url('/users/'.$comment->user_id)}}"><i class="fa fa-male" aria-hidden="true"></i>&nbsp;
                  {{$comment->author->name}}</a> <small>{{ date('Y-m-d H:i:s', strtotime($comment->publish_at)) }}</small></h4>
              <a class="pull-left" href="{{url('/users/'.$comment->user_id)}}">
                  <img class="media-object img-thumbnail" src="{{asset($comment->author->avatar)}}" alt="{{$comment->author->name}}">
              </a>
               {{$comment->content}}
              <div class="clearfix"></div>
              @if (Auth::user())
              <p class="text-right">
                  <a href="#addcomment" data-user="{{$comment->author->name}}" class="link-addcomment btn btn-md btn-primary">Ответить</a>
                  <a href="{{route('comment.info.moderator', [$comment->id, Auth::user()->id])}}" class="btn btn-md btn-danger">Пожаловаться</a>
              </p>
              @else
              <p class="text-right">
                  <a href="" class="btn btn-md btn-primary disabled">Ответить</a>
                  <a href="" class="btn btn-md btn-danger disabled">Пожаловаться</a>
              </p>
              @endif

          </div>
          @endif


            @endforeach

            @if (Auth::user())
            <a name="addcomment"></a>
            <p class="lead">
                Вы можете прокомментировать
            </p>
            <div class="col-sm-12">
              <form class="form-horizontal" role="form" method="POST" action="{{ route($url, [$instance,  Auth::user()->id]) }}">
                {{ csrf_field() }}
                    <div class="form-group">
                        <div class="col-sm-12">
                            <textarea id="text-addcomment" class="form-control" rows="5" name="comment" placeholder="Максимальная длина 1000 знаков">
                            {{old('comment')}}
                            </textarea>
                            <p class="help-block text-danger">{!! $errors->first('comment') !!}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Добавить комментарий</button>
                        </div>
                    </div>
                </form>
            </div>
            @else
            <p class="lead">
                Комментировать могут только зарегистрированные пользователи.
                Выполните <a href="{{url('/login')}}">вход на сайт</a>
            </p>
            @endif
