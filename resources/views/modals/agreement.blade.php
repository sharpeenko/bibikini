<div class="modal fade" id="modalAgreement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div id="mod1" class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Заявка на участие</h4>
            </div>
            <div class="modal-body">
              @foreach ($contents as $content)
                    @if ($content->key == 'nomination_agreement')
                        {!! $content->content !!}
                    @endif
              @endforeach
              <form class="form-horizontal" role="form" method="POST" action="{{ route('competitor.create', [$nomination->id]) }}">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Участвовать</button>
                
              </form>
            </div>
        </div>
    </div>
</div>
