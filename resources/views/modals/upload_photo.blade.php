<div class="modal fade" id="modalUploadPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Загрузить фото</h4>
            </div>
            <div class="modal-body">
                <div id="alert-block">
                </div>
                
        @foreach ($competitor->getPhoto as $photo)
         @if($photo->photo_status_id == 1)
            <div class="col-md-3 col-sm-3 col-xs-4">
	            	<div class="thumbnail photo">
		                <div class="pic mini download">
		                    <img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="...">
		                    <div class="pic-bar">
                        <p class="text-center">
                            Проверка
                        </p>
                    </div>
		                </div>

		            </div>
		        </div>
            
               
                
            @elseif($photo->photo_status_id == 2)
                <div class="col-md-3 col-sm-3 col-xs-4">
	            	<div class="thumbnail photo">
		                <div class="pic mini">
		                    <img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="...">
		                   
		                </div>

		            </div>
		        </div>
                
            @else
                <div class="col-md-3 col-sm-3 col-xs-4">
	            	<div class="thumbnail photo">
		                <div class="pic mini rejected">
		                    <img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="...">
		                    <div class="pic-bar">
                        <p class="text-center">
                            Отклонена
                        </p>
                    </div>
		                </div>

		            </div>
		        </div>
                
        @endif
        @endforeach
                <div id="last-photo" class="clearfix"></div>
                <div id="photo-count" class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                        По правилам номинации можно загрузить не более {{$competitor->photo_count}} фото.
                        Осталось {{$competitor->photo_balance}} фото.
                </div>
		        <div class="alert alert-danger">
		        		<button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
                        Размер загружаемого фото не должен превышать 1 мб.
                </div>
		        <form class="form-horizontal incollapse" role="form" action="{{route('photo.store', ['competitor' => $competitor->id, 'nomination' => $competitor->getNomination->id])}}" method="POST" id="frm-upload-photo" enctype = "multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control btn-primary" name="photo" id="photo" title="Загрузить фото" required>
                            <p class="help-block text-danger">{!! $errors->first('photo') !!}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5 col-xs-6">
                            <button type="submit" class="btn btn-success">Загрузить</button>
                            <button type="button" class="btn btn-danger" id="frm-close">Закрыть</button>
                        </div>
                        
                    </div>
                </form>

                

               

            </div>
        </div>
    </div>
</div>