@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('fund.index') !!}
@endsection
@section('header')
        <h1>Призовые фонды</h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-funds') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'all_funds', 'cookie_url'=>'/cookie/funds/informer'])
  @endif
@endsection
@section('content')


        @foreach ($funds as $fund)
            <div class="row">
                <div class="col-lg-12">
                    <h2>Призовой фонд номинации "{{strip_tags($fund->name)}}" <small></small></h3>
                    <div class="col-sm-5 table-responsive">
                        <h3>Приз победителю <small>Оценки</small></h3>
                    <table class="table fund">
                        <tr>
                            <td class="prize trophy">
                                <i class="fa fa-trophy" aria-hidden="true"></i>
                            </td>
                            <td class="prize">
                                @if(!empty($fund->fundsScores->sum))
                                <small>{{$fund->fundsScores->sum}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></small>
                               @endif

                            </td>
                        </tr>
                        <tr>
                            <td class="position">
                                <small class="first">&#8544;</small>&nbsp;место
                            </td>
                            <td class="sum">
                                @if(!empty($fund->fundsScores->position_1))
                                {{$fund->fundsScores->position_1}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                               @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="position">
                                <small class="second">&#8545;</small>&nbsp;место
                            </td>
                            <td class="sum">
                                @if(!empty($fund->fundsScores->position_2))
                                {{$fund->fundsScores->position_2}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                               @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="position">
                                <small class="third">&#8546;</small>&nbsp;место
                            </td>
                            <td class="sum">
                                @if(!empty($fund->fundsScores->position_3))
                                {{$fund->fundsScores->position_3}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                               @endif
                            </td>
                        </tr>
                </table>
                </div>
                    <div class="col-sm-5 col-sm-offset-2 table-responsive">
                        <h3>Приз зрительских симпатий <small>Лайки</small></h3>
                    <table class="table fund">
                        <tr>

                            <td class="prize like">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </td>
                            <td class="prize">
                                @if(!empty($fund->fundsLikes->sum))
                                <small>{{$fund->fundsLikes->sum}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></small>
                               @endif
                            </td>
                        </tr>
                        <tr>

                            <td class="position">
                                <small class="first">&#8544;</small>&nbsp;место
                            </td>
                            <td class="sum">
                                @if(!empty($fund->fundsLikes->position_1))
                                {{$fund->fundsLikes->position_1}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                               @endif

                            </td>
                        </tr>
                        <tr>

                            <td class="position">
                                <small class="second">&#8545;</small>&nbsp;место
                            </td>
                            <td class="sum">
                                @if(!empty($fund->fundsLikes->position_2))
                                {{$fund->fundsLikes->position_2}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                               @endif
                            </td>
                        </tr>
                        <tr>

                            <td class="position">
                                <small class="third">&#8546;</small>&nbsp;место
                            </td>
                            <td class="sum">
                                @if(!empty($fund->fundsLikes->position_3))
                                {{$fund->fundsLikes->position_3}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                               @endif
                            </td>
                        </tr>

                    </table>
                </div>
                </div>
            </div>
        @endforeach

    <div class="row">
        <div class="col-lg-12 text-center">
            {{ $funds->links() }}
        </div>
    </div>


@endsection
