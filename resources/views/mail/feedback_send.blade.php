@extends('layouts.mail')

@section('content')


<tr>
    <td>
        <p>
            Сообщение формы обратной связи
        </p>
    </td>
</tr>
<tr>
    <td>
        <table>
            <tr>
                <td>
                    <p>
                        Отправитель
                    </p>
                </td>
                <td>
                    <p>
                        {{$name}}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        e-mail
                    </p>
                </td>
                <td>
                    <p>
                        <a href="mailto:{{$email}}">{{$email}}</a>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        Сообщение
                    </p>
                </td>
                <td>

                    <p>{{$messmail}}</p>


                </td>
            </tr>
        </table>
    </td>
</tr>
@endsection



