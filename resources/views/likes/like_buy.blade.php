@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('like.buy', $user->id) !!}
@endsection
@section('header')
      <h1>Купить лайки! <small></small></h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-likebuy') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'like_buy', 'cookie_url'=>'/cookie/likebuy/informer'])
  @endif
@endsection
@section('content')




            <div class="row">
              <div class="col-lg-12">
                <h3>Покупка лайков</h3>
              </div>
                <div class="col-lg-8">
                    <form action="{{ route('like.add')}}" class="form-horizontal" method="POST" role="form">
                        {{ csrf_field() }}

                             <div class="radio col-sm-offset-2 col-sm-10">
                                  <label><input type="radio" name="like_count" value="3"><i class="fa fa-heart color-red" aria-hidden="true"></i> 3 <span class="help-block">На тестовый период предоставляются бесплатно, не более одной покупки в сутки</span></label>
                                </div>
                                <div class="radio col-sm-offset-2 col-sm-10">
                                     <label><input type="radio" name="like_count" value="2"><i class="fa fa-heart color-red" aria-hidden="true"></i> 2 <span class="help-block">На тестовый период предоставляются бесплатно, не более одной покупки в сутки</span></label>
                                   </div>


                            <br><br>



                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button class="btn btn-success" type="submit">
                                    Купить!
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


@endsection
