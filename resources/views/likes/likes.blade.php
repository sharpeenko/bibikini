@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('like.likes', $photo->id) !!}
@endsection
@section('header')
        <h1>Кто лайкнул фото <small></small></h1>
@endsection
@section('content')
  <div class="row">
             @foreach ($likes as $like)
              <div class="col-md-2 col-xs-3">
                  <div class="thumbnail user liked">
                      <div class="liked">
                          <a href="{{route('users.show', [$like->user_id])}}"><img class="img-responsive img-rounded" src="{{$like->user->avatar}}" alt="..."></a>
                      </div>
                  </div>
                   <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;{{ date('d.m.Y', strtotime($like->created_at)) }}</span></p>
              </div>
             @endforeach
         </div>

            <div class="row">
                <div class="col-lg-12 text-center">
                   {{ $likes->links() }}
                </div>
            </div>


@endsection
