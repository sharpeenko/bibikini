@extends('layouts.bibikini')
@section('content')

    <div class="row hidden-xs">
        <div class="col-lg-12">
                <ol class="breadcrumb">
                        <li><a href="http://fitness.com.dev">Главная</a></li>
                        <li class="active">Номинации</li>
                </ol>

        </div>
    </div>
    @include('widget.blue_bar')

    <div class="row">
        <div class="col-lg-12">
            <h3>Призовой фонд номинации "Качихи" <small>Оценивается по количеству лайков.</small></h3>
            <div class="col-sm-5 table-responsive">
                <h3>По судейским оценкам <small>Оценивается по количеству лайков.</small></h3>
            <table class="table fund">
                <tr>
                    <td class="prize trophy">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                    </td>
                    <td class="prize">
                        <small>30 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></small>
                    </td>
                </tr>
                <tr>
                    <td class="position">
                        <small class="first">&#8544;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        15 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
                <tr>
                    <td class="position">
                        <small class="second">&#8545;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        10 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
                <tr>
                    <td class="position">
                        <small class="third">&#8546;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        5 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
        </table>
        </div>
            <div class="col-sm-5 col-sm-offset-2 table-responsive">
                <h3>По лайкам <small>Оценивается по количеству лайков.</small></h3>
            <table class="table fund">
                <tr>

                    <td class="prize like">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                    </td>
                    <td class="prize">
                        <small>15 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></small>
                    </td>
                </tr>
                <tr>

                    <td class="position">
                        <small class="first">&#8544;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        8 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
                <tr>

                    <td class="position">
                        <small class="second">&#8545;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        5 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
                <tr>

                    <td class="position">
                        <small class="third">&#8546;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        2 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>

            </table>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>Призовой фонд номинации "Даешь прогресс" <small>Оценивается по количеству лайков.</small></h3>
            <div class="col-sm-5 table-responsive">
                <h3>По судейским оценкам <small>Оценивается по количеству лайков.</small></h3>
            <table class="table fund">
                <tr>
                    <td class="prize trophy">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                    </td>
                    <td class="prize">
                        <small>30 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></small>
                    </td>
                </tr>
                <tr>
                    <td class="position">
                        <small class="first">&#8544;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        15 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
                <tr>
                    <td class="position">
                        <small class="second">&#8545;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        10 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
                <tr>
                    <td class="position">
                        <small class="third">&#8546;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        5 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
        </table>
        </div>
            <div class="col-sm-5 col-sm-offset-2 table-responsive">
                <h3>По лайкам <small>Оценивается по количеству лайков.</small></h3>
            <table class="table fund">
                <tr>

                    <td class="prize like">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                    </td>
                    <td class="prize">
                        <small>15 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></small>
                    </td>
                </tr>
                <tr>

                    <td class="position">
                        <small class="first">&#8544;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        8 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
                <tr>

                    <td class="position">
                        <small class="second">&#8545;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        5 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>
                <tr>

                    <td class="position">
                        <small class="third">&#8546;</small>&nbsp;место
                    </td>
                    <td class="sum">
                        2 000&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                    </td>
                </tr>

            </table>
        </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>Рейтинг фото <small>Оценивается по количеству лайков.</small></h3>
            <div class="table-responsive">
                <table class="table scores">
                    <tr>
                        <td class="photo">
                            <div class="thumbnail">
                                <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                            </div>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;1256</small>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-bar-chart color-green-bibikini" aria-hidden="true"></i>&nbsp;1</small>
                        </td>
                    </tr>
                    <tr>
                        <td class="photo">
                            <div class="thumbnail">
                                <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                            </div>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;3478</small>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-bar-chart color-green-bibikini" aria-hidden="true"></i>&nbsp;2</small>
                        </td>
                    </tr>
                    <tr>
                        <td class="photo">
                            <div class="thumbnail">
                                <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                            </div>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;45</small>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-bar-chart color-green-bibikini" aria-hidden="true"></i>&nbsp;3</small>
                        </td>
                    </tr>
                    <tr>
                        <td class="photo">
                            <div class="thumbnail">
                                <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                            </div>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;800</small>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-bar-chart color-green-bibikini" aria-hidden="true"></i>&nbsp;4</small>
                        </td>
                    </tr>

                </table>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>Рейтинг судейских оценок <small>Оценивается по средней оценке.</small></h3>
            <div class="table-responsive">
                <table class="table scores">
                    <tr>
                        <td class="photo">
                            <div class="thumbnail">
                                <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                            </div>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-graduation-cap color-black" aria-hidden="true"></i>&nbsp;7.6</small>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-bar-chart color-green" aria-hidden="true"></i>&nbsp;1</small>
                        </td>
                    </tr>
                    <tr>
                        <td class="photo">
                            <div class="thumbnail">
                                <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                            </div>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-graduation-cap color-black" aria-hidden="true"></i>&nbsp;7.6</small>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-bar-chart color-green" aria-hidden="true"></i>&nbsp;2</small>
                        </td>
                    </tr>
                    <tr>
                        <td class="photo">
                            <div class="thumbnail">
                                <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                            </div>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-graduation-cap color-black" aria-hidden="true"></i>&nbsp;7.6</small>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-bar-chart color-green" aria-hidden="true"></i>&nbsp;3</small>
                        </td>
                    </tr>
                    <tr>
                        <td class="photo">
                            <div class="thumbnail">
                                <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                            </div>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-graduation-cap color-black" aria-hidden="true"></i>&nbsp;7.6</small>
                        </td>
                        <td class="score">
                                <small class="score"><i class="fa fa-bar-chart color-green" aria-hidden="true"></i>&nbsp;4</small>
                        </td>
                    </tr>

                </table>
            </div>
        </div>


    </div>


    <div class='row'>
     <div class="col-lg-12">
      <h3>Оценки <small>Судьи должны оценивать участников! При отсутствии оценок обращайтесь к администрации.</small></h3>

     <div class="table-responsive">
         <table class="table scores">
             <tr>
                 <td class="photo">
                  <div class="thumbnail">
                      <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                  </div>
                 </td>
                 <td class="revize">
                   <small><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;2</small>
                 </td>
                 <td class="score">

                   <small class="score-actual"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;7.6</small>

                 </td>
                 <td class="score">

                   <small>12.05.17</small>

                 </td>
                 <td class="score">

                   <small><i class="fa fa-camera" aria-hidden="true"></i></small>

                 </td>
                 <td class="score">
                  <button type="button" class="btn btn-xs btn-info" data-toggle="collapse" data-target="#editdata_1">
                      История
                  </button>
                 </td>
                 <td class="score middle" rowspan="5">
                  <span class="badge badge-info"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;7.6</span>
                  <br>
                  <span class="badge badge-warning"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;22</span>
                 </td>

             </tr>
             <tr id="editdata_1" class="collapse">
                 <td>
                  1111111
                 </td>
             </tr>
             <tr>
                 <td class="photo">
                  <div class="thumbnail">
                      <a href=""><img src="/assets/img/test/bf2.jpg" /></a>
                  </div>
                 </td>
                 <td class="revize no">
                   <small><i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;0</small>
                 </td>
                 <td class="score no">
                   <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;0</small>
                 </td>
                 <td class="score">

                   <small></small>

                 </td>
                 <td class="score">

                   <small><i class="fa fa-camera" aria-hidden="true"></i></small>

                 </td>
                 <td class="score">
                  <button type="button" class="btn btn-xs btn-info" data-toggle="collapse" data-target="#editdata_2">
                      История
                  </button>
                 </td>

             </tr>
             <tr id="editdata_2" class="collapse">
                 <td>
                  1111111
                 </td>
             </tr>
             <tr>
                 <td class="photo">
                  <div class="thumbnail">
                      <a href=""><img src="/assets/img/test/bf1.jpg" /></a>
                  </div>
                 </td>
                 <td class="revize">
                   <small><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;2</small>
                 </td>
                 <td class="score">
                   <small class="score-actual"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;7.6</small>
                 </td>
                 <td class="score">

                   <small>12.05.17</small>

                 </td>
                 <td class="score">

                   <small><i class="fa fa-camera" aria-hidden="true"></i></small>

                 </td>
                 <td class="score">
                  <button type="button" class="btn btn-xs btn-info" data-toggle="collapse" data-target="#editdata_3">
                      История
                  </button>
                 </td>
             </tr>
             <tr id="editdata_3" class="collapse">

                 <td class="td-collapse"></td>
                 <td class="td-collapse"></td>
                 <td class="score td-collapse">
                  <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;7.6</small>&nbsp;&nbsp;&nbsp;&nbsp;<small>12.05.17</small>
                  <br>
                  <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;7.6</small>&nbsp;&nbsp;&nbsp;&nbsp;<small>12.05.17</small>
                  <br>
                  <small class="score-actual"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;7.6</small>&nbsp;&nbsp;&nbsp;&nbsp;<small>12.05.17</small>
                  <br>
                 </td>
             </tr>
             <tr>
                 <td class="photo">
                  <div class="thumbnail">
                      <a href=""><img src="/assets/img/test/bf2.jpg" /></a>
                  </div>
                 </td>
                 <td class="revize">
                   <small><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;2</small>
                 </td>
                 <td class="score no">
                   <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;0</small>
                 </td>
                 <td class="score">

                   <small></small>

                 </td>
                 <td class="score">

                   <small><i class="fa fa-camera" aria-hidden="true"></i></small>

                 </td>
                 <td class="score">
                  <button type="button" class="btn btn-xs btn-info" data-toggle="collapse" data-target="#editdata_4">
                      История
                  </button>
                 </td>
             </tr>
             <tr id="editdata_4" class="collapse">
                 <td class="score">
                  1111111
                 </td>
             </tr>
             <tr>
                 <td class="photo">
                  <div class="thumbnail">
                      <a href=""><img src="/assets/img/test/bf2.jpg" /></a>
                  </div>
                 </td>
                 <td class="revize no">
                   <small><i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;0</small>
                 </td>
                 <td class="score no">
                   <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;0</small>
                 </td>
                 <td class="score">

                   <small></small>

                 </td>
                 <td class="score no">

                   <small><i class="fa fa-camera" aria-hidden="true"></i></small>

                 </td>
                 <td class="score">
                  <button type="button" class="btn btn-xs btn-info" data-toggle="collapse" data-target="#editdata_5">
                      История
                  </button>
                 </td>
             </tr>
             <tr id="editdata_5" class="collapse">
                 <td>
                  1111111
                 </td>
             </tr>

         </table>
     </div>
    </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Кто лайкал это фото <small>Полный список</small></h1>
            </div>
            <p class="lead">Там можно будет узнать о достоинствах и недостатках специалистов по здоровому образу жизни. <br>
                Единый реестр фитнес-тренеров Москвы может появиться уже в сентябре будущего года.
            </p>

        </div>
    </div>
    <div class="row">
     <div class="col-md-2 col-xs-3">
         <div class="thumbnail user liked">
             <div class="liked">
                 <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
             </div>
         </div>
          <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;18.09.17</span></p>
     </div>
     <div class="col-md-2 col-xs-3">
         <div class="thumbnail user liked">
             <div class="liked">
                 <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
             </div>
         </div>
          <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;18.09.17</span></p>
     </div>
     <div class="col-md-2 col-xs-3">
         <div class="thumbnail user liked">
             <div class="liked">
                 <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
             </div>
         </div>
          <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;18.09.17</span></p>
     </div>
     <div class="col-md-2 col-xs-3">
         <div class="thumbnail user liked">
             <div class="liked">
                 <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
             </div>
         </div>
          <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;18.09.17</span></p>
     </div>
     <div class="col-md-2 col-xs-3">
         <div class="thumbnail user liked">
             <div class="liked">
                 <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
             </div>
         </div>
          <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;18.09.17</span></p>
     </div>
     <div class="col-md-2 col-xs-3">
         <div class="thumbnail user liked">
             <div class="liked">
                 <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
             </div>
         </div>
          <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;18.09.17</span></p>
     </div>
     <div class="col-md-2 col-xs-3">
         <div class="thumbnail user liked">
             <div class="liked">
                 <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
             </div>
         </div>
          <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;18.09.17</span></p>
     </div>
     <div class="col-md-2 col-xs-3">
         <div class="thumbnail user liked">
             <div class="liked">
                 <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
             </div>
         </div>
          <p class="text-center"><span class="label label-info"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;18.09.17</span></p>
     </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <ul class="pagination">
                <li class="disabled"><a href="#">&laquo;</a></li>
                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>
    </div>

   <br><br><br>

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Всех фитнес-тренеров - в единый реестр <small>Занесут скоро</small></h1>
            </div>
            <p class="lead">Там можно будет узнать о достоинствах и недостатках специалистов по здоровому образу жизни. <br>
                Единый реестр фитнес-тренеров Москвы может появиться уже в сентябре будущего года.
            </p>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-sm-4 col-xs-4">
            <div class="thumbnail photo">
                <div class="pic">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                        <p class="text-right">
                        <a href="" class="color-white opacity-hide-less" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-white opacity-hide-less" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </div>
                </div>

            </div>
        </div>







    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <ul class="pagination">
                <li class="disabled"><a href="#">&laquo;</a></li>
                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Всех фитнес-тренеров - в единый реестр <small>Занесут скоро</small></h1>
            </div>
            <p class="lead">Там можно будет узнать о достоинствах и недостатках специалистов по здоровому образу жизни. <br>
                Единый реестр фитнес-тренеров Москвы может появиться уже в сентябре будущего года.
            </p>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-inline form-search" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Найти">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar closed">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name closed"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                    <p>
                        <a href="" tabindex="0" class="color-gold opacity-hide" data-toggle="popover" title="Заголовок панели" data-poload="/makeup/testajax"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide" data-poload="/makeup/popaj"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide" data-toggle="modal" data-target="#modalInformer"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/slider1.jpg" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="">Чемпионка</a></h4>
                    <p>
                        <a href="" class="color-gold opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar closed">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/slider2.jpg" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name closed"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                    <p>
                        <a href="" class="color-gold opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                    <p>
                        <a href="" class="color-gold opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                    <p>
                        <a href="" class="color-gold opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                    <p>
                        <a href="" class="color-gold opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                    <p>
                        <a href="" class="color-gold opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                    <p>
                        <a href="" class="color-gold opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar">
                    <a href=""><img class="img-responsive img-rounded" src="/assets/img/test/back2.png" alt="..."></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="">Stronger Самый самый лучший стронгер в мире</a></h4>
                    <p>
                        <a href="" class="color-gold opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>4</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>4.709</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>6.7</small></a>&nbsp;
                        <a href="" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>304</small></a>&nbsp;
                        <a href="" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>8</small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="#" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <ul class="pagination">
                <li class="disabled"><a href="#">&laquo;</a></li>
                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>И все номинации - в единый реестр <small>Уже заносят</small></h1>
            </div>
            <p class="lead">Там можно будет узнать о достоинствах и недостатках специалистов по здоровому образу жизни. <br>
                Единый реестр фитнес-тренеров Москвы может появиться уже в сентябре будущего года.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail nomination">
                <div class="header woman">
                    <h3>Наши <br>бикиняши</h3>
                    <img class="nomination_1" src="/assets/img/test/nom-bikini1.png" >
                </div>
                <div class="description">
                    <div class="block sex woman">
                        <i class="fa fa-female" aria-hidden="true"></i>
                        <small>500</small>
                    </div>
                    <div class="block prize">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                        <small>102 345</small>
                    </div>
                    <div class="block like">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        <small>1020</small>
                    </div>
                </div>
                <div class="calendar">
                    <div class="block">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <small>28.12.2016</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <small>28.04.2017</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                        <small>48</small>
                    </div>
                </div>
                <div class="caption">
                    <ul class="list-unstyled">
                        <li>
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp; профессиональное судейство
                        </li>
                        <li>
                            <i class="fa fa-money" aria-hidden="true" style="color: #0e6f5c;"></i>&nbsp; оплата без обмана
                        </li>
                        <li>
                            <i class="fa fa-line-chart" aria-hidden="true"></i>&nbsp; растет призовой фонд
                        </li>
                        <li>
                            <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp; дополнительные фото
                        </li>
                        <li>
                            <i class="fa fa-heart" aria-hidden="true" style="color: red;"></i>&nbsp; легко!
                        </li>
                    </ul>
                    <p class="text-right">
                        <a href="#" class="btn btn-warning" role="button">Участвовать</a>
                        <a href="#" class="btn btn-info" role="button">Подробнее</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail nomination">
                <div class="header woman">
                    <h3>Просто <br>спортсменки</h3>
                    <img class="nomination_2" src="/assets/img/test/nom-bikini2.png" >
                </div>
                <div class="description">
                    <div class="block sex woman">
                        <i class="fa fa-female" aria-hidden="true"></i>
                        <small>500</small>
                    </div>
                    <div class="block prize">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                        <small>102 345</small>
                    </div>
                    <div class="block like">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        <small>1020</small>
                    </div>
                </div>
                <div class="calendar">
                    <div class="block">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <small>28.12.2016</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <small>28.04.2017</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                        <small>48</small>
                    </div>
                </div>
                <div class="caption">
                    <ul class="list-unstyled">
                        <li>
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp; профессиональное судейство
                        </li>
                        <li>
                            <i class="fa fa-money" aria-hidden="true" style="color: #0e6f5c;"></i>&nbsp; оплата без обмана
                        </li>
                        <li>
                            <i class="fa fa-line-chart" aria-hidden="true"></i>&nbsp; растет призовой фонд
                        </li>
                        <li>
                            <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp; дополнительные фото
                        </li>
                        <li>
                            <i class="fa fa-heart" aria-hidden="true" style="color: red;"></i>&nbsp; легко!
                        </li>
                    </ul>
                    <p class="text-right">
                        <a href="#" class="btn btn-warning" role="button">Участвовать</a>
                        <a href="#" class="btn btn-info" role="button">Подробнее</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail nomination">
                <div class="header man">
                    <h3>Настоящие<br> мужики</h3>
                    <img class="nomination_2" src="/assets/img/test/nom-bikini3.png" >
                </div>
                <div class="description">
                    <div class="block sex man">
                        <i class="fa fa-male" aria-hidden="true"></i>
                        <small>500</small>
                    </div>
                    <div class="block prize">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                        <small>102 345</small>
                    </div>
                    <div class="block like">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        <small>1020</small>
                    </div>
                </div>
                <div class="calendar">
                    <div class="block">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <small>28.12.2016</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <small>28.04.2017</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                        <small>48</small>
                    </div>
                </div>
                <div class="caption">
                    <ul class="list-unstyled">
                        <li>
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp; профессиональное судейство
                        </li>
                        <li>
                            <i class="fa fa-money" aria-hidden="true" style="color: #0e6f5c;"></i>&nbsp; оплата без обмана
                        </li>
                        <li>
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp; профессиональное судейство
                        </li>
                        <li>
                            <i class="fa fa-money" aria-hidden="true" style="color: #0e6f5c;"></i>&nbsp; оплата без обмана
                        </li>
                        <li>
                            <i class="fa fa-money" aria-hidden="true" style="color: #0e6f5c;"></i>&nbsp; оплата без обмана
                        </li>
                    </ul>
                    <p class="text-right">
                        <a href="#" class="btn btn-warning" role="button">Участвовать</a>
                        <a href="#" class="btn btn-info" role="button">Подробнее</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail nomination">
                <div class="header unisex">
                    <h3>Даешь<br> прогресс!</h3>
                    <img class="nomination_4" src="/assets/img/test/nom-bikini4.png" >
                </div>
                <div class="description">
                    <div class="block sex unisex">
                        <i class="fa fa-male" aria-hidden="true"></i>
                        <i class="fa fa-female" aria-hidden="true"></i>
                        <small>500</small>
                    </div>
                    <div class="block prize">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                        <small>102 345</small>
                    </div>
                    <div class="block like">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        <small>1020</small>
                    </div>
                </div>
                <div class="calendar">
                    <div class="block">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <small>28.12.2016</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <small>28.04.2017</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                        <small>48</small>
                    </div>
                </div>
                <div class="caption">
                    <ul class="list-unstyled">
                        <li>
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp; профессиональное судейство
                        </li>
                        <li>
                            <i class="fa fa-money" aria-hidden="true" style="color: #0e6f5c;"></i>&nbsp; оплата без обмана
                        </li>
                        <li>
                            <i class="fa fa-line-chart" aria-hidden="true"></i>&nbsp; растет призовой фонд
                        </li>
                        <li>
                            <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp; дополнительные фото
                        </li>
                        <li>
                            <i class="fa fa-heart" aria-hidden="true" style="color: red;"></i>&nbsp; легко!
                        </li>
                    </ul>
                    <p class="text-right">
                        <a href="#" class="btn btn-warning" role="button">Участвовать</a>
                        <a href="#" class="btn btn-info" role="button">Подробнее</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail nomination">
                <div class="header woman closed">
                    <h3>Просто <br>спортсменки</h3>
                    <img class="nomination_2" src="/assets/img/test/nom-bikini2.png" >
                </div>
                <div class="description">
                    <div class="block sex woman">
                        <i class="fa fa-female" aria-hidden="true"></i>
                        <small>500</small>
                    </div>
                    <div class="block prize">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                        <small>102 345</small>
                    </div>
                    <div class="block like">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        <small>1020</small>
                    </div>
                </div>
                <div class="calendar">
                    <div class="block">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <small>28.12.2016</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <small>28.04.2017</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-bell-slash-o" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="caption">
                    <ul class="list-unstyled">
                        <li>
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp; профессиональное судейство
                        </li>
                        <li>
                            <i class="fa fa-money" aria-hidden="true" style="color: #0e6f5c;"></i>&nbsp; оплата без обмана
                        </li>
                        <li>
                            <i class="fa fa-line-chart" aria-hidden="true"></i>&nbsp; растет призовой фонд
                        </li>
                        <li>
                            <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp; дополнительные фото
                        </li>
                        <li>
                            <i class="fa fa-heart" aria-hidden="true" style="color: red;"></i>&nbsp; легко!
                        </li>
                    </ul>
                    <p class="text-right">
                        <a href="#" class="btn btn-warning disabled" role="button">Участвовать</a>
                        <a href="#" class="btn btn-info" role="button">Подробнее</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <ul class="pagination">
                <li class="disabled"><a href="#">&laquo;</a></li>
                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>
    </div>

@endsection
