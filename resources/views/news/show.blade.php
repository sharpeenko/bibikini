@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('news.show', $newone->id) !!}
@endsection
@section('header')
      <h1>{{$newone->header}} <small></small></h1>
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-12">

            {!!$newone->content!!}
            <p class="text-muted">{{$newone->author}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d.m.Y', strtotime($newone->publish_at)) }}</p>
            @if ($newone->source)
            <p class="pull-left w50p"><em><a href="{{$newone->source}}">{{$newone->source}}</a></em></p>
            @else
            <p class="pull-left w50p"></p>
            @endif
            <div class="pull-right w50p text-right temp-like">
              @include('widget.social')
            </div>
            <div class="clearfix"></div>

            <h3>Читайте также:</h3>
            @foreach ($news as $newr)
            <p><a href="{{route('news.show', [$newr->id])}}">{{$newr->header}}</a></p>
            @endforeach
            <br><br>
            <p class="text-right"><a href="{{url('/news')}}">Все новости</a></p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            @include('comment.comments_block', ['url'=>'comment.store.news', 'instance' => $newone->id, 'comments' => $newone->comments])
       </div>
    </div>


@endsection
