@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('news.index') !!}
@endsection
@section('header')
      <h1>Новости сайта <small></small></h1>
@endsection
@section('content')



            <div class="row">
              <div class="col-lg-12">
            @foreach ($news as $newone)
            <div class="media newslist-content">
                <h3 class="media-heading"><a href="{{route('news.show', [$newone->id])}}">{{$newone->header}}</a></h3>
                <a class="pull-left" href="{{route('news.show', [$newone->id])}}">
                    <img class="media-object img-thumbnail" src="{{$newone->pic}}" alt="{{$newone->header}}">
                </a>
                <div class="media-body">
                    <p class="text-muted pull-left">{{$newone->author}}</p>
                    <p class="text-muted pull-right">{{ date('d.m.Y', strtotime($newone->publish_at)) }}</p>
                    <div class="clearfix"></div>
                    <a href="{{route('news.show', [$newone->id])}}">
                        {!!$newone->introtext!!}
                    </a>
                </div>
                <div class="clearfix"></div>
                <div class="divider"></div>
                <p class="text-right">
                    <a href="{{route('news.show', [$newone->id])}}" class="btn btn-md btn-primary">Подробнее</a>
                </p>
            </div>
            @endforeach
          </div>
          </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            {{ $news->links() }}
        </div>
    </div>

@endsection
