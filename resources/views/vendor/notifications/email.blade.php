@extends('layouts.mail')

@section('content')
@if(isset($notificationStatus))

@if($notificationStatus == 'InfoModerator') {{-- Если жалоба на комментарий --}}
<tr>



    <td>

        @if($initialComment->commentable_type == 'App\News')
            <p><a href="{{route('news.show', [$initialComment->commentable_id])}}#comment_{{$initialComment->id}}">{{$initialComment->content}}</a></p>
        @endif
        @if($initialComment->commentable_type == 'App\Competitor')
            <p><a href="{{route('competitor.show', [$initialComment->commentable_id])}}#comment_{{$initialComment->id}}">{{$initialComment->content}}</a></p>
        @endif
        @if($initialComment->commentable_type == 'App\Photo')
            <p><a href="{{route('photo.show', [$initialComment->commentable_id])}}#comment_{{$initialComment->id}}">{{$initialComment->content}}</a></p>
        @endif
    </td>
</tr>
<tr>

    <td>
        <p>Инициатор сообщения: <a href="{{route('users.show', $initialUser->id)}}">{{$initialUser->name}}</a></p>
    </td>
</tr>
@endif
@if($notificationStatus == 'AddCommentCompetitor') {{-- Если добавлен комментарий участнику --}}
<tr>



    <td>

        <p>Пользователь <a href="{{route('users.show', $initialUser->id)}}">{{$initialUser->name}}</a> добавил комментарий Вашему участнику.</p>
        <br><br><br>


            <p><a href="{{route('competitor.show', [$initialComment->commentable_id])}}#comment_{{$initialComment->id}}">{{$initialComment->content}}</a></p>

    </td>
</tr>

@endif
@if($notificationStatus == 'AddCommentPhoto') {{-- Если добавлен комментарий фото --}}
<tr>



    <td>

        <p>Пользователь <a href="{{route('users.show', $initialUser->id)}}">{{$initialUser->name}}</a> добавил комментарий Вашему фото.</p>
        <br><br><br>


            <p><a href="{{route('photo.show', [$initialComment->commentable_id])}}#comment_{{$initialComment->id}}">{{$initialComment->content}}</a></p>

    </td>
</tr>

@endif
@if($notificationStatus == 'ChangeStatusPhoto') {{-- Если в админке изменен статус фото --}}
<tr>



    <td>
        @if($initialPhoto->is_addition == 0)
        <p>Ваше <a href="{{route('photo.show', $initialPhoto->id)}}">фото</a> было
         @if($initialPhoto->photo_status_id == 2)
            одобрено
         @endif
         @if($initialPhoto->photo_status_id == 3)
            отклонено
         @endif
         модератором.</p>
        <br><br><br>
      @elseif ($initialPhoto->is_addition == 1)
        @if($initialPhoto->photo_status_id == 4)
          {{--TODO: Вот здесь проверка на привелегию бесплатной загрузки дополнительных фото, но передать это нужно бы извне...--}}
          @if($initialPhoto->getCompetitor->is_privelege_addition_photo == 1) {{--Если привелегия--}}
            <p>Ваше фото, загруженное, как дополнительное, было одобрено модератором.</p>
            <p>Чтобы подтвердить публикацию фото, перейдите по <a href="{{route('photo.confirm.addition', [$initialPhoto->competitor_id, $initialPhoto->id])}}">ссылке </a></p>
          @else {{--Если привелегии нет--}}
            <p>Ваше фото, загруженное, как дополнительное, было одобрено модератором.</p>
            <p>Чтобы подтвердить публикацию фото, перейдите по
              <a href="{{route('payment.yandexmoney.additionalphoto', [$initialPhoto->getCompetitor->nomination_id])}}">ссылке </a></p>
              <p>В комментарии к переводу обязательно укажите идентификатор фото <span class="text-info"><strong>{{$initialPhoto->id}}</strong></span></p>
          @endif
        @endif
        @if($initialPhoto->photo_status_id == 3)
          <p>Ваше фото, загруженное, как дополнительное, было отклонено модератором.</p>

        @endif

      @endif


    </td>
</tr>

@endif
@if($notificationStatus == 'UserRegistered') {{-- Подтверждение регистрации --}}
<tr>



    <td>
        <p>Благодарим за регистрацию!</p>
        <p>Для подтверждения учетной записи пройдите по ссылке ниже.</p>
        <p><a href="{{route('confirm', [$initialConfirm->id, $initialConfirm->activation_code])}}">Ссылка для автивации учетной записи</a></p>
        <br><br><br>




    </td>
</tr>

@endif

@if($notificationStatus == 'AddLikePhoto') {{-- Если добавлен лайк к фото --}}
<tr>



    <td>

        <p>Пользователь <a href="{{route('users.show', $initialUser->id)}}">{{$initialUser->name}}</a> добавил лайк Вашему фото.</p>
        <br><br><br>


            <p><a href="{{route('photo.show', [$initialLike->likeable_id])}}">Ссылка</a></p>

    </td>
</tr>

@endif

@if($notificationStatus == 'AddRefereeNomination') {{-- Если в админке судья добавлен в номинацию --}}
<tr>



    <td>

        <p>Вы добавлены в <a href="{{route('nomination.show', $initialReferee->nomination_id)}}">номинацию</a> в качестве судьи.</p>
        <br><br><br>




    </td>
</tr>

@endif

@if($notificationStatus == 'AddCompetitorNomination') {{-- Если участник добавлен в номинацию --}}
<tr>



    <td>

        <p>Вы добавлены в <a href="{{route('nomination.show', $initialCompetitor->nomination_id)}}">номинацию</a> участником.</p>
        <br><br><br>




    </td>
</tr>

@endif

@if($notificationStatus == 'AddScore') {{-- Если поставили оценку --}}
<tr>



    <td>

        <p><a href="{{route('competitor.show', $initialScore->competitor_id)}}">Участнику</a> поставлена оценка {{$initialScore->score}}.</p>
        <br><br><br>




    </td>
</tr>





@endif

@else {{--В случае отправки оповещения при сбросе пароля--}}
  <tr>



      <td>
          <p>Чтобы сбросить пароль, пройдите по ссылке ниже.</p>
          <p><a href="{{ $actionUrl }}" target="_blank">
              {{ $actionUrl }}
          </a></p>
          <br><br><br>




      </td>

  <tr>



@endif

@endsection
