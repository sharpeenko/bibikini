@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('payment.yandexmoney.nomination', $nomination->id) !!}
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <h3>Взнос за участие в номинации {{$nomination->name}}</h3>
        {!! $nomination->payment_form !!}
    </div>
  </div>


@endsection
