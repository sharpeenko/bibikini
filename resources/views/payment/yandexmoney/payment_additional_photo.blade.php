@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('payment.yandexmoney.additionalphoto', $nomination->id) !!}
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <h3>Оплата загрузки дополнительного фото в номинации {{$nomination->name}}</h3>
        {!! $nomination->payment_form_additional_photo !!}
    </div>
  </div>


@endsection
