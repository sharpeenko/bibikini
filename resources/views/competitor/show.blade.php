@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('competitor.show', $competitor->id) !!}
@endsection
@section('header')
        <h1>{{$competitor->getUser->name}}
          @if($competitor->is_champion == 1)
            <small class="text-warning">Призёр номинации "{{strip_tags($competitor->getNomination->name)}}", занял {{$competitor->prize_place}} место</small>
          @else
          <small>Участник номинации "{{strip_tags($competitor->getNomination->name)}}"</small>
         @endif
        </h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-competitor') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'competitor_profile', 'cookie_url'=>'/cookie/competitor/informer'])
  @endif
@endsection
@section('content')
  <div class="row">
        <div class="col-sm-6">
          @if($competitor->avatar=='default')
             @php
             $competitor->avatar= asset('assets/img/default/avatar.png');
             @endphp
          @endif
            <a href="{{asset($competitor->avatar)}}" data-toggle="lightbox" data-title="{{$competitor->getUser->name}}" data-footer="{{$competitor->getUser->name}}">
                <img src="{{asset($competitor->avatar)}}" class="img-responsive img-thumbnail pull-left img-content img-left" alt="{{$competitor->getUser->name}}">
            </a>
        </div>
        <div class="col-sm-6">
            <h3>Информация</h3>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td>
                            Имя
                        </td>
                        <td>
                            {{$competitor->getUser->name}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Хэштег участника
                        </td>
                        <td>
                            {{$competitor->hashtag}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Хэштег пользователя
                        </td>
                        <td>
                            {{$competitor->getUser->hashtag}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Пол
                        </td>
                        <td>
                            {{$competitor->getUser->sex}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Пользователь
                        </td>
                        <td>
                            <a href="{{route('users.show', [$competitor->getUser->id])}}">{{$competitor->getUser->name}}</a>
                        </td>
                    </tr>
                </table>
            </div>
            <h3>О себе</h3>
            <p>{{$competitor->about}}</p>
        </div>
    </div>
    @if($competitor->is_champion == 1)
      <div class="row">
        <div class="col-lg-12">
          <h3>Чемпион <small>Подтверждения получения приза и т п</small></h3>
          {!! $competitor->champion_description !!}
        </div>
        </div>
    @endif


    <div class="row">
      <div class="col-lg-12">
        <a name="photos"></a>
      <h3>Фотогалерея <small></small></h3>
    </div>
    @foreach ($competitor->getPhoto as $photo)
    <div class="col-md-2 col-xs-3">
        <div class="thumbnail photo">
            <div class="pic mini">
                <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                <div class="pic-bar">

                    <p class="text-right">
                        <a href="" class="color-black opacity-hide-less" ><i class="fa fa-bar-chart" aria-hidden="true"></i></a>&nbsp;
                        <a href="" class="color-black opacity-hide-less" ><small>
                            @if(!empty($photo->ratingLike->position))
                            {{$photo->ratingLike->position}}
                           @endif
                        </small></a>&nbsp;
                     <a href="{{route('like.create', [$photo->id])}}" class="color-red opacity-hide-less" ><i class="fa fa-heart" aria-hidden="true"></i></a>&nbsp;
                     <a href="{{route('like.likes', [$photo->id])}}" class="color-red opacity-hide-less" ><small>{{$photo->likes->count()}}</small></a>&nbsp;
                    <a href="{{route('photo.show', [$photo->id])}}" class="color-black opacity-hide-less"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$photo->comments->count()}}</small></a>&nbsp;
                </p>
                </div>
            </div>

        </div>
    </div>
    @endforeach
      </div>


    <div class='row'>
     <div class="col-lg-12">
      <a name="scores"></a>
      <h3>Оценки <small>Судьи должны оценивать участников! При отсутствии оценок обращайтесь к администрации.</small></h3>

      @include('widget.table_score_agr')

    </div>
    </div>


    <div class="row">

    <div class="col-lg-12">
    @include('comment.comments_block', ['url'=>'comment.store.competitor', 'instance' => $competitor->id, 'comments' => $competitor->comments])
    </div>
    </div>
@endsection
