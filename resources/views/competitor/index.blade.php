@extends('layouts.bibikini')
@section('breadcrumbs')
@yield('breadcrumbs')
@endsection
@section('header')
    <h1>@yield('competitors_title')</h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-competitors') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'all_competitors', 'cookie_url'=>'/cookie/competitors/informer'])
  @endif
@endsection
@section('content')
  <div class="row">
      <div class="col-lg-12">
        <h3>@yield('subheader')</h3>
      </div>

        @foreach ($competitors as $competitor)
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
                <div class="avatar
                @if ($competitor->getNomination->status == 3)
                 closed
               @endif
                ">
                  @if($competitor->avatar=='default')
                     @php
                     $competitor->avatar= asset('assets/img/default/avatar.png');
                     @endphp
                  @endif
                    <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}"><img class="img-responsive img-rounded" src="{{asset($competitor->avatar)}}" alt="{{$competitor->name}}"></a>
                </div>

                <div class="caption">
                    <h4 class="text-name
                    @if ($competitor->getNomination->status == 3)
                     closed
                   @endif
                    "><a href="{{route('competitor.show', ['competitor' => $competitor->id])}}">{{$competitor->name}}</a></h4>
                    <p>
                      @if($competitor->is_champion == 1)
                        @if ($competitor->prize_place == 1)
                            <i class="fa fa-trophy color-gold" aria-hidden="true"></i>&nbsp;
                        @elseif ($competitor->prize_place == 2)
                            <i class="fa fa-trophy color-blue" aria-hidden="true"></i>&nbsp;
                        @else
                             <i class="fa fa-trophy color-green-bibikini" aria-hidden="true"></i>&nbsp;
                        @endif

                      @endif
                     <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>
                         @if(!empty($competitor->ratingScore->position))
                         {{$competitor->ratingScore->position}}
                        @endif
                    </small></a>&nbsp;
                     <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#scores" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>
                      @if(!empty(Nomination::avgScore($competitor->id)->first()->avgscore))
                      {{Nomination::avgScore($competitor->id)->first()->avgscore}}
                     @endif

                     </small></a>&nbsp;
                     <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#photos" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>
                      {{Nomination::countLikesCompetitor($competitor->id)}}
                     </small></a>&nbsp;
                     <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#photos" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>

                      {{$competitor->photos->count()}}

                     </small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="{{route('competitor.show', ['competitor' => $competitor->id])}}" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            {{ $competitors->links() }}
        </div>
    </div>


@endsection
