@extends('competitor.index')
@section('breadcrumbs')
{!! Breadcrumbs::render('competitor.index', $nomination->id) !!}
@endsection
@section('competitors_title')
Участники номинации "{{strip_tags($nomination->name)}}" <small>{{$nomination->type}}</small>
@endsection
@section('subheader')
Участники номинации
@endsection
@section('lead_introtext')
{{$nomination->introtext}} <br>
Перед тем, как участвовать, обязательно прочитайте <a href="ЗДЕСЬ ССЫЛКА НА ПРАВИЛА">правила</a>!
@endsection
@section('competitor_name')

@endsection
