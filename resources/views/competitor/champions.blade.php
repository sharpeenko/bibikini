@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('champion') !!}
@endsection
@section('header')
        <h1>Чемпионы</h1>
@endsection
@section('content')
  <div class="row">
    @foreach ($nominations as $nomination)
          <div class="col-lg-12">
              <h2><a href="{{route('nomination.show', [$nomination->id])}}">{!! $nomination->name !!}</a></h2>

              @php
              $champions = $nomination->ratingScores->slice(0, 3);
              $first_place = $champions[0];
              $second_place = $champions[1];
              $third_place = $champions[2];
              $champions = [$second_place, $first_place, $third_place]
              @endphp

            <div class="rating-block">
                @foreach ($champions  as $ratingScore)
                    <div class="block">
                      @if($loop->iteration == 1)
                        <h4 class="text-center text-primary">&#8545; место</h4>
                      @elseif($loop->iteration == 2)
                       <h4 class="text-center text-warning">&#8544; место</h4>
                      @elseif($loop->iteration == 3)
                        <h4 class="text-center text-success">&#8546; место</h4>
                     @endif
                        <div class="thumbnail">
                            <a href="{{route('competitor.show', [$ratingScore->competitor_id])}}"><img src="{{asset($ratingScore->competitor->avatar)}}" /></a>
                        </div>
                        <p class="prize text-center">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i><small><a href="{{route('competitor.show', [$ratingScore->competitor_id])}}#scores">{{$ratingScore->avscore}}</a></small>
                       </p>
                        <a href="{{route('rating.score', [$nomination->id])}}"
                          @if($loop->iteration == 1)
                            class="lenta prize second"
                          @elseif($loop->iteration == 2)
                            class="lenta prize first"
                          @elseif($loop->iteration == 3)
                            class="lenta prize third"
                         @endif
                          style="height: {{Nomination::ratingLentaHeight($ratingScore->avscore, 2, 'px')}};"></a>
                    </div>
                @endforeach
                </div>

          </div>
   @endforeach
  </div>
@endsection
