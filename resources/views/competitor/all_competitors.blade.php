@extends('competitor.index')
@section('breadcrumbs')
{!! Breadcrumbs::render('competitor.all') !!}
@endsection
@section('competitors_title')
@if(!isset($nomination_current))
Участники всех номинаций <small></small>
@else
Участники номинации "{{strip_tags($nomination_current->name)}}" <small>Результаты поиска</small>
@endif
@endsection
@section('subheader')
Все участники во всех номинациях
@endsection
@section('competitors_search')

<div class="col-lg-12">
    <form class="form-inline" role="search" method="POST" action="{{ route('competitor.search') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <select name="nomination" class="form-control">
                <option value="">Номинация</option>
                @foreach ($nominations as $nomination)
                <option value="{{$nomination->id}}">{{strip_tags($nomination->name)}}</option>
                @endforeach
            </select>

        </div>
        <div class="form-group">
            <select name="sex" class="form-control">
                <option value="мужской" >мужской</option>
                <option value="женский" >женский</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Найти</button>
    </form>
</div>


@endsection
