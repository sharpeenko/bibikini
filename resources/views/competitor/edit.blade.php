@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('competitor.edit', $competitor->id) !!}
@endsection
@section('header')
  <h1>{{$competitor->getUser->name}}
    @if($competitor->is_champion == 1)
      <small class="text-warning">Призёр номинации "{{strip_tags($competitor->getNomination->name)}}", занял {{$competitor->prize_place}} место</small>
    @else
    <small>Участник номинации "{{strip_tags($competitor->getNomination->name)}}"</small>
   @endif
  </h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-competitor-edit') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'competitor_edit', 'cookie_url'=>'/cookie/competitor/edit/informer'])
  @endif
@endsection
@section('content')
@include('modals.upload_photo')




    <div class="row">
        <div class="col-sm-6">
          @if($competitor->avatar=='default')
             @php
             $competitor->avatar= asset('assets/img/default/avatar.png');
             @endphp
          @endif
            <a href="{{asset($competitor->avatar)}}" data-toggle="lightbox" data-title="{{$competitor->getUser->name}}" data-footer="{{$competitor->getUser->name}}">
                <img src="{{asset($competitor->avatar)}}" class="img-responsive img-thumbnail pull-left img-content img-left" alt="{{$competitor->getUser->name}}">
            </a>
        </div>
        <div class="col-sm-6">
            <h3>Информация</h3>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td>
                            Имя
                        </td>
                        <td>
                            {{$competitor->getUser->name}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Хэштег участника
                        </td>
                        <td>
                            {{$competitor->hashtag}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Хэштег пользователя
                        </td>
                        <td>
                            {{$competitor->getUser->hashtag}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Пол
                        </td>
                        <td>
                            {{$competitor->getUser->sex}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Пользователь
                        </td>
                        <td>
                            <a href="{{route('users.show', [$competitor->getUser->id])}}">{{$competitor->getUser->name}}</a>
                        </td>
                    </tr>
                </table>
            </div>
            <h3>О себе</h3>
            <p>{{$competitor->about}}</p>

        </div>
    </div>
    @if($competitor->is_champion == 1)
      <br><br>
      <div class="row">
        <div class="col-lg-12">
          <h3>Чемпион <small>Подтверждения получения приза и т п</small></h3>
          {!! $competitor->champion_description !!}
        </div>
        </div>
    @endif
    <br><br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="form-control btn-primary" data-toggle="collapse" data-target="#editdata">
                Редактировать информацию, изменить аватар <i class="fa fa-chevron-down" aria-hidden="true"></i>
            </button>

            <div id="editdata" class="collapse">
                <form class="form-horizontal incollapse" role="form" method="POST" action="{{ url('competitor/'.$competitor->id) }}" enctype = "multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="about" class="col-sm-2 control-label">О себе</label>
                        <div class="col-sm-7 col-sm-offset-1">
                            <textarea class="form-control" rows="5" name="about">{{$competitor->about}}</textarea>
                            <p class="help-block text-danger">{!! $errors->first('about') !!}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="avatar" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control btn-primary" name="avatar" title="Загрузить аватар">
                            <p class="help-block text-danger">{!! $errors->first('avatar') !!}</p>
                            <p  class="text-danger"><small>Допускается загрузка изображений форматов png, jpg, jpeg, gif размером не более 1МБ.</small></p>
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-5 col-xs-6">
                          <a href="{{Request::url()}}" class="btn btn-danger">Отменить</a>
                      </div>
                        <div class="col-sm-5 col-xs-6">
                            <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <a name="photos"></a>
      <h3>Фотогалерея <small>Для загрузки фото нажмите на "плюс"</small></h3>
    </div>
    @foreach ($competitor->getPhoto as $photo)
     @if($photo->photo_status_id == 1)

            <div class="col-md-2 col-xs-4">
        <div class="thumbnail photo">
            <div class="pic mini download">
                <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                <div class="pic-bar">
                    <p class="text-center">
                        Проверка
                    </p>
                </div>
            </div>

        </div>
    </div>
  @elseif($photo->photo_status_id == 4)

           <div class="col-md-2 col-xs-4">
       <div class="thumbnail photo">
           <div class="pic mini download">
               <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
               <div class="pic-bar">
                   <p class="text-center">
                       Дополнительное
                   </p>
               </div>
           </div>

       </div>
   </div>
        @elseif($photo->photo_status_id == 2)
            <div class="col-md-2 col-xs-4">
                <div class="thumbnail photo">
                    <div class="pic mini">
                        <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                        <div class="pic-bar">

                            <p class="text-right">
                                <a href="" class="color-black opacity-hide-less" ><i class="fa fa-bar-chart" aria-hidden="true"></i></a>&nbsp;
                                <a href="" class="color-black opacity-hide-less" ><small>
                                    @if(!empty($photo->ratingLike->position))
                                    {{$photo->ratingLike->position}}
                                   @endif
                                </small></a>&nbsp;
                             <a href="{{route('like.create', [$photo->id])}}" class="color-red opacity-hide-less" ><i class="fa fa-heart" aria-hidden="true"></i></a>&nbsp;
                             <a href="{{route('like.likes', [$photo->id])}}" class="color-red opacity-hide-less" ><small>{{$photo->likes->count()}}</small></a>&nbsp;
                            <a href="{{route('photo.show', [$photo->id])}}" class="color-black opacity-hide-less"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$photo->comments->count()}}</small></a>&nbsp;
                        </p>
                        </div>
                    </div>

                </div>
            </div>

        @else
            <div class="col-md-2 col-xs-4">
        <div class="thumbnail photo">
            <div class="pic mini rejected">
                <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                <div class="pic-bar">
                    <p class="text-center">
                        Отклонена
                    </p>
                </div>
            </div>

        </div>
    </div>

    @endif
    @endforeach
    <a name="uploadPhoto"></a>
    <div class="col-md-2 col-xs-3">
        <div class="photo">
            <div class="pic mini">
                <a class="pic-add" href="" data-toggle="modal" data-target="#modalUploadPhoto"><i class="fa fa-plus-square-o fa-5x" aria-hidden="true"></i></a>

            </div>

        </div>
    </div>
      </div>

    <div class='row'>
     <div class="col-lg-12">
      <a name="scores"></a>
      <h3>Оценки <small>Судьи должны оценивать участников! При отсутствии оценок обращайтесь к администрации.</small></h3>

      @include('widget.table_score_agr')

    </div>
    </div>

    <div class="row">

    <div class="col-lg-12">
    @include('comment.comments_block', ['url'=>'comment.store.competitor', 'instance' => $competitor->id, 'comments' => $competitor->comments])
    </div>
    </div>

@endsection
