@extends('nomination.index')
@section('breadcrumbs')
{!! Breadcrumbs::render('nomination.index') !!}
@endsection
@section('nomination_title')
  @foreach ($contents as $content)
        @if ($content->key == 'all_nominations')
            {!! $content->header !!}
        @endif
  @endforeach
@endsection
