@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('nomination.description', $nomination->id) !!}
@endsection
@section('header')
      <h1>Правила номинации "{{strip_tags($nomination->name)}}" <small></small></h1>
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-12">

            {!!$nomination->description!!}
            <div class="pull-right w50p text-right temp-like">
              @include('widget.social')
            </div>
            <div class="clearfix"></div>
        </div>
    </div>


@endsection
