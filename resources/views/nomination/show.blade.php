@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('nomination.show', $nomination->id) !!}
@endsection
@section('header')
      <h1>{{strip_tags($nomination->name)}} <small>{{$nomination->type}}</small> {!! $nomination->introtext !!}</h1>
@endsection
@section('content')
@if (!Nomination::checkNominationCompetitor($nomination->id))
  @if ($nomination->status == 1)
    @include('modals.agreement')
  @endif
@endif
  <div class="row">
      <div class="col-lg-12">
        <h3>Сведения о номинации</h3>
      </div>
      <div class="col-md-8">

        <div class="col-md-6 col-sm-6 text-center">
            @if (Nomination::checkNominationCompetitor($nomination->id))
            <?php $is_competitor = Nomination::getCompetitorInfo($nomination->id)?>
            <div class="is-competitor">
                <h3>Вы - участник!</h3>
                <div class="thumbnail">
                    <a href="{{route('competitor.show', ['competitor' => $is_competitor->id])}}"><img src="{{$is_competitor->avatar}}" /></a>
                </div>
                <p class="prize text-center">

                    <a href="{{route('competitor.show', ['competitor' => $is_competitor->id])}}" class="color-black opacity-hide">
                      <i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;
                      @if(!empty($is_competitor->ratingScore->position))
                      <small>{{$is_competitor->ratingScore->position}}</small>
                    @endif
                    </a>&nbsp;
                    <a href="{{route('competitor.show', ['competitor' => $is_competitor->id])}}#scores" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>
                     @if(!empty(Nomination::avgScore($is_competitor->id)->first()->avgscore))
                     {{Nomination::avgScore($is_competitor->id)->first()->avgscore}}
                    @endif

                    </small></a>&nbsp;
                    <a href="{{route('competitor.show', ['competitor' => $is_competitor->id])}}#photos" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>
                     {{Nomination::countLikesCompetitor($is_competitor->id)}}
                    </small></a>&nbsp;
                    <a href="{{route('competitor.show', ['competitor' => $is_competitor->id])}}#photos" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>
                     @if($is_competitor->photos->count()>0)
                     {{$is_competitor->photos->count()}}
                    @endif
                    </small></a>&nbsp;
                </p>
               {{--В завершенной номинации загружать нельзя, в продленной зависит от того, прошел ли участник в следующий тур--}}
                @if ($nomination->status == 1)
                    <a href="{{route('competitor.show', [$is_competitor->id])}}#uploadPhoto" class="btn btn-info" role="button">
                    <i class="fa fa-camera" aria-hidden="true"></i>&nbsp;
                    Загрузить
                  </a>
              @endif

            </div>
            @else
              @if ($nomination->status == 1)
            <div class="jumbotron nommi sex woman">
                <img class="{{$nomination->css_img_nommi}}" src="{{asset($nomination->pic)}}" ><br>
                {{--Если заходит гость--}}
                @if(!Auth::user())
                  <a href="{{route('login')}}" class="btn btn-primary btn-lg" role="button"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;Вход на сайт</a>
                @else
                  {{--Здесь проверить платность/бесплатность номинации и привелегии пользователя--}}
                  @if(!$nomination->is_commercial) {{--Если бесплатная--}}
                  <a href="" data-toggle="modal" data-target="#modalAgreement" class="btn btn-warning btn-lg btn-nommi" role="button"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;Участвовать</a>
                  @else {{--Если платная--}}
                    @if(!Auth::user()->is_privelege) {{--Но пользователь не привелегированный--}}
                      <a href="{{route('payment.yandexmoney.nomination', [$nomination->id])}}" class="btn btn-warning btn-lg btn-nommi" role="button"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;Участвовать</a>
                    @else
                      <a href="" data-toggle="modal" data-target="#modalAgreement" class="btn btn-warning btn-lg btn-nommi" role="button"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;Участвовать</a>
                    @endif
                  @endif
               @endif


            </div>
            @endif
            @endif
        </div>
        <div class="col-md-6 col-sm-6 text-center">
            <div class="jumbotron nommi rules">
                <img class="{{$nomination->css_img_rules}}" src="{{asset($nomination->pic_rules)}}" ><br>
                <a href="{{route('nomination.description', [$nomination->id])}}" class="btn btn-danger btn-lg btn-nommi" role="button"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;Правила</a>
            </div>
        </div>
        <div class="col-md-12">
          @if ($nomination->status == 3)
            @php
            $champions = $nomination->ratingScores->slice(0, 3);
            $first_place = $champions[0];
            $second_place = $champions[1];
            $third_place = $champions[2];
            $champions = [$second_place, $first_place, $third_place]
            @endphp
          <h2>Чемпионы</h2>
          <div class="rating-block">
              @foreach ($champions  as $ratingScore)
                  <div class="block">
                    @if($loop->iteration == 1)
                      <h4 class="text-center text-primary">&#8545; место</h4>
                    @elseif($loop->iteration == 2)
                     <h4 class="text-center text-warning">&#8544; место</h4>
                    @elseif($loop->iteration == 3)
                      <h4 class="text-center text-success">&#8546; место</h4>
                   @endif
                      <div class="thumbnail">
                          <a href="{{route('competitor.show', [$ratingScore->competitor_id])}}"><img src="{{asset($ratingScore->competitor->avatar)}}" /></a>
                      </div>
                      <p class="prize text-center">
                          <i class="fa fa-graduation-cap" aria-hidden="true"></i><small><a href="{{route('competitor.show', [$ratingScore->competitor_id])}}#scores">{{$ratingScore->avscore}}</a></small>
                     </p>
                      <a href="{{route('rating.score', [$nomination->id])}}"
                        @if($loop->iteration == 1)
                          class="lenta prize second"
                        @elseif($loop->iteration == 2)
                          class="lenta prize first"
                        @elseif($loop->iteration == 3)
                          class="lenta prize third"
                       @endif
                        style="height: {{Nomination::ratingLentaHeight($ratingScore->avscore, 2, 'px')}};"></a>
                  </div>
              @endforeach
              </div>
            @endif
          <h2>Рейтинг участников <small>По судейским оценкам</small></h2>
          <div class="rating-block">
              @foreach ($nomination->ratingScores as $ratingScore)
                  <div class="block">
                      <div class="thumbnail">
                          <a href="{{route('competitor.show', [$ratingScore->competitor_id])}}"><img src="{{asset($ratingScore->competitor->avatar)}}" /></a>
                      </div>
                      <p class="prize text-center">
                          <i class="fa fa-graduation-cap" aria-hidden="true"></i><small><a href="{{route('competitor.show', [$ratingScore->competitor_id])}}#scores">{{$ratingScore->avscore}}</a></small>
                     </p>
                      <a href="{{route('rating.score', [$nomination->id])}}" class="lenta prize universal" style="height: {{Nomination::ratingLentaHeight($ratingScore->avscore, 2, 'px')}};"></a>
                  </div>
              @endforeach
              </div>
              <h2>Рейтинг фото <small>По количеству лайков</small></h2>
              <div class="rating-block">
                  @foreach ($nomination->ratingLikes as $ratingLike)
                  <div class="block">
                      <div class="thumbnail">
                          <a href="{{route('photo.show', [$ratingLike->photo_id])}}"><img src="{{asset($ratingLike->photo->photo)}}"/></a>
                      </div>
                      <p class="prize text-center">
                          <i class="fa fa-heart color-red" aria-hidden="true"></i><small><a href="{{route('photo.show', [$ratingLike->photo_id])}}">{{$ratingLike->like_count}}</a></small>
                     </p>
                      <a href="{{route('rating.like', [$nomination->id])}}" class="lenta like universal" style="height: {{Nomination::ratingLentaHeight($ratingLike->like_count, 0.1, 'px')}};"></a>
                  </div>
                  @endforeach
              </div>
              <h2>Судейство <small>Сводные данные по номинации.</small></h2>


                 <table class="table scores">
                    @foreach($nomination->referees as $referee)
                     <tr>
                         <td class="photo">
                          <div class="thumbnail">
                            @if($referee->user->avatar=='default')
                               @php
                               $referee->user->avatar= asset('assets/img/default/avatar.png');
                               @endphp
                            @endif
                              <a href=""><img src="{{asset($referee->user->avatar)}}" /></a>
                          </div>
                         </td>
                         @if ($referee->review->count() > 0)
                          <td class="revize">
                          <small><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;{{$referee->review->count()}}</small>
                         @else
                          <td class="revize no">
                          <small><i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;0</small>
                         @endif
                        </td>
                        @if ($referee->scoreActual->count() > 0)
                         <td class="revize">
                         <small>{!!$nomination->type_icon!!}&nbsp;{{$referee->scoreActual->count()}}</small>
                        @else
                         <td class="revize no">
                         <small>{!!$nomination->type_icon!!}&nbsp;0</small>
                        @endif
                        </td>
                        @if (!empty($referee->avg_score->avgscore))
                         <td class="score">
                              <small class="score-actual"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;{{$referee->avg_score->avgscore}}</small>
                        @else
                         <td class="score no">
                           <small><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;0</small>
                        @endif
                        </td>
                    </tr>
                    @endforeach

                 </table>

        </div>
      </div>
        <div class="col-md-4">
          <table class="table table-striped">
            <tr class="warning">
              <td><a href="#fund-scores" class="text-warning">Призовой фонд</a></td>
              <td class="color-gold"><i class="fa fa-trophy" aria-hidden="true"></i></td>
              <td class="color-grew">{{$nomination->fundsScores->sum}} <i class="fa fa-rub" aria-hidden="true"></i></td>
            </tr>
            <tr>
               <td></td>
                <td class="text-warning">
                    &#8544;&nbsp;место
                </td>

                <td class="color-grew">
                    {{$nomination->fundsScores->position_1}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                </td>
            </tr>
            <tr>
               <td></td>
                <td class="text-info">
                    &#8545;&nbsp;место
                </td>

                <td class="color-grew">
                    {{$nomination->fundsScores->position_2}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                </td>
            </tr>
            <tr>
               <td></td>
                <td class="text-success">
                   &#8546;&nbsp;место
                </td>

                <td class="color-grew">
                    {{$nomination->fundsScores->position_3}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                </td>
            </tr>
            <tr class="danger">
              <td><a href="#fund-likes" class="text-danger">Призовой фонд</a></td>
              <td class="color-red"><i class="fa fa-heart" aria-hidden="true"></i></td>
              <td class="color-grew">{{$nomination->fundsLikes->sum}} <i class="fa fa-rub" aria-hidden="true"></i></td>
            </tr>
            <tr>
              <td></td>
                <td class="text-warning">
                    &#8544;&nbsp;место
                </td>

                <td class="color-grew">
                    {{$nomination->fundsLikes->position_1}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                </td>
            </tr>
            <tr>
              <td></td>
                <td class="text-info">
                    &#8545;&nbsp;место
                </td>

                <td class="color-grew">
                    {{$nomination->fundsLikes->position_2}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                </td>
            </tr>
            <tr>
              <td></td>
                <td class="text-success">
                    &#8546;&nbsp;место
                </td>

                <td class="color-grew">
                    {{$nomination->fundsLikes->position_3}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i>
                </td>
            </tr>
            <tr class="info">
              <td class="color-grew">Старт</td>
              <td class="color-green-bibikini"><i class="fa fa-calendar" aria-hidden="true"></i></td>
              <td class="color-grew">{{AppHelper::getRussianDate($nomination->start)}}</td>
            </tr>
            <tr class="info">
              <td class="color-grew">Финиш</td>
              <td class="color-red"><i class="fa fa-check" aria-hidden="true"></i></td>
              <td class="color-grew">{{AppHelper::getRussianDate($nomination->finish)}}</td>
            </tr>
            <tr class="info">
              <td class="color-grew">До финиша</td>
              @if ($nomination->status == 1)
                  <td class="color-gold"><i class="fa fa-bell" aria-hidden="true"></i></td>
                  <td class="color-grew">{{$nomination->before_finish}} дней</td>
              @elseif  ($nomination->status == 2)
                <td class="color-gold"><i class="fa fa-bell" aria-hidden="true"></i></td>
                <td class="color-grew">Продлена</td>
              @else
                  <td class="color-gold"><i class="fa fa-bell-slash-o" aria-hidden="true"></i></td>
                  <td class="color-grew">Завершена</td>
              @endif

            </tr>
            <tr>
              <td><a href="{{route('nomination.competitors', [$nomination->id])}}">Участники</a></td>
              <td class="color-green-bibikini">{!!$nomination->type_icon!!}</td>
              <td><a href="{{route('nomination.competitors', [$nomination->id])}}">{{$nomination->count_competitors}}</a></td>
            </tr>
            <tr>
              <td><a href="{{route('nomination.photos', [$nomination->id])}}">Фото</a></td>
              <td class="color-grew"><i class="fa fa-camera" aria-hidden="true"></i></td>
              <td><a href="{{route('nomination.photos', [$nomination->id])}}">{{$nomination->count_photos}}</a></td>
            </tr>
            <tr class="success">
              <td class="color-grew">Стоимость участия</td>
              <td class="color-black"><i class="fa fa-credit-card" aria-hidden="true"></i></td>
              <td class="color-grew">{{$nomination->participation_fee}}&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></td>
            </tr>
          </table>
        </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
      <h3>Участники <small></small></h3>
    </div>
    @foreach ($competitors as $competitor)
        <div class="col-sm-3 col-xs-6">
            <div class="thumbnail index-user">
                <div class="avatar">
                  @if($competitor->avatar=='default')
                     @php
                     $competitor->avatar= asset('assets/img/default/avatar.png');
                     @endphp
                  @endif
                    <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}"><img class="img-responsive img-rounded" src="{{asset($competitor->avatar)}}" alt="{{$competitor->name}}"></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="{{route('competitor.show', ['competitor' => $competitor->id])}}">{{$competitor->name}}</a></h4>
                    <p>
                      @if($competitor->is_champion == 1)
                        @if ($competitor->prize_place == 1)
                            <i class="fa fa-trophy color-gold" aria-hidden="true"></i>&nbsp;
                        @elseif ($competitor->prize_place == 2)
                            <i class="fa fa-trophy color-blue" aria-hidden="true"></i>&nbsp;
                        @else
                             <i class="fa fa-trophy color-green-bibikini" aria-hidden="true"></i>&nbsp;
                        @endif
                        @endif
                        <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>
                            @if(!empty($competitor->ratingScore->position))
                            {{$competitor->ratingScore->position}}
                           @endif
                        </small></a>&nbsp;
                        <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#scores" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>
                            @if(!empty(Nomination::avgScore($competitor->id)->first()->avgscore))
                            {{Nomination::avgScore($competitor->id)->first()->avgscore}}
                           @endif
                        </small></a>&nbsp;
                        <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#photos" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>
                            {{Nomination::countLikesCompetitor($competitor->id)}}
                        </small></a>&nbsp;
                        <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#photos" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>
                            {{$competitor->photos->count()}}
                        </small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="{{route('competitor.show', ['competitor' => $competitor->id])}}" class="btn btn-info" role="button"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Подробнее</a></p>
                </div>
            </div>
        </div>
    @endforeach
    <div class="clearfix"></div>


    <p class="text-left">
        <a href="{{route('nomination.competitors', [$nomination->id])}}" class="btn btn-warning btn-lg"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Все участники</a>
    </p>
    </div>

    <div class="row">
      <div class="col-lg-12">
      <h3>Фото <small></small></h3>
    </div>
    @foreach ($photos as $photo)
        <div class="col-sm-3 col-xs-6">
            <div class="thumbnail index-photo">
                <div class="pic">
                    <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="{{route('competitor.show', [$photo->getCompetitor->id])}}">{{$photo->getCompetitor->getUser->name}}</a></h4>
                        <p class="text-right">
                         <a href="" class="color-white opacity-hide-less" ><i class="fa fa-bar-chart" aria-hidden="true"></i></a>&nbsp;
                         <a href="" class="color-white opacity-hide-less" ><small>
                             @if(!empty($photo->ratingLike->position))
                             {{$photo->ratingLike->position}}
                            @endif
                         </small></a>&nbsp;
                         <a href="{{route('like.create', [$photo->id])}}" class="color-white opacity-hide-less" ><i class="fa fa-heart" aria-hidden="true"></i></a>&nbsp;
                         <a href="{{route('like.likes', [$photo->id])}}" class="color-white opacity-hide-less" ><small>{{$photo->likes->count()}}</small></a>&nbsp;
                        <a href="{{route('photo.show', [$photo->id])}}" class="color-white opacity-hide-less"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$photo->comments->count()}}</small></a>&nbsp;
                    </p>
                    </div>
                </div>

            </div>
        </div>
    @endforeach
    <div class="clearfix"></div>

    <p class="text-left">
        <a href="{{route('nomination.photos', [$nomination->id])}}" class="btn btn-danger btn-lg"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Все фото</a>
    </p>
  </div>

    <div class="row">
      <div class="col-lg-12">
    @include('comment.comments_block', ['url'=>'comment.store.nomination', 'instance' => $nomination->id, 'comments' => $nomination->comments])
    </div>
    </div>

@endsection
