@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('nomination.photos', $nomination->id) !!}
@endsection
@section('header')
      <h1>Фотогалерея номинации "{{strip_tags($nomination->name)}}" <small></small></h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-photos') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'all_photos', 'cookie_url'=>'/cookie/photos/informer'])
  @endif
@endsection
@section('content')




    <div class="row">
      <div class="col-lg-12">
        <h3>Фотогалерея номинации</h3>
      </div>
    	@foreach ($photos as $photo)
        <div class="col-sm-4">
            <div class="thumbnail photo">
                <div class="pic
                @if ($photo->getNomination->status == 3)
                 closed
               @endif
                ">
                    <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                    <div class="pic-bar">
                        <h4 class="text-name"><a href="{{route('competitor.show', [$photo->getCompetitor->id])}}">{{$photo->getCompetitor->getUser->name}}</a></h4>
                        <p class="text-right">
                          <p class="text-right">
                           <a href="" class="color-white opacity-hide-less" ><i class="fa fa-bar-chart" aria-hidden="true"></i></a>&nbsp;
                           <a href="" class="color-white opacity-hide-less" ><small>
                               @if(!empty($photo->ratingLike->position))
                               {{$photo->ratingLike->position}}
                              @endif
                           </small></a>&nbsp;
                           @if ($photo->getNomination->status == 3)
                            <i class="fa fa-heart color-white opacity-hide-less" aria-hidden="true"></i>&nbsp;
                          @else
                            <a href="{{route('like.create', [$photo->id])}}" class="color-white opacity-hide-less" ><i class="fa fa-heart" aria-hidden="true"></i></a>&nbsp;
                          @endif
                         <a href="{{route('like.likes', [$photo->id])}}" class="color-white opacity-hide-less" ><small>{{$photo->likes->count()}}</small></a>&nbsp;
                        <a href="{{route('photo.show', [$photo->id])}}" class="color-white opacity-hide-less"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$photo->comments->count()}}</small></a>&nbsp;
                    </p>
                    </div>
                </div>

            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            {{ $photos->links() }}
        </div>
    </div>


@endsection
