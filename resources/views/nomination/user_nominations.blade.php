@extends('nomination.index')
@section('breadcrumbs')
{!! Breadcrumbs::render('users.nominations', $user->id) !!}
@endsection
@section('nomination_title')
Номинации пользователя {{$user->name}} <small>В которых участвовал</small>
@endsection
