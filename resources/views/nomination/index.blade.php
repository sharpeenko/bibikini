@extends('layouts.bibikini')
@section('breadcrumbs')
@yield('breadcrumbs')
@endsection
@section('header')
      <h1>@yield('nomination_title')</h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-nomination') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'all_nominations', 'cookie_url'=>'/cookie/nomination/informer'])
  @endif
@endsection
@section('content')



    <div class="row">
      <div class="col-lg-12">
        <h3>Список номинаций</h3>
      </div>
        @foreach ($nominations as $nomination)
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail nomination">
                <div class="header {{$nomination->css_header}}
                  @if ($nomination->status == 3)
                   closed
                 @endif
                  ">
                    <h3>{!! $nomination->name !!}</h3>
                    <img class="{{$nomination->css_pic}}" src="{{asset($nomination->pic)}}" >
                </div>
                <div class="description">
                    <div class="block sex {{$nomination->css_header}}">
                        {!!$nomination->type_icon!!}
                        <small>{{$nomination->count_competitors}}</small>
                    </div>
                    <div class="block prize">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                        @if (!empty($nomination->fundsScores->sum))
                         <small>{{$nomination->fundsScores->sum}}</small>
                        @else
                          <small></small>
                        @endif
                      </div>
                    <div class="block like">
                      <i class="fa fa-heart" aria-hidden="true"></i>
                      @if (!empty($nomination->fundsLikes->sum))
                       <small>{{$nomination->fundsLikes->sum}}</small>
                      @else
                        <small></small>
                      @endif

                    </div>
                    <div class="block">
                      <i class="fa fa-credit-card" aria-hidden="true"></i>

                       <small>{{$nomination->participation_fee}}</small>


                    </div>
                </div>
                <div class="calendar">
                    <div class="block">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <small>{{$nomination->start}}</small>
                    </div>
                    <div class="block">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <small>{{$nomination->finish}}</small>
                    </div>
                    <div class="block">
                      @if ($nomination->status == 1)
                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                        <small>{{$nomination->before_finish}}</small>
                     @else
                        <i class="fa fa-bell-slash-o" aria-hidden="true"></i>
                     @endif
                    </div>
                </div>
                <div class="caption">
                    <ul class="list-unstyled">
                        {!!$nomination->introtext_list!!}
                    </ul>
                    <p class="text-right">
                        {{--@TODO Здесь, по-идее, кнопка участника должна быть убрана, так как затруднительно в модальное передать. И не только в этом дело, подробности в дневнике разработки--}}
                        {{-- <a href="{{route('competitor.create', [$nomination->id])}}" class="btn btn-warning" role="button" @if(Nomination::checkNominationCompetitor($nomination->id))disabled="disabled"@endif>
                           Участвовать
                    </a> --}}

                    @if(Nomination::checkNominationCompetitor($nomination->id) && $nomination->status == 1)
                      @php
                      $competitor = Nomination::checkNominationCompetitor($nomination->id);
                      @endphp
                      <a href="{{route('competitor.show', [$competitor->id])}}#uploadPhoto" class="btn btn-danger" role="button">
                        <i class="fa fa-camera" aria-hidden="true"></i>&nbsp;
                        <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                      </a>
                    @endif

                    <a href="{{route('nomination.show', [$nomination->id])}}" class="btn btn-info" role="button">Подробнее</a>
                </p>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        {{ $nominations->links() }}
    </div>
</div>

@endsection
