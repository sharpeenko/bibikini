@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('photo.show', $photo->id) !!}
@endsection
@section('header')
    <h1>Фото {{$photo->getCompetitor->getUser->name}} <small>Номинация "{{strip_tags($photo->getNomination->name)}}"</small></h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-photo') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'photo', 'cookie_url'=>'/cookie/photo/informer'])
  @endif
@endsection
@php
 $instance = $photo;
@endphp

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h3>Фотогалерея участника</h3>
  </div>
        <div class="col-sm-6 col-xs-12">
	        <div class="thumbnail photo">
		        <div class="pic big">
		            <a href="{{$photo->photo}}" data-toggle="lightbox" data-title="{{$photo->getCompetitor->getUser->name}}" data-footer="{{$photo->getCompetitor->getUser->name}}">
		                <img src="{{$photo->photo}}" class="img-responsive img-thumbnail pull-left img-left" alt="{{$photo->getCompetitor->getUser->name}}">
		            </a>
		            <div class="pic-bar">

                        <p class="text-right">
                        <a href="{{route('competitor.show', [$photo->getCompetitor->id])}}" class="color-green-bibikini opacity-hide-less"><i class="fa fa-user" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                        <a href="" class="color-black opacity-hide-less" ><i class="fa fa-bar-chart" aria-hidden="true"></i></a>&nbsp;
                        <a href="" class="color-black opacity-hide-less" ><small>
                            @if(!empty($photo->ratingLike->position))
                            {{$photo->ratingLike->position}}
                           @endif
                        </small></a>&nbsp;
                        <a href="{{route('like.create', [$photo->id])}}" class="color-red opacity-hide-less" ><i class="fa fa-heart" aria-hidden="true"></i></a>&nbsp;
                        <a href="{{route('like.likes', [$photo->id])}}" class="color-red opacity-hide-less" ><small>{{$photo->likes->count()}}</small></a>&nbsp;
                        <a href="{{route('photo.show', [$photo->id])}}" class="color-black opacity-hide-less"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$photo->comments->count()}}</small></a>&nbsp;
                    </p>
                    </div>
		        </div>
		    </div>
      </div>
      <div class="col-sm-6">
        @foreach($photos as $photo)
        <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="thumbnail photo">
            <div class="pic mini">
                <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                <div class="pic-bar">

                    <p class="text-center">
                    <a href="" class="color-black opacity-hide-less" ><i class="fa fa-bar-chart" aria-hidden="true"></i></a>&nbsp;
                    <a href="" class="color-black opacity-hide-less" ><small>
                        @if(!empty($photo->ratingLike->position))
                        {{$photo->ratingLike->position}}
                       @endif
                    </small></a>&nbsp;
                    <a href="{{route('like.create', [$photo->id])}}" class="color-red opacity-hide-less" ><i class="fa fa-heart" aria-hidden="true"></i></a>&nbsp;
                    <a href="{{route('like.likes', [$photo->id])}}" class="color-red opacity-hide-less" ><small>{{$photo->likes->count()}}</small></a>&nbsp;
                    <a href="{{route('photo.show', [$photo->id])}}" class="color-black opacity-hide-less" ><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$photo->comments->count()}}</small></a>&nbsp;
                </p>
                </div>
            </div>

        </div>
    </div>
        @endforeach
      </div>
    </div>
		    <div class="row">
          <div class="col-lg-12">
          @include('comment.comments_block', ['url'=>'comment.store.photo', 'instance' => $instance->id, 'comments' => $instance->comments])
        </div>
    </div>



@endsection
