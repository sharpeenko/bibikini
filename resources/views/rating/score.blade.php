@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('rating.score', $nomination->id) !!}
@endsection
@section('header')
      <h1>Рейтинг оценок по номинации  {{strip_tags($nomination->name)}}<small></small></h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-rating-scores') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'rating_score', 'cookie_url'=>'/cookie/rating/scores/informer'])
  @endif
@endsection
@section('content')



    <div class="row">
      <div class="col-lg-12">
        <h3>Рейтинг оценок</h3>
      </div>
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table scores">
                        @foreach ($ratings as $rating)
                            <tr>
                                <td class="photo">
                                    <div class="thumbnail">
                                      @if($rating->competitor->avatar=='default')
                                         @php
                                         $rating->competitor->avatar= asset('assets/img/default/avatar.png');
                                         @endphp
                                      @endif
                                        <a href="{{route('competitor.show', [$rating->competitor_id])}}"><img src="{{asset($rating->competitor->avatar)}}" /></a>
                                    </div>
                                </td>
                                <td class="score">
                                        <small class="score"><i class="fa fa-graduation-cap color-black" aria-hidden="true"></i>&nbsp;{{$rating->avscore}}</small>
                                </td>
                                <td class="score">
                                        <small class="score"><i class="fa fa-bar-chart color-green" aria-hidden="true"></i>&nbsp;{{$rating->position}}</small>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                </div>

            </div>

    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            {{ $ratings->links() }}
        </div>
    </div>


@endsection
