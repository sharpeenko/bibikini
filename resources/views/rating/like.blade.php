@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('rating.like', $nomination->id) !!}
@endsection
@section('header')
      <h1>Рейтинг лайков по номинации  {{strip_tags($nomination->name)}}<small></small></h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-rating-likes') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'rating_likes', 'cookie_url'=>'/cookie/rating/likes/informer'])
  @endif
@endsection

@section('content')


    <div class="row">
      <div class="col-lg-12">
        <h3>Рейтинг лайков</h3>
      </div>
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table scores">
                        @foreach ($ratings as $rating)
                            <tr>
                                <td class="photo">
                                    <div class="thumbnail">
                                        <a href="{{route('photo.show', [$rating->photo_id])}}"><img src="{{asset($rating->photo->photo)}}" /></a>
                                    </div>
                                </td>
                                <td class="score">
                                        <small class="score"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;{{$rating->like_count}}</small>
                                </td>
                                <td class="score">
                                        <small class="score"><i class="fa fa-bar-chart color-green-bibikini" aria-hidden="true"></i>&nbsp;{{$rating->position}}</small>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                </div>

            </div>

    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            {{ $ratings->links() }}
        </div>
    </div>


@endsection
