@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('rating.index') !!}
@endsection
@section('header')
        <h1>Рейтинг лайков и оценок по всем номинациям <small></small></h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-ratings') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'all_ratings', 'cookie_url'=>'/cookie/ratings/informer'])
  @endif
@endsection
@section('content')

    <div class="row">
    	@foreach ($nominations_ratings as $nomination_rating)
            <div class="col-lg-12">
                <h2>{!! $nomination_rating->name !!}</h2>
                <h3>Рейтинг судейских оценок <small>Оценивается по средней оценке.</small></h3>
                <div class="table-responsive">
                    <table class="table scores">
                        @foreach ($nomination_rating->ratingScores as $ratingScore)
                            <tr>
                                <td class="photo">
                                    <div class="thumbnail">
                                      @if($ratingScore->competitor->avatar=='default')
                                         @php
                                         $ratingScore->competitor->avatar= asset('assets/img/default/avatar.png');
                                         @endphp
                                      @endif
                                        <a href="{{route('competitor.show', [$ratingScore->competitor_id])}}"><img src="{{asset($ratingScore->competitor->avatar)}}" /></a>
                                    </div>
                                </td>
                                <td class="score">
                                        <small class="score"><i class="fa fa-graduation-cap color-black" aria-hidden="true"></i>&nbsp;{{$ratingScore->avscore}}</small>
                                </td>
                                <td class="score">
                                        <small class="score"><i class="fa fa-bar-chart color-green" aria-hidden="true"></i>&nbsp;{{$ratingScore->position}}</small>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <p class="text-right">
                        <a href="{{route('rating.score', [$nomination_rating->id])}}" class="btn btn-sm btn-primary">Подробнее</a>
                    </p>
                </div>
                <h3>Рейтинг фото <small>Оценивается по количеству лайков.</small></h3>
                <div class="table-responsive">
                    <table class="table scores">
                        @foreach ($nomination_rating->ratingLikes as $ratingLike)
                        <tr>
                            <td class="photo">
                                <div class="thumbnail">
                                    <a href="{{route('photo.show', [$ratingLike->photo_id])}}"><img src="{{asset($ratingLike->photo->photo)}}" /></a>
                                </div>
                            </td>
                            <td class="score">
                                    <small class="score"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;{{$ratingLike->like_count}}</small>
                            </td>
                            <td class="score">
                                    <small class="score"><i class="fa fa-bar-chart color-green-bibikini" aria-hidden="true"></i>&nbsp;{{$ratingLike->position}}</small>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <p class="text-right">
                        <a href="{{route('rating.like', [$nomination_rating->id])}}" class="btn btn-sm btn-primary">Подробнее</a>
                    </p>
                </div>
            </div>
     @endforeach
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            {{ $nominations_ratings->links() }}
        </div>
    </div>


@endsection
