<div class="row">
    @foreach ($users as $user)
    <div class="col-md-3 col-xs-3">
        <div class="thumbnail user-pic">
            <a href="{{route('users.show', ['user' => $user->id])}}">
                <img src="{{asset($user->avatar)}}" alt="{{$user->name}}" data-toggle="tooltip" data-placement="left" title="{{$user->name}}">
            </a>
        </div>
    </div>
    @endforeach
    <div class="clearfix"></div>
    <div class="col-lg-12">
        <p><a href="#" class="btn btn-primary" role="button">Все пользователи</a></p>
    </div>
</div>






