@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('users.index') !!}
@endsection
@section('header')
      <h1>Все пользователи <small></small></h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-users') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'all_users', 'cookie_url'=>'/cookie/users/informer'])
  @endif
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h3>Список пользователей</h3>
  </div>
        @foreach ($users as $user)
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail user">
              @if($user->avatar=='default')
                 @php
                 $user->avatar= asset('assets/img/default/avatar.png');
                 @endphp
              @endif
                <div class="avatar">
                    <a href="{{route('users.show', ['user' => $user->id])}}"><img class="img-responsive img-rounded" src="{{asset($user->avatar)}}" alt="{{$user->name}}"></a>
                </div>

                <div class="caption">
                    <h4 class="text-name"><a href="{{route('users.show', ['user' => $user->id])}}">{{$user->name}}</a></h4>
                    <p>
                        <a href="{{route('users.nominations', [$user->id])}}" tabindex="0" class="color-black opacity-hide"><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp;<small>{{$user->competitors->count()}}</small></a>&nbsp;
                        <a href="{{route('users.comments', [$user->id])}}" class="color-black opacity-hide" ><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$user->comments->count()}}</small></a>&nbsp;
                        <a href="{{route('users.show', ['user' => $user->id])}}#photos" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>
                        {{Nomination::countPhotoUser($user->id)}}
                        </small></a>&nbsp;
                    </p>
                    <p class="text-right"><a href="{{route('users.show', ['user' => $user->id])}}" class="btn btn-success" role="button">Подробнее</a></p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            {{ $users->links() }}
        </div>
    </div>

@endsection
