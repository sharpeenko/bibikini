@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('users.edit', $user->id) !!}
@endsection
@section('header')
        <h1>{{$user->name}} <small></small></h1>
@endsection
@section('informer')
  @if (!Cookie::get('informer-user-edit') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'user_edit', 'cookie_url'=>'/cookie/user/edit/informer'])
  @endif
@endsection

@section('content')

<div class="row">
        <div class="col-sm-6">
          @if($user->avatar=='default')
             @php
             $user->avatar= asset('assets/img/default/avatar.png');
             @endphp
          @endif
            <a href="{{asset($user->avatar)}}" data-toggle="lightbox" data-title="{{$user->name}}" data-footer="{{$user->name}}">
                <img src="{{asset($user->avatar)}}" class="img-responsive img-thumbnail pull-left img-content img-left" alt="{{$user->name}}">
            </a>
        </div>
        <div class="col-sm-6">
            <h3>Информация</h3>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td>
                            Имя
                        </td>
                        <td colspan="2">
                            {{$user->name}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Хэштег
                        </td>
                        <td colspan="2">
                            {{$user->hashtag}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Пол
                        </td>
                        <td colspan="2">
                            {{$user->sex}}
                        </td>
                    </tr>
                    <tr>
                        <td class="color-red">
                            <i class="fa fa-heart fa-2x" aria-hidden="true"></i>
                        </td>
                        <td>
                            <span class="label label-info">
                             @if(!$user->likes)
                             0
                            @else
                             {{$user->likes->like_count}}
                            @endif
                            </span>
                        </td>
                        <td>
                            <a href="{{route('like.buy', [$user->id])}}" class="btn btn-success">Купить!</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="form-control btn-primary" data-toggle="collapse" data-target="#editdata">
                Редактировать информацию, изменить аватар <i class="fa fa-chevron-down" aria-hidden="true"></i>
            </button>

            <div id="editdata" class="collapse">
                <form class="form-horizontal incollapse" role="form" method="POST" action="{{ url('users/'.$user->id) }}" enctype = "multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Имя пользователя <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{{$user->name}}">
                            <p class="help-block text-danger">{!! $errors->first('name') !!}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sex" class="col-sm-2 control-label">Пол <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <select name="sex" class="form-control">
                                <option value="мужской" {{MenuHelper::getSelectedOption('мужской', $user->sex)}}>мужской</option>
                                <option value="женский" {{MenuHelper::getSelectedOption('женский', $user->sex)}}>женский</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="avatar" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control btn-primary" name="avatar" title="Загрузить аватар">
                            <p class="help-block text-danger">{!! $errors->first('avatar') !!}</p>
                            <p  class="text-danger"><small>Допускается загрузка изображений форматов png, jpg, jpeg, gif размером не более 1МБ.</small></p>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5 col-xs-6">
                          <a href="{{Request::url()}}" class="btn btn-danger">Отменить</a>
                        </div>
                        <div class="col-sm-5 col-xs-6">
                            <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="form-control btn-primary" data-toggle="collapse" data-target="#editpassword">
                Изменить пароль <i class="fa fa-chevron-down" aria-hidden="true"></i>
            </button>

            <div id="editpassword" class="collapse">
                <form class="form-horizontal incollapse" role="form" method="POST" action="{{ url('users/'.$user->id.'/password') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Пароль <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" placeholder="Введите пароль">
                            <p class="help-block text-danger">{!! $errors->first('password') !!}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password_confirm" class="col-sm-2 control-label">Повторите пароль<span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Повторите пароль">
                            <p class="help-block text-danger">{!! $errors->first('password_confirmation') !!}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-7">
                            <button type="submit" class="btn btn-success">Подтвердить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br><br>


    <div class="row">
      <div class="col-lg-12">
        <a name="mynominations"></a>
      <h3>Мои номинации <small></small></h3>
    </div>
    @foreach ($nominations as $nomination)
               <div class="col-md-4 col-sm-6">
                 <div class="thumbnail nomination user">
                     <div class="header {{$nomination->css_header}}
                       @if ($nomination->status == 3)
                        closed
                      @endif
                       ">
                         <h3>{!! $nomination->name !!}</h3>
                         <img class="{{$nomination->css_pic}}" src="{{asset($nomination->pic)}}" >
                     </div>
                     <div class="description">
                         <div class="block sex {{$nomination->css_header}}">
                             {!! $nomination->type_icon !!}
                             <small>{{$nomination->count_competitors}}</small>
                         </div>
                         <div class="block prize">
                             <i class="fa fa-trophy" aria-hidden="true"></i>
                             @if (!empty($nomination->fund_scores))
                                 <small>{{$nomination->fund_scores}}</small>
                             @else
                                 <small></small>
                             @endif
                           </div>
                             <div class="block like">
                               <i class="fa fa-heart" aria-hidden="true"></i>
                               @if (!empty($nomination->fund_likes))
                                <small>{{$nomination->fund_likes}}</small>
                               @else
                                 <small></small>
                               @endif

                             </div>
                             <div class="block">
                               <i class="fa fa-credit-card" aria-hidden="true"></i>

                                <small>{{$nomination->participation_fee}}</small>


                             </div>
                     </div>
                     <div class="calendar">
                         <div class="block">
                             <i class="fa fa-calendar" aria-hidden="true"></i>
                             <small>{{$nomination->start}}</small>
                         </div>
                         <div class="block">
                             <i class="fa fa-check" aria-hidden="true"></i>
                             <small>{{$nomination->finish}}</small>
                         </div>
                         <div class="block">
                           @if ($nomination->status == 1)
                             <i class="fa fa-bell-o" aria-hidden="true"></i>
                             <small>{{$nomination->before_finish}}</small>
                          @else
                             <i class="fa fa-bell-slash-o" aria-hidden="true"></i>
                          @endif
                         </div>
                     </div>
                     <div class="competitor">
                         <div class="block avatar">
                           @if($nomination->avatar=='default')
                              @php
                              $nomination->avatar= asset('assets/img/default/avatar.png');
                              @endphp
                           @endif
                             <a href="{{route('competitor.show', [$nomination->competitor_id])}}">
                                 <img class="img-thumbnail" src="{{asset($nomination->avatar)}}">
                             </a>
                         </div>
                         <div class="block photo text-center">
                             <i class="fa fa-camera-retro" aria-hidden="true"></i>
                             <small><a href="{{route('competitor.show', [$nomination->competitor_id])}}#photos">{{Nomination::competitorForUserProfile($nomination->id, $user->id)->photos->count()}}</a></small>
                         </div>
                         <div class="block rating text-center">
                             <i class="fa fa-bar-chart" aria-hidden="true"></i>
                             @if(!empty(Nomination::competitorForUserProfile($nomination->id, $user->id)->ratingScore->position))
                             <small><a href="">{{Nomination::competitorForUserProfile($nomination->id, $user->id)->ratingScore->position}}</a></small>
                           @endif
                         </div>
                         <div class="block like text-center">
                             <i class="fa fa-heart" aria-hidden="true"></i>
                             <small><a href="">{{Nomination::countLikesCompetitor($nomination->competitor_id)}}</a></small>
                         </div>
                     </div>
                     <div class="caption">
                         <p class="text-right">
                             <a href="{{route('nomination.show', [$nomination->id])}}" class="btn btn-primary" role="button">Перейти</a>
                         </p>
                     </div>
                 </div>
               </div>
           @endforeach
      </div>

      <div class="row">
        <div class="col-lg-12">
          <a name="myratings"></a>
        <h3>Мои рейтинги <small></small></h3>
      </div>


        <div class="col-lg-12">

              <h3>Мои рейтинги по судейским оценкам</h3>

              <div class="table-responsive">
                  <table class="table scores">
                      @foreach ($user->competitors as $competitor)
                          <tr>
                            <td class="photo">
                                <div class="thumbnail">
                                    <a href="{{route('nomination.show', [$competitor->getNomination->id])}}"><img src="{{asset($competitor->getNomination->pic)}}" /></a>
                                </div>
                            </td>
                              <td class="photo">
                                  <div class="thumbnail">
                                    @if($competitor->avatar=='default')
                                       @php
                                       $competitor->avatar= asset('assets/img/default/avatar.png');
                                       @endphp
                                    @endif
                                      <a href="{{route('competitor.show', [$competitor->id])}}"><img src="{{asset($competitor->avatar)}}" /></a>
                                  </div>
                              </td>
                              <td class="score">
                                <small class="score"><i class="fa fa-graduation-cap color-black" aria-hidden="true"></i>&nbsp;
                                  @if(!empty($competitor->ratingScore->avscore))
                                    {{$competitor->ratingScore->avscore}}
                                  @endif
                                  </small>
                              </td>
                              <td class="score">
                                      <small class="score"><i class="fa fa-bar-chart color-green" aria-hidden="true"></i>&nbsp;
                                        @if(!empty($competitor->ratingScore->position))
                                            {{$competitor->ratingScore->position}}
                                        @endif
                                      </small>
                              </td>
                          </tr>
                      @endforeach
                  </table>
                </div>

                <h3>Мои рейтинги фото</h3>

                <div class="table-responsive">
                    <table class="table scores">
                      @foreach ($user->competitors as $competitor)
                        @foreach ($competitor->photos as $photo)
                            <tr>
                              <td class="photo">
                                  <div class="thumbnail">
                                      <a href="{{route('nomination.show', [$photo->getNomination->id])}}"><img src="{{asset($photo->getNomination->pic)}}" /></a>
                                  </div>
                              </td>
                              <td class="photo">
                                  <div class="thumbnail">
                                    @if($photo->getCompetitor->avatar=='default')
                                       @php
                                       $photo->getCompetitor->avatar= asset('assets/img/default/avatar.png');
                                       @endphp
                                    @endif
                                      <a href="{{route('competitor.show', [$photo->getCompetitor->id])}}"><img src="{{asset($photo->getCompetitor->avatar)}}" /></a>
                                  </div>
                              </td>
                              <td class="photo">
                                  <div class="thumbnail">
                                      <a href="{{route('photo.show', [$photo->id])}}"><img src="{{asset($photo->photo)}}" /></a>
                                  </div>
                              </td>

                              <td class="score">
                                      <small class="score"><i class="fa fa-heart color-red" aria-hidden="true"></i>&nbsp;
                                        @if(!empty($photo->ratingLike->like_count))
                                          {{$photo->ratingLike->like_count}}
                                      @endif
                                      </small>
                              </td>
                              <td class="score">
                                      <small class="score"><i class="fa fa-bar-chart color-green-bibikini" aria-hidden="true"></i>&nbsp;
                                        @if(!empty($photo->ratingLike->position))
                                        {{$photo->ratingLike->position}}
                                      @endif
                                      </small>
                              </td>
                            </tr>
                        @endforeach
                        @endforeach
                    </table>
                  </div>
                </div>
        </div>


      <div class="row">
        <div class="col-lg-12">
          <a name="myphotos"></a>
        <h3>Мои фото <small></small></h3>
      </div>
      @foreach ($user->competitors as $competitor)
       @foreach ($competitor->getPhoto as $photo)
        @if($photo->photo_status_id == 1)

               <div class="col-md-2 col-xs-4">
           <div class="thumbnail photo">
               <div class="pic mini download">
                   <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                   <div class="pic-bar">
                       <p class="text-center">
                           Проверка
                       </p>
                   </div>
               </div>

           </div>
       </div>

           @elseif($photo->photo_status_id == 2)
               <div class="col-md-2 col-xs-4">
                   <div class="thumbnail photo">
                       <div class="pic mini">
                           <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                           <div class="pic-bar">

                               <p class="text-right">
                                   <a href="" class="color-black opacity-hide-less" ><i class="fa fa-bar-chart" aria-hidden="true"></i></a>&nbsp;
                                   <a href="" class="color-black opacity-hide-less" ><small>
                                       @if(!empty($photo->ratingLike->position))
                                       {{$photo->ratingLike->position}}
                                      @endif</small></a>&nbsp;
                                <a href="{{route('like.create', [$photo->id])}}" class="color-red opacity-hide-less" ><i class="fa fa-heart" aria-hidden="true"></i></a>&nbsp;
                                <a href="{{route('like.likes', [$photo->id])}}" class="color-red opacity-hide-less" ><small>{{$photo->likes->count()}}</small></a>&nbsp;
                               <a href="{{route('photo.show', [$photo->id])}}" class="color-black opacity-hide-less"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$photo->comments->count()}}</small></a>&nbsp;
                           </p>
                           </div>
                       </div>

                   </div>
               </div>

           @else
               <div class="col-md-2 col-xs-4">
           <div class="thumbnail photo">
               <div class="pic mini rejected">
                   <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                   <div class="pic-bar">
                       <p class="text-center">
                           Отклонена
                       </p>
                   </div>
               </div>

           </div>
       </div>

       @endif
       @endforeach
      @endforeach
           </div>
           <div class="row">
             <div class="col-lg-12">
             <h3>Мои комментарии <small></small></h3>
           </div>
           <div class="col-lg-12">
             @foreach ($user->comments as $comment)
                  <div class="media comment-preview">
                      <div class="media-body">
                          <h4 class="media-heading">
                          @if($comment->commentable_type == 'App\News')
                              <a href="{{route('news.show', [$comment->commentable_id])}}">
                              <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                              &nbsp;{{$comment->commentable->header}}</a>
                          @endif
                          @if($comment->commentable_type == 'App\Competitor')
                              <a href="{{route('competitor.show', [$comment->commentable_id])}}">
                              <i class="fa fa-trophy" aria-hidden="true"></i>
                              &nbsp;{{strip_tags($comment->commentable->getNomination->name)}}
                          </a>
                          @endif
                          @if($comment->commentable_type == 'App\Photo')
                              <a href="{{route('photo.show', [$comment->commentable_id])}}">
                              <i class="fa fa-camera" aria-hidden="true"></i>
                              &nbsp;{{$comment->commentable->getCompetitor->getUser->name}}
                          </a>
                          @endif


                          <br>
                          <small>{{ date('Y-m-d H:i:s', strtotime($comment->publish_at)) }}</small>
                          </h4>
                              {{$comment->content}}
                          </div>
                          <div class="clearfix"></div>
                          <div class="divider"></div>
                          <p class="text-right">
                              @if($comment->commentable_type == 'App\News')
                              <a href="{{route('news.show', [$comment->commentable_id])}}#comment_{{$comment->id}}" class="btn btn-sm btn-primary">Перейти</a>
                              @endif
                              @if($comment->commentable_type == 'App\Competitor')
                              <a href="{{route('competitor.show', [$comment->commentable_id])}}#comment_{{$comment->id}}" class="btn btn-sm btn-primary">Перейти</a>
                              @endif
                              @if($comment->commentable_type == 'App\Photo')
                              <a href="{{route('photo.show', [$comment->commentable_id])}}#comment_{{$comment->id}}" class="btn btn-sm btn-primary">Перейти</a>
                              @endif

                          </p>
                      </div>

              @endforeach

              <p class="text-left">
                  <a href="{{route('users.comments', [$user->id])}}" class="btn btn-success">Все комментарии</a>
              </p>
           </div>
           </div>

@endsection
