@extends('layouts.layout')
@section('breadcrumbs')
{!! Breadcrumbs::render('users.newscomments', $user->id) !!}
@endsection
@section('header')
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@endsection
@section('content')

<section id="allcomments" class="autohight">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Комментарии пользователя</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="comments">
                    @foreach($user_comment_news as $comment)
                    <div class="comments-block">
                        <div class="col-lg-12">
                            <a class="comment-header" href="{{route('news.show', [$comment->new_id])}}">{{$comment->header}}</a> &nbsp;&nbsp;&nbsp; <span class="text-muted">({{ date('Y-m-d H:i:s', strtotime($comment->publish_at))}})</span>
                            <br><br>
                            <p>
                                {{$comment->content}}
                            </p>
                            <p>
                                <a class="link-addcomment btn btn-default btn-blue" href="{{route('news.show', [$comment->new_id])}}#comment_{{$comment->id}}">Перейти</a>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    {!! $user_comment_news->links() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
