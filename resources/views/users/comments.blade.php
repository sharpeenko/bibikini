@extends('layouts.bibikini')
@section('breadcrumbs')
{!! Breadcrumbs::render('users.comments', $user->id) !!}
@endsection
@section('header')
      <h1>Комментарии пользователя {{$user->name}} <small></small></h1>
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h3>Комментарии</h3>
            @foreach ($comments as $comment)
                <div class="media comment-preview">
                    <div class="media-body">
                        <h4 class="media-heading">
                        @if($comment->commentable_type == 'App\News')
                            <a href="{{route('news.show', [$comment->commentable_id])}}">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            &nbsp;{{$comment->commentable->header}}</a>
                        @endif
                        @if($comment->commentable_type == 'App\Competitor')
                            <a href="{{route('competitor.show', [$comment->commentable_id])}}">
                            <i class="fa fa-trophy" aria-hidden="true"></i>
                            &nbsp;{{strip_tags($comment->commentable->getNomination->name)}}
                        </a>
                        @endif
                        @if($comment->commentable_type == 'App\Photo')
                            <a href="{{route('photo.show', [$comment->commentable_id])}}">
                            <i class="fa fa-camera" aria-hidden="true"></i>
                            &nbsp;{{$comment->commentable->getCompetitor->getUser->name}}
                        </a>
                        @endif


                        <br>
                        <small>{{ date('Y-m-d H:i:s', strtotime($comment->publish_at)) }}</small>
                        </h4>
                            {{$comment->content}}
                        </div>
                        <div class="clearfix"></div>
                        <div class="divider"></div>
                        <p class="text-right">
                            @if($comment->commentable_type == 'App\News')
                            <a href="{{route('news.show', [$comment->commentable_id])}}#comment_{{$comment->id}}" class="btn btn-sm btn-primary">Перейти</a>
                            @endif
                            @if($comment->commentable_type == 'App\Competitor')
                            <a href="{{route('competitor.show', [$comment->commentable_id])}}#comment_{{$comment->id}}" class="btn btn-sm btn-primary">Перейти</a>
                            @endif
                            @if($comment->commentable_type == 'App\Photo')
                            <a href="{{route('photo.show', [$comment->commentable_id])}}#comment_{{$comment->id}}" class="btn btn-sm btn-primary">Перейти</a>
                            @endif

                        </p>
                    </div>

            @endforeach

        </div>
    </div>
    <div class="row">
    <div class="col-lg-12 text-center">
        {{ $comments->links() }}
    </div>
</div>

@endsection
