@extends('layouts.bibikini')
{{--@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="active">Главная</li>
    </ol>
@endsection --}}
{{-- @section('header')
    <h1>На старт! <small></small></h1>
@endsection --}}
@section('informer')
  @if (!Cookie::get('informer-index') )
    @include('widget.informer',['contents' => $contents, 'content_key'=>'index_quick_start_1', 'cookie_url'=>'/cookie/index/informer'])
  @endif
  {{--@php
  $informer_index = false;
  if(isset($_COOKIE['informer-index'])) {
    $informer_index = true;
  }
  @endphp

  @if (!$informer_index)
    @include('widget.informer',['contents' => $contents, 'content_key'=>'index_quick_start_1'])
  @endif--}}
@endsection


@section('content')
{{-- <div class="row">
          <div class="col-lg-12">
          <h3>Главные новости</h3>
        </div>
      @foreach ($important_news as $important_new)
        <div class="col-md-4 col-sm-4">
          <div class="jumbotron {{$important_new->important_styles}}">
              <a href="{{route('news.show', [$important_new->id])}}">
                  <img class="img-thumbnail img-responsive" src="{{$important_new->pic}}"/>
              </a>
              <p>
                  <a href="{{route('news.show', [$important_new->id])}}">
                    {{$important_new->important_intro}}
                  </a>
              </p>
              <p class="text-center">
                  <a href="{{route('news.show', [$important_new->id])}}" class="btn {{$important_new->important_button_style}} btn-lg btn-nommi" role="button">{!! $important_new->important_button_icon !!}&nbsp;{{$important_new->important_button_text}}</a>
              </p>

          </div>
        </div>
      @endforeach
</div> --}}


<div class="row">
  <div class="col-lg-12">
  <h3>Номинации <small></small></h3>
</div>
@foreach ($nominations as $nomination)
           <div class="col-md-3 col-sm-6">
               <div class="jumbotron nomination">
                   <div class="header {{$nomination->css_header}}
                     @if ($nomination->status == 3)
                      closed
                    @endif
                     ">
                       <div class="block">
                           <img class="img-responsive pull-left" src="{{asset($nomination->pic)}}"/>
                       </div>
                       <div class="block">
                           <h3 class="pull-left">{!! $nomination->name !!}</h3>
                       </div>
                  </div>
                  <div class="description">
                  <div class="block sex {{$nomination->css_header}}">
                      {!!$nomination->type_icon!!}
                      <small>{{$nomination->count_competitors}}</small>
                  </div>
                  <div class="block prize">
                      <i class="fa fa-trophy" aria-hidden="true"></i>
                      @if (!empty($nomination->fundsScores->sum))
                       <small>{{$nomination->fundsScores->sum}}</small>
                      @else
                        <small></small>
                      @endif
                  </div>
                  <div class="block like">
                      <i class="fa fa-heart" aria-hidden="true"></i>
                      @if (!empty($nomination->fundsLikes->sum))
                       <small>{{$nomination->fundsLikes->sum}}</small>
                      @else
                        <small></small>
                      @endif
                  </div>
              </div>
              <div class="calendar">
                      <div class="block text-center">
                        @if ($nomination->status == 1)
                          <i class="fa fa-bell-o" aria-hidden="true"></i>
                          <small>{{$nomination->before_finish}}</small>
                       @else
                          <i class="fa fa-bell-slash-o" aria-hidden="true"></i>
                       @endif

                      </div>
                  </div>
                   <p class="text-center">
                       <a href="{{route('nomination.show', [$nomination->id])}}" class="btn btn-danger btn-lg btn-nommi" role="button"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Подробнее!</a>
                   </p>

               </div>
           </div>
       @endforeach
       <div class="clearfix"></div>

       <p class="text-left">
           <a href="{{url('/nomination')}}" class="btn btn-success btn-lg"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Все номинации</a>
       </p>



</div>


        <div class="row">
            <div class="col-lg-12">
            <h3>Реклама</h3>
          </div>
          {{--<div class="col-md-2 col-sm-3 col-xs-4">
            <a href=""><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/index_2.jpg')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4">
            <a href=""><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/index_3.jpg')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4">
            <a href=""><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/index_2.jpg')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4 hidden-xs">
            <a href=""><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/index_3.jpg')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4 hidden-sm hidden-xs">
            <a href=""><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/index_3.jpg')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4 hidden-sm hidden-xs">
            <a href=""><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/index_2.jpg')}}"/></a>
          </div>--}}

          <div class="col-md-2 col-sm-3 col-xs-4">
            <a href="/feedback"><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/reclama.png')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4">
            <a href="/feedback"><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/reclama.png')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4">
            <a href="/feedback"><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/reclama.png')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4 hidden-xs">
            <a href="/feedback"><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/reclama.png')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4 hidden-sm hidden-xs">
            <a href="/feedback"><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/reclama.png')}}"/></a>
          </div>
          <div class="col-md-2 col-sm-3 col-xs-4 hidden-sm hidden-xs">
            <a href="/feedback"><img class="img-thumbnail img-responsive" src="{{asset('/assets/img/test/reclama.png')}}"/></a>
          </div>
        </div>



        <div class="row">
          <div class="col-lg-12">
          <h3>Участники <small></small></h3>
        </div>
        @foreach ($competitors as $competitor)
            <div class="col-sm-3 col-xs-6">
                <div class="thumbnail index-user">
                    <div class="avatar">
                      @if($competitor->avatar=='default')
                         @php
                         $competitor->avatar= asset('assets/img/default/avatar.png');
                         @endphp
                      @endif
                        <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}"><img class="img-responsive img-rounded" src="{{asset($competitor->avatar)}}" alt="{{$competitor->name}}"></a>
                    </div>

                    <div class="caption">
                        <h4 class="text-name"><a href="{{route('competitor.show', ['competitor' => $competitor->id])}}">{{$competitor->name}}</a></h4>
                        <p>

                            <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}" class="color-black opacity-hide"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<small>
                                @if(!empty($competitor->ratingScore->position))
                                {{$competitor->ratingScore->position}}
                               @endif
                            </small></a>&nbsp;
                            <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#scores" class="color-black opacity-hide"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<small>
                                @if(!empty(Nomination::avgScore($competitor->id)->first()->avgscore))
                                {{Nomination::avgScore($competitor->id)->first()->avgscore}}
                               @endif
                            </small></a>&nbsp;
                            <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#photos" class="color-red opacity-hide"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;<small>
                                {{Nomination::countLikesCompetitor($competitor->id)}}
                            </small></a>&nbsp;
                            <a href="{{route('competitor.show', ['competitor' => $competitor->id])}}#photos" class="color-black opacity-hide"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;<small>
                                {{$competitor->photos->count()}}
                            </small></a>&nbsp;
                        </p>
                        <p class="text-right"><a href="{{route('competitor.show', ['competitor' => $competitor->id])}}" class="btn btn-info" role="button"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Подробнее</a></p>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="clearfix"></div>


        <p class="text-left">
            <a href="{{url('/competitor')}}" class="btn btn-warning btn-lg"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Все участники</a>
        </p>
        </div>

        <div class="row">
          <div class="col-lg-12">
          <h3>Фото <small></small></h3>
        </div>
        @foreach ($photos as $photo)
            <div class="col-sm-3 col-xs-6">
                <div class="thumbnail index-photo">
                    <div class="pic">
                        <a href="{{route('photo.show', [$photo->id])}}"><img class="img-responsive img-rounded" src="{{$photo->photo}}" alt="..."></a>
                        <div class="pic-bar">
                            <h4 class="text-name"><a href="{{route('competitor.show', [$photo->getCompetitor->id])}}">{{$photo->getCompetitor->getUser->name}}</a></h4>
                            <p class="text-right">
                             <a href="" class="color-white opacity-hide-less" ><i class="fa fa-bar-chart" aria-hidden="true"></i></a>&nbsp;
                             <a href="" class="color-white opacity-hide-less" ><small>
                                 @if(!empty($photo->ratingLike->position))
                                 {{$photo->ratingLike->position}}
                                @endif
                             </small></a>&nbsp;
                             <a href="{{route('like.create', [$photo->id])}}" class="color-white opacity-hide-less" ><i class="fa fa-heart" aria-hidden="true"></i></a>&nbsp;
                             <a href="{{route('like.likes', [$photo->id])}}" class="color-white opacity-hide-less" ><small>{{$photo->likes->count()}}</small></a>&nbsp;
                            <a href="{{route('photo.show', [$photo->id])}}" class="color-white opacity-hide-less"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;<small>{{$photo->comments->count()}}</small></a>&nbsp;
                        </p>
                        </div>
                    </div>

                </div>
            </div>
        @endforeach
        <div class="clearfix"></div>

        <p class="text-left">
            <a href="{{url('/photo')}}" class="btn btn-danger btn-lg"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Все фото</a>
        </p>
      </div>


      {{--<div class="row">
        <div class="col-lg-12">
        <h3>Новости <small></small></h3>
      </div>
      @foreach ($news as $newone)
        <div class="col-sm-3 col-xs-6">
          <div class="media newslist-content">
      <h5 class="media-heading"><a href="{{route('news.show', [$newone->id])}}">{{$newone->header}}</a></h5>
      <a href="{{route('news.show', [$newone->id])}}">
          <img class="media-object img-thumbnail" src="{{$newone->pic}}" alt="{{$newone->header}}">
      </a>
      <div class="media-body">
          <p class="text-muted pull-left"><small>{{$newone->author}}</small></p>
          <p class="text-muted pull-right"><small>{{ date('d.m.Y', strtotime($newone->publish_at)) }}</small></p>
          <div class="clearfix"></div>
          <p>
          {!!$newone->introtext!!}
          </p>
      </div>

      <p class="text-right">
          <a href="{{route('news.show', [$newone->id])}}" class="btn btn-lg btn-warning"><i class="fa fa-chevron-right" aria-hidden="true"></i>&nbsp;Подробнее</a>
      </p>
  </div>
</div>
      @endforeach
    </div>--}}



@endsection
