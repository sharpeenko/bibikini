<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', ['uses' => 'IndexController@index', 'as' => 'index']);

Auth::routes();

//Подтверждение регистрации
Route::get('/confirm/{id}/{code}', ['uses' => 'Auth\RegisterController@confirmRegistration', 'as' => 'confirm']);

//Аутентификация, переопределенные роуты
// Вызов страницы регистрации пользователя
Route::get('/register', ['uses' => 'Auth\RegisterController@showRegistrationForm', 'as' => 'register']);

// Вызов формы ввода e-mail для сброса пароля
Route::get('/password/reset', ['uses' => 'Auth\ForgotPasswordController@showLinkRequestForm', 'as' => 'password.reset']);

// Вызов формы ввода нового пароля при сбросе
Route::get('/password/reset/{token}', ['uses' => 'Auth\ResetPasswordController@showResetForm', 'as' => 'password.token']);

// При запрещенном к просмотру разделе
Route::get('noaccess', ['uses' => 'Auth\NoaccessController@noaccess', 'as' => 'noaccess']);

Route::get('/home', 'HomeController@index');

//Пользователи
Route::resource('users', 'UserController');
Route::post('users/search', ['uses' => 'UserController@search', 'as' => 'users.search']);
Route::post('users/{user}/password', ['uses' => 'UserController@changePassword', 'as' => 'users.password']);
Route::get('users/{users}/newscomments', ['uses' => 'UserController@newsComments', 'as' => 'users.newscomments']);
Route::get('users/{users}/nominations', ['uses' => 'UserController@getUserNominations', 'as' => 'users.nominations']);
Route::get('users/comments/{user}', ['uses' => 'UserController@comments', 'as' => 'users.comments']);

// Обратная связь

Route::resource('feedback', 'FeedbackController');
Route::post('feedback/send', ['uses' => 'FeedbackController@send', 'as' => 'feedback.send']);

//Новости

Route::resource('news', 'NewsController');
Route::post('news/{news}/{user}/addcomment', ['uses' => 'NewsController@addComment', 'as' => 'news.addcomment']);

//Номинация

Route::resource('nomination', 'NominationController');
Route::get('nomination/{nomination}/competitors', ['uses' => 'NominationController@getCompetitors', 'as' => 'nomination.competitors']);
Route::get('nomination/{nomination}/description', ['uses' => 'NominationController@showDescription', 'as' => 'nomination.description']);
Route::get('nomination/{nomination}/photos', ['uses' => 'NominationController@photos', 'as' => 'nomination.photos']);
Route::get('nomination/{nomination}/{agree?}', ['uses' => 'NominationController@show', 'as' => 'nomination.show']);

// Competitors

Route::resource('competitor', 'CompetitorController');
Route::post('competitor/{nomination}/create', ['uses' => 'CompetitorController@create', 'as' => 'competitor.create']);
//Route::post('competitor/search', ['uses' => 'CompetitorController@search', 'as' => 'competitor.search']);
Route::post('competitor', ['uses' => 'CompetitorController@index', 'as' => 'competitor.search']);
Route::get('champion', ['uses' => 'CompetitorController@champions', 'as' => 'champion']);


//Фотогалерея

Route::resource('photo', 'PhotoController');
Route::post('photo/{competitor}/{nomination}', ['uses' => 'PhotoController@store', 'as' => 'photo.store']); // основное фото
Route::post('photo/{competitor}/{nomination}/addition', ['uses' => 'PhotoController@storeAddition', 'as' => 'photo.store.addition']); // дополнительное фото
Route::get('photo/confirm/addition/{competitor}/{photo}', ['uses' => 'PhotoController@confirmAddition', 'as' => 'photo.confirm.addition']); // дополнительное фото, подтверждение публикации
Route::post('photo/search', ['uses' => 'PhotoController@index', 'as' => 'photo.search']);

//Комментарии

Route::resource('comment', 'CommentController');
Route::post('comment/add/news/{instance}/{user}', ['uses' => 'CommentController@storeCommentNews', 'as' => 'comment.store.news']);
Route::post('comment/add/competitor/{instance}/{user}', ['uses' => 'CommentController@storeCommentCompetitor', 'as' => 'comment.store.competitor']);
Route::post('comment/add/photo/{instance}/{user}', ['uses' => 'CommentController@storeCommentPhoto', 'as' => 'comment.store.photo']);
Route::post('comment/add/nomination/{instance}/{user}', ['uses' => 'CommentController@storeCommentNomination', 'as' => 'comment.store.nomination']);
Route::get('comment/info/{comment}/{user}', ['uses' => 'CommentController@infoModerator', 'as' => 'comment.info.moderator']);

// Лайки

Route::resource('like', 'LikeController');
Route::get('likes/buy/{user}', ['uses' => 'LikeController@buy', 'as' => 'like.buy']);
Route::post('likes/add', ['uses' => 'LikeController@add', 'as' => 'like.add']);
Route::get('like/create/{photo}', ['uses' => 'LikeController@create', 'as' => 'like.create']);
Route::get('likes/{photo}', ['uses' => 'LikeController@likes', 'as' => 'like.likes']);

// Судейство

Route::resource('referee', 'RefereeController');
Route::get('referee/score/{competitor}', ['uses' => 'RefereeController@score', 'as' => 'referee.score']);
Route::post('referee/score/{competitor}', ['uses' => 'RefereeController@storeScore', 'as' => 'referee.score.store']);

// Призовой фонд

Route::resource('fund', 'FundController');

// Рейтинг

Route::resource('rating', 'RatingController');
Route::get('rating/score/{nomination}', ['uses' => 'RatingController@showRatingScore', 'as' => 'rating.score']);
Route::get('rating/like/{nomination}', ['uses' => 'RatingController@showRatingLike', 'as' => 'rating.like']);

// Обработка персональных данных

Route::get('/personal-data', ['uses' => 'ContentController@showPersonalData', 'as' => 'personal_data.index']);

// Подробно о сайте

Route::get('/site-description', ['uses' => 'ContentController@showSiteDescription', 'as' => 'site_description']);

// Пользовательское соглашение

Route::get('/user-agreement', ['uses' => 'ContentController@showUserAgreement', 'as' => 'user_agreement']);

// Справка

Route::get('/help', ['uses' => 'ContentController@showHelp', 'as' => 'help']);

//Cookies

Route::post('/cookie/index/informer', ['uses' => 'CookieController@setCookieIndexInformer', 'as' => 'cookie.index.informer']);
Route::post('/cookie/nomination/informer', ['uses' => 'CookieController@setCookieNominationInformer', 'as' => 'cookie.nomination.informer']);
Route::post('/cookie/photos/informer', ['uses' => 'CookieController@setCookiePhotosInformer', 'as' => 'cookie.photos.informer']);
Route::post('/cookie/competitors/informer', ['uses' => 'CookieController@setCookieCompetitorsInformer', 'as' => 'cookie.competitors.informer']);
Route::post('/cookie/competitor/edit/informer', ['uses' => 'CookieController@setCookieCompetitorEditInformer', 'as' => 'cookie.competitor.edit.informer']);
Route::post('/cookie/competitor/informer', ['uses' => 'CookieController@setCookieCompetitorInformer', 'as' => 'cookie.competitor.informer']);
Route::post('/cookie/funds/informer', ['uses' => 'CookieController@setCookieFundsInformer', 'as' => 'cookie.funds.informer']);
Route::post('/cookie/likebuy/informer', ['uses' => 'CookieController@setCookieLikebuyInformer', 'as' => 'cookie.likebuy.informer']);
Route::post('/cookie/photo/informer', ['uses' => 'CookieController@setCookiePhotoInformer', 'as' => 'cookie.photo.informer']);
Route::post('/cookie/ratings/informer', ['uses' => 'CookieController@setCookieRatingsInformer', 'as' => 'cookie.ratings.informer']);
Route::post('/cookie/rating/likes/informer', ['uses' => 'CookieController@setCookieRatingLikesInformer', 'as' => 'cookie.rating.likes.informer']);
Route::post('/cookie/rating/scores/informer', ['uses' => 'CookieController@setCookieRatingScoresInformer', 'as' => 'cookie.rating.scores.informer']);
Route::post('/cookie/users/informer', ['uses' => 'CookieController@setCookieUsersInformer', 'as' => 'cookie.users.informer']);
Route::post('/cookie/user/edit/informer', ['uses' => 'CookieController@setCookieUserEditInformer', 'as' => 'cookie.user.edit.informer']);
Route::post('/cookie/user/informer', ['uses' => 'CookieController@setCookieUserInformer', 'as' => 'cookie.user.informer']);

//Платежные системы

//Форма оплаты за участие в номинации
Route::get('/payment/yandexmoney/{nomination}', ['uses' => 'Payment\YandexMoneyController@showFormNomination', 'as' => 'payment.yandexmoney.nomination']);
//Страница перехода с платежной системы при оплате участия в номинации
//Route::get('/payment/yandexmoney/recive/nomination/{nomination}', ['uses' => 'Payment\YandexMoneyController@recivePaymentNomination', 'as' => 'payment.yandexmoney.recive.nomination']);
//Форма оплаты загрузки дополнительных фото
Route::get('/payment/yandexmoney/{nomination}/additionalphoto', ['uses' => 'Payment\YandexMoneyController@showFormAdditionalPhoto', 'as' => 'payment.yandexmoney.additionalphoto']);


// Верстка

Route::group(['middleware' => 'makeup'], function () {

    Route::get('/template', function () {
        return view('makeup.template');
    });

    Route::get('/makeup/feedback', function () {
        return view('makeup.feedback');
    });

    Route::get('/makeup/nomination-index', function () {
        return view('makeup.nomination-index');
    });

    Route::get('/makeup/nomination-show', function () {
        return view('makeup.nomination-show');
    });

// Верстка jasny

    Route::get('/makeup/jasny', function () {
        return view('makeup.jasny');
    });

    Route::get('/makeup/main', function () {
        return view('makeup.main');
    });

    Route::get('/makeup/index', function () {
        return view('makeup.index');
    });

    Route::get('/makeup/text', function () {
        return view('makeup.text');
    });

    Route::get('/makeup/content', function () {
        return view('makeup.content');
    });

    Route::get('/makeup/comment', function () {
        return view('makeup.comment');
    });

    Route::get('/makeup/preview', function () {
        return view('makeup.preview');
    });

    Route::get('/makeup/nomination', function () {
        return view('makeup.nomination');
    });

    Route::get('/makeup/newslist', function () {
        return view('makeup.newslist');
    });

    Route::get('/makeup/gallery-competitor', function () {
        return view('makeup.gallery-competitor');
    });

    Route::get('/makeup/likebuy', function () {
        return view('makeup.likebuy');
    });

    Route::get('/makeup/referee', function () {
        return view('makeup.referee');
    });

    Route::get('/makeup/info', function () {
        return view('makeup.info');
    });

});

//Проверка загрузки содержимого в popover через ajax

Route::get('/makeup/testajax', ['uses' => 'UserController@testAjax', 'as' => 'users.testajax']);
