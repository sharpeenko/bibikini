<?php

use App\Competitor;
use App\News;
use App\Nomination;
use App\Photo;
use App\User;

// Index
Breadcrumbs::register('index', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('index'));
});

// Index > Login
Breadcrumbs::register('login', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Вход на сайт', route('login'));
});

// Index > Register
Breadcrumbs::register('register', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Регистрация', route('register'));
});

// Index > Ввод e-mail для сброса пароля
Breadcrumbs::register('password.reset', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Сброс пароля', route('password.reset'));
});

// Index > Отсутствие прав доступа, на представлении noaccess
Breadcrumbs::register('noaccess', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Нет доступа', route('noaccess'));
});

// Index > Пользователи
Breadcrumbs::register('users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Пользователи', route('users.index'));
});

//Профиль пользователя

Breadcrumbs::register('users.show', function ($breadcrumbs, $id) {
    $user = User::findOrFail($id);
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push($user->name, route('users.show', $user->id));
});

//Редактирование пользователя

Breadcrumbs::register('users.edit', function ($breadcrumbs, $id) {
    $user = User::findOrFail($id);
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('Мой профиль', route('users.edit', $user->id));
});

//Для комментариев

Breadcrumbs::register('users.comments', function ($breadcrumbs, $id) {
    $user = User::findOrFail($id);
    $breadcrumbs->parent('users.show', $user->id);
    $breadcrumbs->push('Комментарии', route('users.comments', $user->id));
});

//Номинации пользователя

Breadcrumbs::register('users.nominations', function ($breadcrumbs, $id) {
    $user = User::findOrFail($id);
    $breadcrumbs->parent('users.show', $user->id);
    $breadcrumbs->push('Номинации', route('users.nominations', $user->id));
});

//Для обратной связи

Breadcrumbs::register('feedback.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Обратная связь', route('feedback.index'));
});

//Для новостей

Breadcrumbs::register('news.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Новости', route('news.index'));
});

Breadcrumbs::register('news.show', function ($breadcrumbs, $id) {
    $newone = News::findOrFail($id);
    $breadcrumbs->parent('news.index');
    $breadcrumbs->push(strip_tags($newone->header), route('news.show', $newone->id));
});

// Номинации

Breadcrumbs::register('nomination.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Номинации', route('nomination.index'));
});

Breadcrumbs::register('nomination.show', function ($breadcrumbs, $id) {
    $nomination = Nomination::findOrFail($id);
    $breadcrumbs->parent('nomination.index');
    $breadcrumbs->push(strip_tags($nomination->name), route('nomination.show', $nomination->id));
});

Breadcrumbs::register('nomination.description', function ($breadcrumbs, $id) {
    $nomination = Nomination::findOrFail($id);
    $breadcrumbs->parent('nomination.show', $id);
    $breadcrumbs->push('Правила', route('nomination.description', $nomination->id));
});

//Участники номинаций

Breadcrumbs::register('competitor.index', function ($breadcrumbs, $id) {
    //$nomination = Nomination::findOrFail($id);
    $breadcrumbs->parent('nomination.show', $id);
    $breadcrumbs->push('Участники', route('nomination.competitors', $id));
});

Breadcrumbs::register('competitor.all', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Участники', route('competitor.index'));
});

Breadcrumbs::register('champion', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Чемпионы', route('champion'));
});

//Все фото
Breadcrumbs::register('photo.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Фотогалерея', route('photo.index'));
});

//Фото номинации

Breadcrumbs::register('nomination.photos', function ($breadcrumbs, $id) {
    $nomination = Nomination::findOrFail($id);
    $breadcrumbs->parent('nomination.show', $id);
    $breadcrumbs->push('Фотогалерея', route('nomination.photos', $nomination->id));
});

//Отдельное фото

Breadcrumbs::register('photo.show', function ($breadcrumbs, $id) {
    $photo = Photo::findOrFail($id);
    $breadcrumbs->parent('nomination.photos', $photo->getNomination->id);
    $breadcrumbs->push($photo->getCompetitor->getUser->name, route('photo.show', $photo->id));
});

// Просмотр

Breadcrumbs::register('competitor.show', function ($breadcrumbs, $id) {
    $competitor = Competitor::findOrFail($id);
    $breadcrumbs->parent('competitor.index', $competitor->getNomination->id);
    $breadcrumbs->push($competitor->getUser->name, route('competitor.show', $competitor->id));
});

// Редактирование

Breadcrumbs::register('competitor.edit', function ($breadcrumbs, $id) {
    $competitor = Competitor::findOrFail($id);
    $breadcrumbs->parent('competitor.index', $competitor->getNomination->id);
    $breadcrumbs->push($competitor->getUser->name, route('competitor.edit', $competitor->id));
});

// Покупка лайков

Breadcrumbs::register('like.buy', function ($breadcrumbs, $id) {
    $user = User::findOrFail($id);
    $breadcrumbs->parent('users.show', $user->id);
    $breadcrumbs->push('Купить лайки', route('like.buy', $user->id));
});

// Просмотр "Кто лайкнул"

Breadcrumbs::register('like.likes', function ($breadcrumbs, $id) {
    $photo = Photo::findOrFail($id);
    $breadcrumbs->parent('photo.show', $photo->id);
    $breadcrumbs->push('Лайкнувшие', route('like.likes', $photo->id));
});

// Судейский профайл

Breadcrumbs::register('referee.show', function ($breadcrumbs, $id) {
    $user = User::findOrFail($id);
    $breadcrumbs->parent('index');
    $breadcrumbs->push($user->name, route('referee.show', $user->id));
});

// Призовые фонды
Breadcrumbs::register('fund.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Призовые фонды', route('fund.index'));
});

//Рейтинги
Breadcrumbs::register('rating.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Рейтинги', route('rating.index'));
});

Breadcrumbs::register('rating.score', function ($breadcrumbs, $id) {
    $nomination = Nomination::findOrFail($id);
    $breadcrumbs->parent('nomination.show', $nomination->id);
    $breadcrumbs->push('Рейтинг оценок', route('rating.score', $nomination->id));
});

Breadcrumbs::register('rating.like', function ($breadcrumbs, $id) {
    $nomination = Nomination::findOrFail($id);
    $breadcrumbs->parent('nomination.show', $nomination->id);
    $breadcrumbs->push('Рейтинг лайков', route('rating.like', $nomination->id));
});

// Обработка ПД

Breadcrumbs::register('personal_data.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Согласие на обработку персональных данных', route('personal_data.index'));
});

// Описание ресурса

Breadcrumbs::register('site_description', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Подробно о сайте', route('site_description'));
});

// Справка

Breadcrumbs::register('help', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Справка', route('help'));
});

// Пользовательское соглашение

Breadcrumbs::register('user_agreement', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Пользовательское соглашение', route('user_agreement'));
});

// Платежные системы
Breadcrumbs::register('payment.yandexmoney.nomination', function ($breadcrumbs, $id) {
    $nomination = Nomination::findOrFail($id);
    $breadcrumbs->parent('nomination.show', $nomination->id);
    $breadcrumbs->push('Оплата участия', route('payment.yandexmoney.nomination', $nomination->id));
});

Breadcrumbs::register('payment.yandexmoney.additionalphoto', function ($breadcrumbs, $id) {
    $nomination = Nomination::findOrFail($id);
    $breadcrumbs->parent('nomination.show', $nomination->id);
    $breadcrumbs->push('Оплата дополнительного фото', route('payment.yandexmoney.additionalphoto', $nomination->id));
});
