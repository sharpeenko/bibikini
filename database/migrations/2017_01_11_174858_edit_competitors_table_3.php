<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditCompetitorsTable3 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('competitors', function (Blueprint $table) {
            $table->unique(['user_id', 'nomination_id'], 'unique_user_nomination');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('competitors', function (Blueprint $table) {
            $table->dropUnique('unique_user_nomination');
        });
    }

}
