<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('header');
            $table->date('publish_at');
            $table->text('introtext');
            $table->string('pic');
            $table->text('content');
            $table->string('author');
            $table->string('source');
            $table->boolean('published');
            $table->boolean('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('news');
    }

}
