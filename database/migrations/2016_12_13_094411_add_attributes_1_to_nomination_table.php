<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributes1ToNominationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('nomination', function (Blueprint $table) {
            $table->date('start');
            $table->date('finish');
            $table->text('introtext_list');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('nomination', function (Blueprint $table) {
            $table->dropColumn('start');
            $table->dropColumn('finish');
            $table->dropColumn('introtext_list');
        });
    }

}
