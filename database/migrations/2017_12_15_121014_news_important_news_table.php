<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsImportantNewsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::table('news', function (Blueprint $table) {
          $table->boolean('is_important')->default(0);
          $table->text('important_intro')->nullable();
          $table->string('important_styles')->nullable();
          $table->string('important_button_style')->nullable();
          $table->string('important_button_text')->nullable();
          $table->string('important_button_icon')->nullable();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::table('news', function (Blueprint $table) {
          $table->dropColumn('is_important');
          $table->dropColumn('important_intro');
          $table->dropColumn('important_styles');
          $table->dropColumn('important_button_style');
          $table->dropColumn('important_button_text');
          $table->dropColumn('important_button_icon');
      });
  }
}
