<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAttributes3ToNominationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('nominations', function (Blueprint $table) {
            $table->dropColumn('type');
            //$table->enum('type', ['мужчины', 'женщины', 'смешанный']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('nominations', function (Blueprint $table) {
            //
        });
    }

}
