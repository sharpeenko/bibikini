<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAttributes1ToNominationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('nominations', function (Blueprint $table) {
            $table->text('introtext')->nullable()->change();
            $table->string('pic')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->date('start')->nullable()->change();
            $table->date('finish')->nullable()->change();
            $table->text('introtext_list')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('nominations', function (Blueprint $table) {
            //
        });
    }

}
