<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTimestampToLikeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_users', function (Blueprint $table) {
           $table->timestamp('got_at')->useCurrent();
           $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_users', function (Blueprint $table) {
            $table->dropColumn('got_at');
            $table->dropColumn('deleted_at');

        });
    }
}
