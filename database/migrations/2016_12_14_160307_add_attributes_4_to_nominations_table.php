<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributes4ToNominationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('nominations', function (Blueprint $table) {
            $table->string('css_header')->nullable();
            $table->string('css_pic')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('nominations', function (Blueprint $table) {
            $table->dropColumn('css_header');
            $table->dropColumn('css_pic');
        });
    }

}
