<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('competitor_id')->unsigned();
            $table->integer('nomination_id')->unsigned();
            $table->float('avscore')->unsigned()->nullable();
            $table->integer('position')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function (Blueprint $table) {
            Schema::dropIfExists('ratings');
        });
    }
}
