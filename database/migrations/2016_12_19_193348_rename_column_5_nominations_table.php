<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumn5NominationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('nominations', function (Blueprint $table) {
            $table->dropColumn('css_styles');
            $table->dropColumn('css_header');
            $table->dropColumn('css_pic');
            $table->dropColumn('type_icon');
            $table->json('styles')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('nominations', function (Blueprint $table) {
            $table->dropColumn('styles');
        });
    }

}
