<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * [CreateFundsTable description]
 */
class CreateFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('prizeable');
            $table->integer('nomination_id')->unsigned();
            $table->float('sum')->unsigned()->nullable();
            $table->float('position_1')->unsigned()->nullable();
            $table->float('position_2')->unsigned()->nullable();
            $table->float('position_3')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funds');
    }
}
