<?php
return [
    'providers' => [
            Barryvdh\Debugbar\ServiceProvider::class,
    ],
    'aliases' => [
        'Debugbar' => Barryvdh\Debugbar\Facade::class,
    ],
];
