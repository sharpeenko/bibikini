<?php

/**
 * @Author: sharpeenko
 * @Date:   2017-02-08 09:19:12
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-02-10 19:58:35
 */

use Illuminate\Foundation\Testing\DatabaseTransactions;

class NominationFunctionalTest extends TestCase
{

    use DatabaseTransactions;

    const TEST_HOST = 'bibikini.com.dev';
    private $testuser = 'testuser1@bibikini.com.dev';
    private $testuser_password = '12345678';
    private $testuser_id = '2';
    private $testadmin = 'testadmin@bibikini.com.dev';
    private $testadmin_password = '12345678';
    private $testmanager = 'testcontent1@bibikini.com.dev';
    private $testmanager_password = '12345678';
    private $testnotcompetitor = 'testuser_not_competitor@bibikini.com.dev';
    private $testnotcompetitor_password = '12345678';
    private $testnotcompetitor_id = '12';
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Гость открывает Номинацию и на странице присутствует элемент кнопка "Участвовать"
     * и нет div.is-competitor
     * @return [type] [description]
     */
    public function testNominationCompetitorGuest()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/nomination/1');
        $this->seeElement('a.btn-nommi');
        $this->dontSeeElement('div.is-competitor');
    }

    /**
     * Пользователь (не участник) открывает Номинацию и на странице присутствует элемент кнопка "Участвовать"
     * @return [type] [description]
     */
    public function testNominationCompetitorUser()
    {
        $this->visit('/login');
        $this->type($this->testnotcompetitor, 'email');
        $this->type($this->testnotcompetitor_password, 'password');
        $this->press('Войти');
        $this->seePageIs('/');
        $this->visit('/nomination/1');
        $this->seeElement('a.btn-nommi');
        $this->dontSeeElement('div.is-competitor');
    }

    /**
     * Участник открывает Номинацию и на странице присутствует элемент div.is-competitor
     * @return [type] [description]
     */
    public function testNominationCompetitor()
    {
        $this->visit('/login');
        $this->type($this->testadmin, 'email');
        $this->type($this->testadmin_password, 'password');
        $this->press('Войти');
        $this->seePageIs('/');
        $this->visit('/nomination/1');
        $this->seeElement('div.is-competitor');
    }

    /**
     * @TODO: написать функцию
     * @TODO: сброс базы данных после теста
     * Пользователь подает заявку, номинация открыта
     * @return [type] [description]
     */
    public function testUserCompetitorNominationOpened()
    {

        //@TODO: Похоже, тестирование не работает с модальными окнами, поэтому от данных тестов пока что откажусь

        //$this->visit('/login');
        //$this->type($this->testnotcompetitor, 'email');
        //$this->type($this->testnotcompetitor_password, 'password');
        //$this->press('Войти');
        //$this->seePageIs('/');
        //$this->visit('/nomination/1');
        //$this->seeElement('a.btn-nommi');
        //$this->click('Участвовать');
        //$this->seeElement('div#mod1');
        //$this->seeElement('a#btn-agree');
        /**
         * @ERROR: InvalidArgumentException: Could not find a link with a body, name, or ID attribute of [#btn-agree].
         */
        //$this->click('#btn-agree');

        /**
         * @ERROR: Maximum function nesting level of '256' reached, aborting! in /home/developer/www/bibikini.com.dev/www/vendor/laravel/framework/src/Illuminate/Foundation/Bootstrap/HandleExceptions.php on line 115
         */
        //$this->visit('/competitor/1/create');

        //$this->seeInDatabase('competitors', ['nomination_id' => '1', 'user_id' => $this->testnotcompetitor_id]);
        //$this->visit('/nomination/1');

    }

    /**
     * @TODO: написать функцию
     * Пользователь подает заявку, номинация закрыта
     * @return [type] [description]
     */
    public function testUserCompetitorNominationClosed()
    {

        //@TODO: Похоже, тестирование не работает с модальными окнами, поэтому от данных тестов пока что откажусь

    }
}
