/**
 * @Author: sharpeenko
 * @Date:   2017-02-06 09:45:43
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-02-07 10:11:09
 */
<?php

use App\User;

class UserFunctionalTest extends TestCase
{
    const TEST_HOST = 'bibikini.com.dev';
    private $testuser = 'testuser1@bibikini.com.dev';
    private $testuser_password = '12345678';
    private $testuser_id = '2';
    private $testadmin = 'testadmin@bibikini.com.dev';
    private $testadmin_password = '12345678';
    private $testmanager = 'testcontent1@bibikini.com.dev';
    private $testmanager_password = '12345678';

    /**
     * Тестовая функция. Вход пользователя с использованием некорректного пароля.
     *
     * @return void
     */
    public function testLoginPasswordUncorrect()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/');
        $this->visit('/login');
        $this->type($this->testuser, 'email');
        $this->type('abracadabra', 'password');
        $this->press('Войти');
        $this->seePageIs('/login');
        $this->see('Пожалуйста проверьте правильность заполнения формы');
    }

    /**
     * Тестовая функция. Вход пользователя с использованием корректного пароля.
     *
     * @return void
     */
    public function testLoginPasswordCorrect()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/');
        $this->visit('/login');
        $this->type($this->testuser, 'email');
        $this->type($this->testuser_password, 'password');
        $this->press('Войти');
        $this->seePageIs('/');
    }

    /**
     * А если авторизованный пользователь открывает собственный профиль, то - редактирование
     * @return [type] [description]
     */
    public function testEdit()
    {

        /**
         * TODO: Не хочет работать вот такая конструкция
         * $user = new User(['id' => 2]);
         * $this->be($user);
         */

        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type($this->testuser, 'email');
        $this->type($this->testuser_password, 'password');
        $this->press('Войти');
        $this->visit('/users/' . $this->testuser_id);
        $this->seePageIs('/users/' . $this->testuser_id . '/edit');

    }

    /**
     * TODO: Дальнейшие методы нужно отредактировать
     */

    /*
     * Редактирование профиля владельцем, интеграционное, с загрузкой фото.
     *
     */
    public function testUpdateIntegr()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type('testuser1@bibikini.com.dev', 'email');
        $this->type('12345678', 'password');
        $this->press('Войти');
        $this->seePageIs('/');
        $this->visit('/users/2/edit');
        /**
         * LogicException: The selected node does not have a form ancestor.
         * Что это значит? Нет формы-родителя?
         */
        $this->press('Редактировать данные');
        $this->type('Тестовый пользователь 1', 'name');
        $this->select('женский', 'sex');
        $this->attach('absolutePathToFile', 'avatar');
        $this->press('Редактировать');
        $this->seePageIs('/users/2/edit');
        $this->see('testuser1@bibikini.com.dev');
    }

    /*
     * Интеграционное, изменение пароля, данные корректны
     */
    public function testChangePasswordCorrect()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type('testuser1@bibikini.com.dev', 'email');
        $this->type('12345678', 'password');
        $this->press('Войти');
        $this->seePageIs('/');
        $this->visit('/users/2/edit');
        $this->type('12345678', 'password');
        $this->type('12345678', 'password_confirmation');
        $this->press('Подтвердить');
        $this->seePageIs('/users/2/edit');
        $this->see('Пароль успешно изменен');
    }

    /*
     * Интеграционное, изменение пароля, недостаточное количество символов
     */
    public function testChangePasswordUncorrect()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type('testuser1@bibikini.com.dev', 'email');
        $this->type('12345678', 'password');
        $this->press('Войти');
        $this->seePageIs('/');
        $this->visit('/users/2/edit');
        $this->type('123', 'password');
        $this->type('123', 'password_confirmation');
        $this->press('Подтвердить');
        $this->seePageIs('/users/2/edit');
        $this->see('Должно быть минимум 6 символов');
    }

    /*
     * Загрузка аватара пользователем
     */
    public function testUploadAvatar()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/');
        $this->visit('/login');
        $this->type($this->testuser, 'email');
        $this->type($this->testuser_password, 'password');
        $this->press('Войти');
        $this->seePageIs('/');
        $this->visit('/users/' . $this->testuser_id . '/edit');
        /**
         * LogicException: The selected node does not have a form ancestor.
         * Что это значит? Нет формы-родителя?
         */
        $this->attach('absolutePathToFile', 'avatar');
        $this->press('Редактировать');
        $this->seePageIs('/users/' . $this->testuser_id . '/edit');
    }

}
