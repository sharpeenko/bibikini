/**
 * @Author: sharpeenko
 * @Date:   2017-02-06 09:47:06
 * @Last Modified by:   sharpeenko
 * @Last Modified time: 2017-02-07 09:57:45
 */


<?php

use App\User;

class UserControllerTest extends TestCase
{

    //protected $mock; // такого нигде в примерах нет

    public function __construct()
    {

        //$this->mock = Mockery::mock('Eloquent', User::class);

    }

    public function tearDown()
    {
        Mockery::close();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Проверяет отображение списка пользователей
     * @return [type] [description]
     */
    public function testIndex()
    {
        /**
         * @TODO: На форуме задан вопрос на тему, почему тест с моком не проходит?
         */
        /**
         * Вариант без мока работает
         */
        //$response = $this->call('GET', 'users');
        //$this->assertViewHas('users');
        // getData() returns all vars attached to the response.
        //$users = $response->original->getData()['users'];
        //$this->assertInstanceOf('Illuminate\Contracts\Pagination\Paginator', $users);

        /**
         * Вариант с моком не работает
         */

        $mock = Mockery::mock('Eloquent', User::class);
        /**
         * Следующая строка корректно срабатывает, если вместо once()
         * поставить never(), тест выдаст ошибку, что, в данном случае, верно.
         * Следующие три строки - единый логический блок
         */
        $mock->shouldReceive('whereHas')->once()->andReturn(10000);
        $this->app->instance(User::class, $mock);
        $this->call('GET', 'users');

        //$this->assertRedirectedToRoute('users.index');
        $this->assertViewHas('users');
        //$this->visit(' / users')->see('beee');
    }

    /**
     * Просмотр профиля пользователя другим авторизованным пользователем
     * @TODO: Думаю, к интеграционному тестированиею данный метод отношения не имеет. Перенести его в UserUnit
     * @return [type] [description]
     */
    public function testShow()
    {
        $user = new User(['id' => 1]);
        $this->be($user);
        $response = $this->action('GET', 'UserController@show', ['user' => 2]);
        $view = $response->original;
        $this->assertEquals('users.profile', $view->getName());
    }

    /*
     * Редактирование профиля владельцем, юнит
     * Тесты надо писать так, чтобы, например, было видно, корректные ли значения "ложатся" в БД
     *
     */
    public function testUpdateUnit()
    {
        $user = new User(['id' => 2]);
        $this->be($user);
        $response = $this->action('PUT', 'UserController@update', ['user' => 2, 'name' => 'Тестированный пользователь', 'sex' => 'мужской']);
        $this->seeInDatabase('users', ['name' => 'Тестированный пользователь']);
    }
}
