<?php

class AdminTest extends TestCase
{

    const TEST_HOST = 'bibikini.com.dev';

    private $testuser = 'testuser1@bibikini.com.dev';
    private $testuser_password = '12345678';
    private $testuser_id = '2';
    private $testadmin = 'testadmin@bibikini.com.dev';
    private $testadmin_password = '12345678';
    private $testmanager = 'testcontent1@bibikini.com.dev';
    private $testmanager_password = '12345678';
    private $testpanel = 'testpanel@bibikini.com.dev';
    private $testpanel_password = '12345678';
    private $testmoderator = 'testmoderator1@bibikini.com.dev';
    private $testmoderator_password = '12345678';

    /*
     * Тестовая функция. Вход гостя в панель администратора.
     * Должен перенаправить на форму входа
     */

    public function testAdminPanelGuest()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/admin');
        $this->visit('/login');
    }

    /*
     * Тестовая функция. Вход зарегистрированного в панель администратора
     * Должен показать, что доступ закрыт
     */

    public function testAdminPanelUser()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type($this->testuser, 'email');
        $this->type($this->testuser_password, 'password');
        $this->press('Войти');
        $this->visit('/admin');
        $this->see('Извините, Вы не можете');
    }

    /*
     * Тестовая функция. Вход тестового пользователя админки
     * Должен показать, что открыт только один доступный раздел
     */

    public function testAdminPanelTestpanel()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type($this->testpanel, 'email');
        $this->type($this->testpanel_password, 'password');
        $this->press('Войти');
        $this->visit('/admin');
        $this->dontSee('Аккаунты');
        $this->dontSee('Роли пользователей');
        $this->dontSee('Получатели');
        $this->dontSee('Новости');
        $this->dontSee('Комментарии новостей');
        $this->dontSee('Номинации');
        $this->dontSee('Участники');
        $this->dontSee('Комментарии');
        $this->see('Сообщения ФОС');
    }

    /*
     * Тестовая функция. Вход администратора в панель администратора.
     * Должны быть видны все разделы.
     */

    public function testAdminPanelAdmin()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type($this->testadmin, 'email');
        $this->type($this->testadmin_password, 'password');
        $this->press('Войти');
        $this->visit('/admin');
        $this->see('Аккаунты');
        $this->see('Роли пользователей');
        $this->see('Получатели');
        $this->see('Сообщения ФОС');
        $this->see('Комментарии новостей');
        $this->see('Новости');
        $this->see('Номинации');
        $this->see('Участники');
        $this->see('Фотогалерея');
        $this->see('Комментарии');
    }

    /*
     * Тестовая функция. Вход контент-менеджера в панель администратора.
     * Не должно быть видно разделов, доступных только админам.
     *
     */

    public function testAdminPanelManager()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type($this->testmanager, 'email');
        $this->type($this->testmanager_password, 'password');
        $this->press('Войти');
        $this->visit('/admin');
        $this->dontSee('Аккаунты');
        $this->dontSee('Роли пользователей');
        $this->dontSee('Получатели');
        $this->dontSee('Номинации');
        $this->see('Сообщения ФОС');
        $this->see('Новости');
        $this->see('Комментарии новостей');
        $this->dontSee('Участники');
        /**
         * Пока есть строка "Комментарии новостей", тут ошибка будет
         */
        //$this->('Комментарии');
        $this->dontSee('Фотогалерея');
    }

    /*
     * Тестовая функция. Вход модератора в панель администратора.
     * Не должно быть видно разделов, доступных только админам.
     *
     */

    public function testAdminModerator()
    {
        $_SERVER['HTTP_HOST'] = self::TEST_HOST;
        $this->visit('/login');
        $this->type($this->testmoderator, 'email');
        $this->type($this->testmoderator_password, 'password');
        $this->press('Войти');
        $this->visit('/admin');
        $this->dontSee('Аккаунты');
        $this->dontSee('Роли пользователей');
        $this->dontSee('Получатели');
        $this->dontSee('Номинации');
        $this->see('Сообщения ФОС');
        $this->dontSee('Новости');
        $this->dontSee('Комментарии новостей');
        $this->dontSee('Участники');
        $this->see('Фотогалерея');
        $this->see('Комментарии');
    }

}
